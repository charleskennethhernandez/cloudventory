<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mortgage_computation {

	public $property_value;
	public $downpayment_rate;
	public $interest_rate;
	public $loan_term;
	public $date_created;
	public $period_type_id;
	public $target_date;
	public $current_date; 
	public $transaction_id; 
	public $payment_method_id; 
	public $loanable_amount; 
	public $transaction = array();


	public function reconstruct( $property_value = 0,$loan_value = 0, $downpayment_rate = 0, $interest_rate = 0, $loan_term = 0, $reservation_rate = 0, $date_created = 0, $period_type_id = 1,$transaction_id = false, $payment_method_id = false, $target_date = false ) {



		if(!empty($payment_method_id) && !empty($transaction_id)){
			$this->payment_method_id = $payment_method_id;
			$this->transaction_id = $transaction_id;
			$CI =  &get_instance();
			$this->transaction = $CI->db->select('*')
				->from('transactions')
				->where('id', $transaction_id)
				->get()->row();
		}
		
		$this->current_date = date('Y-m-d');
		$this->reservation_rate = $reservation_rate;
		$this->period_type_id = $period_type_id;

		// if( $property_value && $downpayment_rate  &&  $interest_rate && $loan_term ) {
			
			$this->property_value = $property_value;
			$this->loan_value = $loan_value;
			$this->downpayment_rate = $downpayment_rate;
			
			$this->interest_rate = $interest_rate;
			$this->loan_term = $loan_term;
			$this->date_created = $date_created;
			if(empty($target_date)):
				$this->target_date = $this->get_target_date();
			else:
				$this->target_date = $target_date;
			endif;	
		// }
	}

	/* INFORMATION SUMMARY START */
	public function loanable_amount() {
		if (isset($this->loan_value)) {
			return $this->loan_value;
		} else {
			return 0;
		}
		
		// return $this->property_value * ( 1 - $this->compute_percentage( $this->downpayment_rate + $this->reservation_rate ) );
	}
	
	public function downpayment_amount() {
		return $this->downpayment_rate;
		// return $this->property_value * $this->compute_percentage( $this->downpayment_rate);
	}
	
	/* MODIFACTION By BRYAN */
	public function get_target_date(){
		/* 
			cornerstone_commission_details table
			get by payment_method_id
			period_id 1. Reservation 2. Downpayment 3. Balance
		*/
		
		$this->payment_method_id;
		$CI =  &get_instance(); 
		$CI->db->select('*');
		$CI->db->from('commission_details');
		$CI->db->where('payment_method_id', $this->payment_method_id);
		$commissions = $CI->db->get()->result();
		// print_r($commissions); die;
		$days = 0;
		
		if($this->period_type_id == 1 || $this->period_type_id == 2){
			
			if(!empty($commissions)):
				foreach($commissions as $commission){
					if($commission->period_id == $this->period_type_id){
						 $days = $days+$commission->days;
					}
					
				} 
				
			endif;
			
			
		} else {
			return $this->transaction->period_target_date;
		} 
				
		return date('Y-m-d', strtotime($this->date_created. '+ '.$days.'days'));
		
	}
	
	public function get_remaining_days(){
		$this->target_date = $this->transaction->period_next_target_date; //$this->transaction->period_target_date;
		$timeleft = strtotime($this->target_date) - strtotime($this->current_date);
		$daysleft = round((($timeleft/24)/60)/60); 
		
		return $daysleft;
		
	}
	public function get_next_payment_date(){
		
		if($this->period_type_id == 1){
			return $this->transaction->period_next_target_date;
		}
		if(!empty($this->transaction)){
			return $this->transaction->period_next_target_date;
		}
		return $this->target_date;
	}
	
	public function get_target_payment_amount($id = false, $current_balance = false){
		$CI = &get_instance();
		//$CI->db->simple_query('ALTER TABLE cornerstone_transaction_histories DROP INDEX TransactionHistoriesIndex');
		$CI->db->simple_query('CREATE INDEX TransactionHistoriesIndex ON cornerstone_transaction_histories (`transaction_id`, `transaction_status_id`, `period_type_id`)');
		$result = $CI->db->select('*')
				->from('transaction_histories')
				->where('transaction_id', $id)
				->where('period_type_id', $this->period_type_id )
				->where('transaction_status_id !=', 4 )
				->where('transaction_status_id !=', 5 )
				->get()->result();

		// $c = substr(strrchr($this->downpayment_rate, "."), 1);

		// if ($c > 99) {
		// 	$dp = $this->downpayment_rate;
		// } else {
			// $dp = $this->downpayment_rate / 100;
		// }

		// $d = substr(strrchr($this->reservation_rate, "."), 1);
		// echo $d;
		// echo "<br>";
		// echo $this->reservation_rate;
		// echo "<br>";
		// if ($d > 99) {
		// 	echo "string";
		// 	$reservation = $this->reservation_rate;
		// } else {
			// echo "string2";
			// $reservation = $this->reservation_rate / 100;
		// }	
		// echo $this->property_value;echo "<br>";

		// $intial_dp =  $this->property_value * $dp;//intial downpayment 
		// $intial_reservation =  $this->property_value * $reservation;//intial downpayment 

		$intial_dp =  $this->downpayment_rate;//intial downpayment 
		$intial_reservation =  $this->reservation_rate;//intial downpayment 

		if($this->period_type_id == 1) { //if period is equal to reservation or pending
			$balance = $intial_reservation;
		} else if($this->period_type_id == 2) {                             
			$query = $CI->db->get_where('cornerstone_transactions', array('id' => $id));
			$result_trans = $query->row_array();
			$result_dp = $CI->db->select('*')
				->from('transaction_histories')
				->where('transaction_id', $id)
				->where('period_type_id', 2 )
				->where('transaction_status_id !=', 4 )
				->where('transaction_status_id !=', 5 )
				->get()->result();
			// echo "<pre>";
			// print_r($result_dp);
			if ($result_dp) {
				if ($result_trans['payment_method_id'] != 2) {
					$balance = $this->monthly_downpayment_amount($result_trans['downpayment_interest_rate'], $result_trans['downpayment_term']);
				} else {
					$balance = $intial_dp;
				}
			// 	foreach($result_dp as $payment){
			// 		$balance = $balance - $payment->last_payment_amount;
			// 	}
			// 	echo $balance;
			// die();

			} else if ($result_trans['payment_method_id'] != 2) {
				$balance = $this->monthly_downpayment_amount($result_trans['downpayment_interest_rate'], $result_trans['downpayment_term']);
			} else {
				$balance = $intial_dp;
			}
		} else {
			$balance = $this->monthly_payment();
		}

		// if (($this->period_type_id == 2)&&($result_trans['payment_method_id'] == 2)) {
			if(empty($result)) {
				return $balance;
			} else {
				foreach($result as $payment){
					$balance = $balance - $payment->last_payment_amount;
				}
				return abs($balance);
			}
		// } else {
			// return $balance;
		// }
	}
	
	public function get_current_status(){
		
	}
	
	public function get_period_title(){
		$CI =  &get_instance();
		
		$period = $CI->db->select('name')
					->from('commission_period_types')
					->where('id', $this->period_type_id)
					->get()->row_array();
		return $period['name'];
	}
	
	/* END OF MODIFACTION By BRYAN */
	
	public function loan_term_in_month() {
		return $this->loan_term * 12;
	}
	
	
	public function monthly_payment() {
		if ($this->interest_rate) {
			return  ( $this->compute_percentage( $this->interest_rate ) / 12 * $this->loanable_amount() ) / ( 1 - pow( ( 1 + $this->compute_percentage( $this->interest_rate ) / 12 ) , - ( $this->loan_term * 12 ) ) );
		} else {
			// return  ( $this->compute_percentage( $this->interest_rate ) / 12 * $this->loanable_amount() ) / ( 1 - pow( ( 1 + $this->compute_percentage( $this->interest_rate ) / 12 ) , - ( $this->loan_term * 12 ) ) );
			if ($this->loan_term) {
				return  ( $this->loanable_amount() ) /  ( $this->loan_term * 12) ;
			} else {
				return 0;
			}
		}
	}

	public function monthly_downpayment_amount($downpayment_interest_rate = 0, $downpayment_term = 0) {

		if ($downpayment_interest_rate) {
			// echo $downpayment_interest_rate;
			// echo "<br>";
			// echo $this->downpayment_amount();
			// echo "<br>";
			// echo $downpayment_term;
			// echo "<br>";
			// echo "string";
			// echo "<br>";

			return  ( $this->compute_percentage( $downpayment_interest_rate ) / 12 * $this->downpayment_amount() ) / ( 1 - pow( ( 1 + $this->compute_percentage( $downpayment_interest_rate ) / 12 ) , - ( $downpayment_term ) ) );
		} else {
			if (empty($downpayment_term)) {
				$downpayment_term = 1;
			}
			return  ( $this->downpayment_amount() ) /  ( $downpayment_term  ) ;
		}

	}

	public function monthly_principal($remaining_balance,$interest_rate = 0) {
		if ($interest_rate) {
			$this->interest_rate = $interest_rate;
		}
		$interest = round( $remaining_balance * $this->compute_percentage( $this->interest_rate ) / 12, 5 );
		if ($this->loanable_amount != 0) {
			return $principal = round( $this->monthly_payment() - $interest, 6, PHP_ROUND_HALF_UP);
		} else {
			return $interest;
		}
	}

	public function monthly_principal_downpayment($downpayment_interest_rate = 0,$downpayment_term = 0,$remaining_downpayment) {

		$interest = round( $remaining_downpayment * $this->compute_percentage( $downpayment_interest_rate ) / 12, 5 );
		// echo $downpayment_interest_rate.'a';
		// echo "<br>";
		// echo $downpayment_term.'b';
		// echo "<br>";
		// echo $remaining_downpayment.'c';
		// echo "<br>";
		// echo $interest.'d';
		// echo "<br>";
		return $principal = round( $this->monthly_downpayment_amount($downpayment_interest_rate,$downpayment_term) - $interest, 6, PHP_ROUND_HALF_UP);
	}

	
	// public function monthly_payment() {
		
	// 	return  ( $this->compute_percentage( $this->interest_rate ) / 12 * $this->loanable_amount() ) / ( 1 - pow( ( 1 + $this->compute_percentage( $this->interest_rate ) / 12 ) , - ( $this->loan_term * 12 ) ) );
	
	// }
	
	public function total_interest() {
	
		return ( $this->monthly_payment() * $this->loan_term * 12 - $this->loanable_amount() );
	
	}


	public function recommended_monthly_income() {
	
		return round ( $this->monthly_payment() / 0.4,0 );
	
	}
	/* INFORMATION SUMMARY END */
	
	/* SAVINGS ON DOWNPAYMENT START */
	
	/* NO DOWNPAYMENT START */
	public function nd_monthly_payment() {
		
		if ($this->interest_rate) {
			return  ( $this->compute_percentage( $this->interest_rate ) / 12 * $this->property_value ) / ( 1 - pow( ( 1 + $this->compute_percentage( $this->interest_rate ) / 12 ) , - ( $this->loan_term * 12 ) ) );
		} else {
			return  ( $this->property_value ) /  ( $this->loan_term * 12) ;
		}
	
	}
	
	public function nd_total_monthly_payment() {
	
		return $this->nd_monthly_payment() * $this->loan_term * 12;
	
	}
	
	public function nd_total_interest() {
	
		return $this->nd_monthly_payment() * $this->loan_term * 12 - $this->property_value;
	
	}
	/* NO DOWNPAYMENT END */
	
	/* CURRENT DOWNPAYMENT START */
	public function cd_total_monthly_payment() {
	
		return  $this->monthly_payment() * $this->loan_term * 12;
	
	}
	
	public function cd_total_interest() {
	
		return $this->monthly_payment() * $this->loan_term * 12 - $this->loanable_amount();
	
	}
	/* CURRENT DOWNPAYMENT END */
	
	public function total_interest_savings() {
	
		return $this->nd_total_interest() - $this->cd_total_interest();
	
	}
	
	/* SAVINGS ON DOWNPAYMENT END */
	
	/* GRAPH START */
	public function g_total_monthly_payment() {
	
		return $this->nd_monthly_payment() * $this->loan_term * 12;
	
	}
	
	public function g_total_interest() {
	
		return $this->nd_total_interest();
	
	}
	
	public function g_total_principal() {
	
		return $this->g_total_monthly_payment() - $this->g_total_interest();
	
	}
	/* GRAPH END */
	
	/* PAYMENT BREAKDOWN START */
	
	public function breakdown() {
	
		$i = 0;
		
		$x = 1;
		
		$sum_amount = 0;
		
		$breakdown = array();
		
		for( $i = 1; $i <= ( $this->loan_term * 12 ); $i++) {
		
			/* MONTHLY BREAKDOWN START */
			$breakdown['monthly'][$i]['month'] = $i;
		
			if( $i == 1 ) {

				$breakdown['monthly'][$i]['beggining_balance'] = round( $this->loanable_amount(), 3 );
				
			} else {

				$breakdown['monthly'][$i]['beggining_balance'] = round( $breakdown['monthly'][($i - 1)]['ending_balance'], 3 );
				
			}
			
			$breakdown['monthly'][$i]['interest'] = round( $breakdown['monthly'][$i]['beggining_balance'] * $this->compute_percentage( $this->interest_rate ) / 12, 5 );

			$sum_amount = $sum_amount + $breakdown['monthly'][$i]['interest'];
				
			$breakdown['monthly'][$i]['principal'] = round( $this->monthly_payment() - $breakdown['monthly'][$i]['interest'], 6, PHP_ROUND_HALF_UP);
				
			$breakdown['monthly'][$i]['ending_balance'] = round( $breakdown['monthly'][$i]['beggining_balance'] - $breakdown['monthly'][$i]['principal'], 3 );
			/* MONTHLY BREAKDOWN END */
			
			/* YEARLY BREAKDOWN START */
			if( ( $i % 12 ) == 0 ) {
			
				$breakdown['yearly'][$x]['year'] = $i / 12;
					
				if( ( $i / 12 ) == 1 ) {

					$breakdown['yearly'][$x]['beggining_balance'] = round( $this->loanable_amount(), 3 );
					
				} else {
					
					$breakdown['yearly'][$x]['beggining_balance'] = round( $breakdown['yearly'][$x  - 1]['ending_balance'], 3 );

				}
					
				$breakdown['yearly'][$x]['interest'] = $sum_amount;
						
				$breakdown['yearly'][$x]['principal'] = $this->monthly_payment() * 12 - $breakdown['yearly'][$x]['interest'];
						
				$breakdown['yearly'][$x]['ending_balance'] = round( $breakdown['yearly'][$x]['beggining_balance'] - $breakdown['yearly'][$x]['principal'], 3 );
					
				$sum_amount = 0;
				
				$x++;
				
			}
			/* YEARLY BREAKDOWN END */
			
		
		}
		
		return $breakdown;
	
	}
	
	/* PAYMENT BREAKDOWN END */
	
	
	private function compute_percentage( $value = 0 ) {
	
		if( $value ) {

			return $value / 100;

		}

	}

}

?>