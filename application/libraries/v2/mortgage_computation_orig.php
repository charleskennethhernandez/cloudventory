<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mortgage_computation_orig {
	public $property_value;
	public $downpayment_rate;
	public $interest_rate;
	public $loan_term;
	
	public function reconstruct( $params = array() ) {
			$this->property_value = $params['collectible_price'];
			$this->reservation_rate = $params['reservation_rate'];
			$this->downpayment_rate = $params['downpayment_rate'];
			$this->loan_rate = $params['loan_rate'];
			$this->loan_interest_rate = $params['loan_interest_rate'];
			$this->downpayment_interest_rate = $params['downpayment_interest_rate'];
			$this->loan_term = $params['loan_term'];
			$this->downpayment_term = $params['downpayment_term'];
			$this->reservation_date = $params['reservation_date'];
			$this->loan_effectivity_date = $params['loan_effectivity_date'];
			$this->downpayment_effectivity_date = $params['downpayment_effectivity_date'];


			if ($this->downpayment_interest_rate  == 0.00) {
				$this->downpayment_interest_rate = 0;
			}

			if ($this->loan_interest_rate == 0.00) {
				$this->loan_interest_rate = 0;
			}
	}

	/* INFORMATION SUMMARY START */
	// public function loanable_amount() {
	// 	return $this->property_value * ( 1 - $this->compute_percentage( $this->downpayment_rate ) );
	// }
	
	
	public function reservation_amount(){
		return $this->reservation_rate;
	}

	public function loanable_amount() {
		return $this->loan_rate;
	}
	
	public function downpayment_amount() {
		return $this->downpayment_rate;
	}
	
	public function loan_term_in_month() {
		return $this->loan_term;
	}
	
	public function monthly_payment() {
		if ($this->loan_interest_rate) {
			return  ( $this->compute_percentage( $this->loan_interest_rate ) / 12 * $this->loanable_amount() ) / ( 1 - pow( ( 1 + $this->compute_percentage( $this->loan_interest_rate ) / 12 ) , - ( $this->loan_term ) ) );
		} else if ($this->loan_term) {
			return  ( $this->loanable_amount() ) / ( $this->loan_term  );
		} else {
			return $this->loanable_amount();
		}
	}
	
	public function dp_monthly_payment() {
		if ( $this->downpayment_interest_rate ) {
			return  ( $this->compute_percentage( $this->downpayment_interest_rate ) / 12 * $this->downpayment_amount() ) / ( 1 - pow( ( 1 + $this->compute_percentage( $this->downpayment_interest_rate ) / 12 ) , - ( $this->downpayment_term ) ) );
		} else if ( $this->downpayment_term ) {
			return  ( $this->downpayment_amount() ) / ( $this->downpayment_term  );
		} else {
			return $this->downpayment_amount();
		}
	}

	public function total_interest() {
		return ( $this->monthly_payment() * $this->loan_term - $this->loanable_amount() );
	}
	
	public function recommended_monthly_income() {
		return round ( $this->monthly_payment() / 0.4,0 );
	}

	/* INFORMATION SUMMARY END */
	
	/* SAVINGS ON DOWNPAYMENT START */
	
	/* NO DOWNPAYMENT START */
	public function nd_monthly_payment() {
		if ($this->loan_interest_rate) {
			return  ( $this->compute_percentage( $this->loan_interest_rate ) / 12 * $this->property_value ) / ( 1 - pow( ( 1 + $this->compute_percentage( $this->loan_interest_rate ) / 12 ) , - ( round($this->loan_term) ) ) );
		} else {
			return  ( $this->property_value ) / ( $this->loan_term );
		}
	}
	
	public function nd_total_monthly_payment() {
		return $this->nd_monthly_payment() * round($this->loan_term);
	}
	
	public function nd_total_interest() {
	
		return $this->nd_monthly_payment() * round($this->loan_term) - $this->property_value;
	
	}
	/* NO DOWNPAYMENT END */
	
	/* CURRENT DOWNPAYMENT START */
	public function cd_total_monthly_payment() {
		return  $this->monthly_payment() * round($this->loan_term);
	}
	
	public function cd_total_interest() {
		return $this->monthly_payment() * round($this->loan_term) - $this->loanable_amount();
	}
	/* CURRENT DOWNPAYMENT END */



	public function total_interest_savings() {
		return $this->nd_total_interest() - $this->cd_total_interest();
	}
	

	/* SAVINGS ON DOWNPAYMENT END */
	
	/* GRAPH START */
	public function g_total_monthly_payment() {
		return $this->nd_monthly_payment() * round($this->loan_term);
	}
	public function g_total_interest() {
		return $this->nd_total_interest();
	}
	public function g_total_principal() {
		return $this->g_total_monthly_payment() - $this->g_total_interest();
	}
	/* GRAPH END */
	
	/* PAYMENT BREAKDOWN START */
	
	public function breakdown() {
		$i = 0;
		$x = 1;
		$sum_amount = 0;
		$breakdown = array();
		$date = $this->loan_effectivity_date;
		for( $i = 1; $i <= ( round($this->loan_term) ); $i++) {
			/* MONTHLY BREAKDOWN START */
			$breakdown['monthly'][$i]['month'] = $i;
			if( $i == 1 ) {
				$breakdown['monthly'][$i]['beginning_balance'] = round( $this->loanable_amount(), 3 );
			} else {
				$breakdown['monthly'][$i]['beginning_balance'] = round( $breakdown['monthly'][($i - 1)]['ending_balance'], 3 );
			}

			$j = $i - 1;
			$cur_date = date('Y-m-d', strtotime( $date . '+1 month') );
			$breakdown['monthly'][$i]['target_date'] = format_date($cur_date);
			$breakdown['monthly'][$i]['paid_date'] = "";
			$breakdown['monthly'][$i]['interest'] = round( $breakdown['monthly'][$i]['beginning_balance'] * $this->compute_percentage( $this->loan_interest_rate ) / 12, 5 );
			$sum_amount = $sum_amount + $breakdown['monthly'][$i]['interest'];
			$breakdown['monthly'][$i]['principal'] = round( $this->monthly_payment() - $breakdown['monthly'][$i]['interest'], 6, PHP_ROUND_HALF_UP);
			$breakdown['monthly'][$i]['ending_balance'] = round( $breakdown['monthly'][$i]['beginning_balance'] - $breakdown['monthly'][$i]['principal'], 3 );
			/* MONTHLY BREAKDOWN END */
			$date = $cur_date;
			/* YEARLY BREAKDOWN START */
			if( ( $i % 12 ) == 0 ) {
				$breakdown['yearly'][$x]['year'] = $i / 12;
				if( ( $i / 12 ) == 1 ) {
					$breakdown['yearly'][$x]['beginning_balance'] = round( $this->loanable_amount(), 3 );
				} else {
					$breakdown['yearly'][$x]['beginning_balance'] = round( $breakdown['yearly'][$x  - 1]['ending_balance'], 3 );
				}
				$breakdown['yearly'][$x]['interest'] = $sum_amount;
				$breakdown['yearly'][$x]['principal'] = $this->monthly_payment() * 12 - $breakdown['yearly'][$x]['interest'];
				$breakdown['yearly'][$x]['ending_balance'] = round( $breakdown['yearly'][$x]['beginning_balance'] - $breakdown['yearly'][$x]['principal'], 3 );
				$sum_amount = 0;
				$x++;
			}
			/* YEARLY BREAKDOWN END */
		}
		return $breakdown;
	}
	
	public function dp_breakdown() {
		$i = 0;
		$x = 1;
		$sum_amount = 0;
		$breakdown = array();

		for( $i = 1; $i <= ( round($this->downpayment_term) ); $i++) {
			/* MONTHLY BREAKDOWN START */
			$breakdown['monthly'][$i]['month'] = $i;
			if( $i == 1 ) {
				$breakdown['monthly'][$i]['beginning_balance'] = round( $this->downpayment_amount(), 3 );
			} else {
				$breakdown['monthly'][$i]['beginning_balance'] = round( $breakdown['monthly'][($i - 1)]['ending_balance'], 3 );
			}
			$j = $i - 1;
			$breakdown['monthly'][$i]['target_date'] = format_date(date('Y-m-d', strtotime( $this->downpayment_effectivity_date. '+'.$j.' month') ));
			$breakdown['monthly'][$i]['paid_date'] = "";
			$breakdown['monthly'][$i]['interest'] = round( $breakdown['monthly'][$i]['beginning_balance'] * $this->compute_percentage( $this->downpayment_interest_rate ) / 12, 5 );
			$sum_amount = $sum_amount + $breakdown['monthly'][$i]['interest'];
			$breakdown['monthly'][$i]['principal'] = round( $this->dp_monthly_payment() - $breakdown['monthly'][$i]['interest'], 6, PHP_ROUND_HALF_UP);
			$breakdown['monthly'][$i]['ending_balance'] = round( $breakdown['monthly'][$i]['beginning_balance'] - $breakdown['monthly'][$i]['principal'], 3 );
			/* MONTHLY BREAKDOWN END */
			
			/* YEARLY BREAKDOWN START */
			if( ( $i % 12 ) == 0 ) {
				$breakdown['yearly'][$x]['year'] = $i / 12;
				if( ( $i / 12 ) == 1 ) {
					$breakdown['yearly'][$x]['beginning_balance'] = round( $this->downpayment_amount(), 3 );
				} else {
					$breakdown['yearly'][$x]['beginning_balance'] = round( $breakdown['yearly'][$x  - 1]['ending_balance'], 3 );
				}
				$breakdown['yearly'][$x]['interest'] = $sum_amount;
				$breakdown['yearly'][$x]['principal'] = $this->dp_monthly_payment() * 12 - $breakdown['yearly'][$x]['interest'];
				$breakdown['yearly'][$x]['ending_balance'] = round( $breakdown['yearly'][$x]['beginning_balance'] - $breakdown['yearly'][$x]['principal'], 3 );
				$sum_amount = 0;
				$x++;
			}
			/* YEARLY BREAKDOWN END */
		}
		return $breakdown;
	}

	/* PAYMENT BREAKDOWN END */
	private function compute_percentage( $value = 0 ) {
		if( $value ) {
			return $value / 100;
		}
	}
}
?>