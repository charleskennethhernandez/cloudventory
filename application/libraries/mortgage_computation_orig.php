<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mortgage_computation {

	public $property_value;
	
	public $downpayment_rate;
	
	public $interest_rate;
	
	public $loan_term;

	public $reservation_rate;
	 
	public function reconstruct( $property_value = 0, $downpayment_rate = 0, $reservation_rate = 0, $interest_rate = 0, $loan_term = 0 ) {
	
		if( $property_value && $downpayment_rate  &&  $interest_rate && $loan_term ) {
		
			$this->property_value = $property_value;

			$this->downpayment_rate = $downpayment_rate;
			
			$this->interest_rate = $interest_rate;
			
			$this->loan_term = $loan_term;

			$this->reservation_rate = $reservation_rate;

		}
	
	}

	/* INFORMATION SUMMARY START */
	public function loanable_amount() {
		
		return $this->property_value * ( 1 - $this->compute_percentage( $this->downpayment_rate + $this->reservation_rate) );
		
	}
	/***Bryan's code***/
	// public function get_remaining_days($target_date){

	// 	$timeleft = strtotime($target_date) - strtotime($this->current_date);
	// 	$daysleft = round((($timeleft/24)/60)/60); 
		
	// 	return $daysleft;
		
	// }
	/***Bryan's code***/

	public function downpayment_amount() {
	
		return $this->property_value * $this->compute_percentage( $this->downpayment_rate);
	
	}
	
	public function loan_term_in_month() {

		return $this->loan_term * 12;
		
	}
	
	public function monthly_payment() {
	
		return  ( $this->compute_percentage( $this->interest_rate ) / 12 * $this->loanable_amount() ) / ( 1 - pow( ( 1 + $this->compute_percentage( $this->interest_rate ) / 12 ) , - ( $this->loan_term * 12 ) ) );
	
	}
	
	public function total_interest() {
	
		return ( $this->monthly_payment() * $this->loan_term * 12 - $this->loanable_amount() );
	
	}
	
	public function recommended_monthly_income() {
	
		return round ( $this->monthly_payment() / 0.4,0 );
	
	}
	/* INFORMATION SUMMARY END */
	
	/* SAVINGS ON DOWNPAYMENT START */
	
	/* NO DOWNPAYMENT START */
	public function nd_monthly_payment() {
	
		return  ( $this->compute_percentage( $this->interest_rate ) / 12 * $this->property_value ) / ( 1 - pow( ( 1 + $this->compute_percentage( $this->interest_rate ) / 12 ) , - ( $this->loan_term * 12 ) ) );
	
	}
	
	public function nd_total_monthly_payment() {
	
		return $this->nd_monthly_payment() * $this->loan_term * 12;
	
	}
	
	public function nd_total_interest() {
	
		return $this->nd_monthly_payment() * $this->loan_term * 12 - $this->property_value;
	
	}
	/* NO DOWNPAYMENT END */
	
	/* CURRENT DOWNPAYMENT START */
	public function cd_total_monthly_payment() {
	
		return  $this->monthly_payment() * $this->loan_term * 12;
	
	}
	
	public function cd_total_interest() {
	
		return $this->monthly_payment() * $this->loan_term * 12 - $this->loanable_amount();
	
	}
	/* CURRENT DOWNPAYMENT END */
	
	public function total_interest_savings() {
	
		return $this->nd_total_interest() - $this->cd_total_interest();
	
	}
	
	/* SAVINGS ON DOWNPAYMENT END */
	
	/* GRAPH START */
	public function g_total_monthly_payment() {
	
		return $this->nd_monthly_payment() * $this->loan_term * 12;
	
	}
	
	public function g_total_interest() {
	
		return $this->nd_total_interest();
	
	}
	
	public function g_total_principal() {
	
		return $this->g_total_monthly_payment() - $this->g_total_interest();
	
	}
	/* GRAPH END */
	
	/* PAYMENT BREAKDOWN START */
	
	public function breakdown() {
	
		$i = 0;
		
		$x = 1;
		
		$sum_amount = 0;
		
		$breakdown = array();
		
		for( $i = 1; $i <= ( $this->loan_term * 12 ); $i++) {
		
			/* MONTHLY BREAKDOWN START */
			$breakdown['monthly'][$i]['month'] = $i;
		
			if( $i == 1 ) {

				$breakdown['monthly'][$i]['beggining_balance'] = round( $this->loanable_amount(), 3 );
				
			} else {

				$breakdown['monthly'][$i]['beggining_balance'] = round( $breakdown['monthly'][($i - 1)]['ending_balance'], 3 );
				
			}
			
			$breakdown['monthly'][$i]['interest'] = round( $breakdown['monthly'][$i]['beggining_balance'] * $this->compute_percentage( $this->interest_rate ) / 12, 5 );

			$sum_amount = $sum_amount + $breakdown['monthly'][$i]['interest'];
				
			$breakdown['monthly'][$i]['principal'] = round( $this->monthly_payment() - $breakdown['monthly'][$i]['interest'], 6, PHP_ROUND_HALF_UP);
				
			$breakdown['monthly'][$i]['ending_balance'] = round( $breakdown['monthly'][$i]['beggining_balance'] - $breakdown['monthly'][$i]['principal'], 3 );
			/* MONTHLY BREAKDOWN END */
			
			/* YEARLY BREAKDOWN START */
			if( ( $i % 12 ) == 0 ) {
			
				$breakdown['yearly'][$x]['year'] = $i / 12;
					
				if( ( $i / 12 ) == 1 ) {

					$breakdown['yearly'][$x]['beggining_balance'] = round( $this->loanable_amount(), 3 );
					
				} else {
					
					$breakdown['yearly'][$x]['beggining_balance'] = round( $breakdown['yearly'][$x  - 1]['ending_balance'], 3 );

				}
					
				$breakdown['yearly'][$x]['interest'] = $sum_amount;
						
				$breakdown['yearly'][$x]['principal'] = $this->monthly_payment() * 12 - $breakdown['yearly'][$x]['interest'];
						
				$breakdown['yearly'][$x]['ending_balance'] = round( $breakdown['yearly'][$x]['beggining_balance'] - $breakdown['yearly'][$x]['principal'], 3 );
					
				$sum_amount = 0;
				
				$x++;
				
			}
			/* YEARLY BREAKDOWN END */
			
		
		}
		
		return $breakdown;
	
	}
	
	/* PAYMENT BREAKDOWN END */
	
	
	private function compute_percentage( $value = 0 ) {
	
		if( $value ) {

			return $value / 100;

		}

	}

}

?>