<!DOCTYPE html>

<html>

<head>

<meta charset="UTF-8">
	
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<meta name="author" content="SFS Web Dev" />

<meta name="language" content="en" />

<title> <?php echo $this->config->item('system_name'); ?> <?php echo ( isset( $title ) ? ' | ' . $title  : '' ); ?> </title>

<?php echo theme_css('bootstrap.min.css', true);?>

<?php echo theme_css('bootstrap-theme.css', true);?>

<?php echo theme_css('jquery_validation/screen.css', true);?>

<?php echo theme_css('waitMe.min.css', true);?>

<link rel="shortcut icon" type="image/x-icon" href="#>">

<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">

<?php echo theme_js('jquery.js', true);?>

<?php echo theme_js('bootstrap.min.js', true);?>

<?php echo theme_js('jquery_validation/jquery.validate.min.js', true);?>

<?php echo theme_js('jquery_validation/additional-methods.min.js', true);?>

<?php echo theme_js('jquery_ui.js', true);?>

<?php echo theme_js('waitMe.min.js', true);?>

<!-- Use Html5Shiv in order to allow IE render HTML5 -->

<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

</head>

<body>

	<?php if( $this->session->userdata( 'account' ) ): ?>

	<nav class="navbar navbar-inverse navbar-static-top" role="navigation">

		<div class="navbar-header">
		
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			
			  <span class="sr-only"> Toggle navigation </span>
			  
			  <span class="icon-bar"></span>
			  
			  <span class="icon-bar"></span>
			  
			  <span class="icon-bar"></span>
			  
			</button>
			
			<a class="navbar-brand" href="<?php echo base_url('dashboard'); ?>"> Dashboard </a>
			
		</div>

		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		
			<ul class="nav navbar-nav navbar-left">
			
				<li class="dropdown"> 
				
					<a href="#" lass="dropdown-toggle" data-toggle="dropdown"> Description <b class="caret"></b></a>
					
					<ul class="dropdown-menu">

						<li> <a href="<?php echo base_url('room_description'); ?>"> Room Description </a> </li>

					</ul>
				
				</li>
				
				<li class="dropdown">

					<a href="#" lass="dropdown-toggle" data-toggle="dropdown"> Rates &amp; Availabilities <b class="caret"></b></a>
					
					<ul class="dropdown-menu">

						<li> <a href="<?php echo base_url('room_availability'); ?>"> Room Availability </a> </li>

					</ul>
					
				</li>

				<li class="dropdown"> 
				
					<a href="#" lass="dropdown-toggle" data-toggle="dropdown"> Bookings <b class="caret"></b></a>
					
					<ul class="dropdown-menu">

						<li> <a href="<?php echo base_url('reservations'); ?>"> Reservations </a> </li>

					</ul>
				
				</li>
				
				<li class="dropdown"> 
				
					<a href="#" lass="dropdown-toggle" data-toggle="dropdown"> Accounts <b class="caret"></b></a>
					
					<ul class="dropdown-menu">
					
						<li> <a href="<?php echo base_url('account_executives'); ?>"> Account Executives </a> </li>
						
						<?php if( $user->ut_id == 1 ): ?>
						
						<li> <a href="<?php echo base_url('users'); ?>"> Users </a> </li>
						
						<?php endif; ?>

					</ul>
				
				</li>
				
				<li class="dropdown"> 
				
					<a href="#" lass="dropdown-toggle" data-toggle="dropdown"> Settings <b class="caret"></b></a>
					
					<ul class="dropdown-menu">
					
						<li> <a href="<?php echo base_url('source_of_bookings'); ?>"> Source of bookings </a> </li>
						
						<li> <a href="<?php echo base_url('status'); ?>"> Status </a> </li>

					</ul>
				
				</li>

			</ul>
		
			<ul class="nav navbar-nav navbar-right">
			
				<li class="dropdown">
				
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"> My Account <b class="caret"></b></a>
					
					<ul class="dropdown-menu">
						
						<li> <a id="change_password" href="#"> Change password </a> </li>
						  
						<li> <a href="<?php echo base_url('security/logout'); ?>"> Logout </a> </li>
						  
					</ul>
				
				</li>
				
			</ul>
		
		</div>
		
	</nav>

	<?php endif; ?>

<div class="container" style="min-height:520px; height:auto;">

	<?php if (isset( $title ) ): ?>
	
		<div class="row">

			<div class="page-header">

			  <h1> 
			  
				<?php echo $title; ?>
			  
				<?php if( $title != 'Dashboard' && $title != 'Room availabilities' ): ?>
				  
					<a id="add-btn" class="btn btn-default pull-right" href="#"> <i class="glyphicon glyphicon-plus-sign"></i> </a> 
					
				<?php endif; ?>
				
				</h1>

			</div>
			
		</div>
	
	<?php endif; ?>
	
	<?php if( ! empty( $title ) ): ?>
	
		<?php if ( validation_errors() ): ?>
		
		<div class="row">

			<div class="alert alert-danger">

				<a class="close" data-dismiss="alert">×</a>

				<?php echo validation_errors();?>

			</div>

		</div>
		
		<?php endif; ?>
	
		<?php if ( $this->session->flashdata('message') ):?>
		
		<div class="row">

			<div class="alert alert-info">

				<a class="close" data-dismiss="alert">×</a>

				<?php echo $this->session->flashdata('message');?>

			</div>

		</div>

		<?php endif;?>

		<?php if ( $this->session->flashdata('error') ): ?>

		<div class="row">

			<div class="alert alert-danger">

				<a class="close" data-dismiss="alert">×</a>

				<?php echo $this->session->flashdata('error');?>

			</div>

		</div>

		<?php endif;?>

		<?php if ( !empty($error) ): ?>

		<div class="row">

			<div class="alert alert-danger">

				<a class="close" data-dismiss="alert">×</a>

				<?php echo $error;?>

			</div>

		</div>

		<?php endif;?>
	
	<?php endif; ?>