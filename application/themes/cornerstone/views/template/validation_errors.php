<!-- VALIDATION ERRORS START -->
<?php if ( $this->session->flashdata('message') ):?>
		
<div class="row">

	<div class="alert alert-info">

		<a class="close" data-dismiss="alert">×</a>

		<?php echo $this->session->flashdata('message');?>

	</div>

</div>

<?php endif;?>

<?php if ( $this->session->flashdata('error') ): ?>

<div class="row">

	<div class="alert alert-danger">

		<a class="close" data-dismiss="alert">×</a>

		<?php echo $this->session->flashdata('error');?>

	</div>

</div>

<?php endif;?>
 
<?php if ( !empty( $error ) ): ?>

<div class="row">

	<div class="alert alert-danger">

		<a class="close" data-dismiss="alert">×</a>

		<?php echo $error;?>

	</div>

</div>

<?php endif;?>
<!-- VALIDATION ERRORS END -->