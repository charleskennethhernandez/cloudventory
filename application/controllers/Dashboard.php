<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard extends Back_Controller {
	private $folder = 'dashboard';
	public function __construct() {	
		parent::__construct();		
		$this->load->model( array('Sales_order_model','Sales_order_item_model','Search_model','Inventory_model','Customer_model') );
	}
	public function index( ) {
		$data['title'] = 'Dashboard';
		$params['folder'] = $this->folder;
		
		$q_params['where'] = array(
			'sales_orders.date_created >=' => date('Y-m-d').' 00:00:00',
			'sales_orders.date_created <=' => date('Y-m-d').' 23:59:59',
		);
		$params['today_sales'] = $this->Sales_order_model->get_sales($q_params);

		$q_params['where'] = array(
			'sales_orders.date_created >=' => date('Y-m-01').' 00:00:00',
			'sales_orders.date_created <=' => date('Y-m-t').' 23:59:59',
		);
		$params['monthly_sales'] = $this->Sales_order_model->get_sales($q_params);

		$params['total_sales'] = $this->Sales_order_model->get_sales();

		$q_params['average'] = 'monthly';
		$params['average_sale'] = $this->Sales_order_model->get_sales($q_params);
		$a = 0;
		for ( $i=0; $i < 14 ; $i++ ) { 
			$wk = 'current';
			$d_params['where'] = array(
				'sales_orders.date_created >=' => date('Y-m-d', strtotime("-".$i." days")).' 00:00:00',
				'sales_orders.date_created <=' => date('Y-m-d', strtotime("-".$i." days")).' 23:59:59',
			);
			$result = $this->Sales_order_model->get_sales($d_params);

			if ($i > 6) {
				$wk = 'last';
				if ($a == 6) {
					$a = -1;
				}
				$a++;
			} else {
				$a = $i;
				$days[] = date('D', strtotime("-".$i." days"));
			}

			if (isset($result['grand_total']) && !empty($result['grand_total'])) {
				$daily_sales[$wk][$a] = $result['grand_total'];
			} else {
				$daily_sales[$wk][$a] = "0.00";
			}
		}

		$params['current_week_sales'] =  json_encode(array_reverse($daily_sales['current']));
		$params['last_week_sales'] =  json_encode(array_reverse($daily_sales['last']));
		
		$sales_params['where'] = array(
			'sales_orders.date_created >=' => date('Y-m-d').' 00:00:00',
			'sales_orders.date_created <=' => date('Y-m-d').' 23:59:59',
		);
		$params['chunk_sales_orders'] = array();
		$sales_orders = $this->Sales_order_model->get_all_sales_order($sales_params);
		// echo $this->db->last_query();
		// echo "<pre>";
		// print_r($sales_orders);
		// die();
		if ($sales_orders) {
			$params['chunk_sales_orders'] = array_chunk(array_reverse($sales_orders), 7);
		}

		$params['sales_orders_count'] = count($sales_orders);

		$sales_params['where'] = array(
			'sales_orders.date_created >=' => date('Y-m-d', strtotime("-7 days")).' 00:00:00',
			'sales_orders.date_created <=' => date('Y-m-d').' 23:59:59',
		);

		$params['days'] = json_encode(array_reverse($days));
		$params['sales_orders_count_week'] = count($this->Sales_order_model->get_all_sales_order($sales_params));

		$this->view( $this->folder . '/dashboard', $params );
	}
}