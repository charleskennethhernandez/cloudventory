<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sales_orders extends Back_Controller {
	private $folder = 'sales_orders';
	private $fields = array(
		array(
			'field'   => 'id', 
			'label'   => 'Id', 
			'rules'   => 'trim|int'
		),
		array(
			'field'   => 'overall_total_cost', 
			'label'   => 'Total Cost', 
 			'rules'   => 'trim|required'
 		),
	);
	public function __construct() {
		parent::__construct();
		$this->load->model( array('Sales_order_model','Sales_order_item_model','Search_model','Inventory_model','Customer_model') );
	}
	public function index($code = 0, $sort_by = 'sales_orders.id', $sort_order = 'DESC', $limit = 10 ) {
		$data['folder'] = $this->folder;
		$data['title'] = "Sales orders";
		$data['description'] = "Sales orders Page";
		/* Sales_order INITIAL VALUES START */
		$data['sales_orders'] = $this->Sales_order_model->get_all_sales_order();
		
		/* PASS PARAMETER TO THE CENTER STAGE START */
		$data['code'] = $code;
		$data['sort_by'] = $sort_by;
		$data['sort_order'] = $sort_order;
		$data['limit'] = $limit;
		/* PASS PARAMETER TO THE CENTER STAGE END */
		
		/* CALL PAGINATION START */
		$this->load->library('pagination');
		$config['base_url']			= base_url( '/sales_orders/form_ajax/' . $code . '/'  . $sort_by . '/' . $sort_order . '/' . $limit );
		$config['total_rows']		= $this->Sales_order_model->get_all_sales_order( array(), true );
		$config['per_page']			= $limit;
		$config['uri_segment']		= 7;
		$this->pagination->initialize( $config );
		
		$data['sales_order_pagination'] = $this->pagination->create_ajax_links('sales_order_list_table_container');
		/* CALL PAGINATION END */

		/* Sales_order INITIAL VALUES END */
		$this->form_validation->set_rules( $this->fields );

		if( $this->form_validation->run() ) {
			// $post = $this->input->post();
			// $post['created_by'] = $this->account['id'];
			// $id = $this->Sales_order_model->save( $post );

			$data = $this->input->post();
			
			//save customer 
			$is_customer_exists = $data['is_customer_exists'];

			if ($is_customer_exists) {
				$sales_orders_data['customer_id'] = $data['customer_id'];
			} else {
				$customers['firstname'] = $data['firstname'];
				$customers['middlename'] = $data['middlename'];
				$customers['lastname'] = $data['lastname'];
				$customers['created_by'] =  $this->account['id'];
				$customer_id = $this->Customer_model->save($customers,@$customers['id']);
				$sales_orders_data['customer_id'] = $customer_id;
			}

			$sales_orders_data['id'] = @$data['id'];
			$sales_orders_data['total_due'] = $data['total_due'];
			$sales_orders_data['grand_total'] = $data['overall_total_cost'];
			$sales_orders_data['total_discount'] = $data['overall_total_discount'];
			$sales_orders_data['amount_paid'] = $data['amount_paid'];
			$sales_orders_data['created_by'] = $this->account['id'];

			if( $data['total_due'] == "0.00" ){ 
				$status_id = 1; 
			} else {
				$status_id = 2; 
			};

			$sales_orders_data['status'] = $status_id;

			$order_items = array();

			$data['sales_order_id'] = $this->Sales_order_model->save($sales_orders_data);
			$order_items_id = array();
			if ($data['inventory_id']) {
				foreach ($data['inventory_id'] as $key => $inventory_id) {
					$inventories = array();
					$order_items['id'] = @$data['sales_order_item_id'][$inventory_id];
					$order_items['inventory_id'] = $inventory_id;
					$order_items['quantity'] = $data['quantity'][$inventory_id];
					$order_items['unit_cost'] = $data['unit_cost'][$inventory_id];
					$order_items['discount'] = $data['discount'][$inventory_id];
					$order_items['total_cost'] = $data['total_cost'][$inventory_id];
					$order_items['sales_order_id'] = $data['sales_order_id'];
					// echo "<pre>";
					// print_r($order_items);
					// die();
					$inventory_quantity = $data['inventory_quantity'][$inventory_id];
					$this->Sales_order_item_model->save($order_items);
					if (empty($order_items['id'])) {
						$inventories['id'] = $inventory_id;
						$inventories['quantity'] = $inventory_quantity - $order_items['quantity'];
						if (empty($inventories['quantity'])) {
							$inventories['status_id'] = 0;
						}
						$this->Inventory_model->save($inventories);
					}
					if (isset($data['sales_order_item_id'][$inventory_id]) || !empty($data['sales_order_item_id'][$inventory_id])) {
						$order_items_id[] = $order_items['id'];
					}
				}

				if (!empty($order_items_id)) {
					$sales_order_items = $this->Sales_order_item_model->get_all_sales_order_item(array('sales_order_id' => $data['sales_order_id']));
					foreach ($sales_order_items as $key => $sales_order_item) {
						if (!in_array($sales_order_item['id'], $order_items_id)) {

							// $inventories['id'] = $inventory_id;
							// $inventories['quantity'] = $inventory_quantity + $sales_order_item['quantity'];
							// $inventories['status_id'] = 1;
							// $this->Inventory_model->save($inventories);

							$this->Sales_order_item_model->delete( $sales_order_item['id'] );
						}
					}
				}
			}

			if( ! empty( $post['id'] ) ) {
				$action = 'update';
			} else {
				$action = 'save';
			}
			if( $this->input->is_ajax_request() ) {
				$this->response( 'save' );
			} else {
				$this->session->set_flashdata( 'message', 'Sales order has been ' . $action . 'd!' );
				redirect( 'dashboard' );
			}
		} else {
			$this->view('dashboard/dashboard', $data );
		}
	}
	public function sales_cart() {
		$data = $this->input->post();
		$params = array('id' => $data['id']);
		$data['inventory'] = $this->Inventory_model->get_all_inventory($params);

		$data['view'] = $this->load->view('sales_orders/sales_cart',$data,true);
		$data['item'] = $this->load->view('sales_orders/single_item',$data,true);

		echo json_encode($data);
	}
	public function form($code = 0, $sort_by = 'inventories.id', $sort_order = 'DESC', $limit = 5 ) {
		$sales_order_id = 0;
		$post = $this->input->post();
		$data['sales_order'] = array();
		$data['sales_order_items'] = array();

		if ($post['id']) {
			$sales_order_id = $post['id'];
			$data['sales_order'] = $this->Sales_order_model->get_all_sales_order(array('id' => $sales_order_id));
			$data['sales_order_items'] = $this->Sales_order_item_model->get_all_sales_order_item(array('sales_order_id' => $sales_order_id,'limit' => '1000','offset' => '0'));
			$data['view'] = $post['view'];
		}

		/* Sales_order INITIAL VALUES START */
		
		$data['inventories'] = $this->Inventory_model->get_all_inventory(array('limit' => 5,'offset' => 0));
		$data['customers'] = $this->Customer_model->get_all_customer(array('limit' => 50,'offset' => 0));
		$data['form'] = 1;
		
		/* PASS PARAMETER TO THE CENTER STAGE START */
		$data['code'] = $code;
		$data['sort_by'] = $sort_by;
		$data['sort_order'] = $sort_order;
		$data['limit'] = $limit;
		/* PASS PARAMETER TO THE CENTER STAGE END */
		
		/* CALL PAGINATION START */
		$this->load->library('pagination');
		$config['base_url']			= base_url( '/inventories/form_ajax/1/' . $code . '/'  . $sort_by . '/' . $sort_order . '/' . $limit );
		$config['total_rows']		= $this->Inventory_model->get_all_inventory( array(), true );
		$config['per_page']			= $limit;
		$config['uri_segment']		= 8;
		$this->pagination->initialize( $config );
		
		$data['pagination'] = $this->pagination->create_ajax_links('list_table_container');
		/* CALL PAGINATION END */

		/* Sales_order INITIAL VALUES END */
		$this->form_validation->set_rules( $this->fields );
		echo $this->load->view('sales_orders/sales_order_form', $data , true);
	}
	public function deactivate( $id = 0 ) {
		if( $id ) {
		
			$this->Sales_order_model->deactivate( $id );
			
			$this->session->set_flashdata( 'message', 'Sales_order has been deactivated' );
			
			redirect( 'dashboard' );
		
		}
		
		$id = $this->input->post( 'id' );
		
		if( $id ) {
		
			$this->Sales_order_model->deactivate( $id );
			
			if( $this->input->is_ajax_request() ) {
			
				$this->response( 'deactivate' );
			
			} else {
			
				$this->session->set_flashdata( 'message', 'Sales_order has been deactivated!' );
				
				redirect( 'dashboard' );
			
			}
		
		}
	}
	
	public function delete_selected( $id = 0 ) {
		$post = $this->input->post();
		if( ! empty( $post['selected_datas'] ) ) {
			foreach( $post['selected_datas'] as $key => $id ) {
				$this->Sales_order_model->delete( $id );
			}
		}
		if( $this->input->is_ajax_request() ) {
			$this->response( 'delete' );
		} else {
			$this->session->set_flashdata( 'message', 'Sales_order has been deleted!' );
			redirect( 'dashboard' );
		}
	}

	public function information() {
		$id = $this->input->post( 'id' );
		if( $id ) {
			$sales_order_information = array();
			$sales_order_information = $this->Sales_order_model->get_all_sales_order(array( 'id' => $id ));
			$response['view'] = $this->partial('sales_orders/sales_order_form',$sales_order_information,true);
			$response['json'] = json_encode( $sales_order_information );
			echo json_encode($response);
		}
	}

	private function response( $action = 'save', $code = 0, $sort_by = 'sales_orders.id', $sort_order = 'DESC', $limit = 10 ) {
		//GET CUSTOMER TABLE WITH RESULT
		$result['sales_orders'] = $this->Sales_order_model->get_all_sales_order( array('is_prospect'=>'1'));
		/* PASS PARAMETER TO THE CENTER STAGE START */
		$result['code'] = $code;
		$result['sort_by'] = $sort_by;
		$result['sort_order'] = $sort_order;
		$result['limit'] = $limit;
		/* PASS PARAMETER TO THE CENTER STAGE END */
		
		/* CALL PAGINATION START */
		$this->load->library('pagination');
		$config['base_url']			= base_url( '/sales_orders/form_ajax/' . $code . '/'  . $sort_by . '/' . $sort_order . '/' . $limit );
		$config['total_rows']		= $this->Sales_order_model->get_all_sales_order( array(), true );
		$config['per_page']			= $limit;
		$config['uri_segment']		= 7;
		$this->pagination->initialize( $config );
		
		$result['pagination'] 	= $this->pagination->create_ajax_links('sales_order_list_table_container');
		/* CALL PAGINATION END */
		$list_table = $this->partial( 'sales_orders/sales_order_list_table', $result, true );
		$data['action'] = $action;
		$data['list_table'] = $list_table;
		$data['success'] = 1;
		$this->update_developer( $data );
	}

	public function form_ajax( $code = 0, $sort_by = 'sales_orders.id', $sort_order = 'DESC', $limit = 10, $offset = 0 ) {
		$term = array();
		if ( $code ) {
			$term	= $this->Search_model->get_term( $code );
			$term	= json_decode( $term );
		}
		//GET ALL CUSTOMER DEPENDING ON CONDITIONS
		$data['sales_orders'] 	= $this->Sales_order_model->get_all_sales_order(
			array(
				'search'		=> (object) $term,
				'sort_by' 		=> $sort_by,
				'sort_order' 	=> $sort_order,
				'limit' 		=> $limit,
				'offset' 		=> $offset
			)
		);
		// GET TOTAL BY CONTIDTION
		$total = $this->Sales_order_model->get_all_sales_order( 
			array( 
				'search'		=> (object) $term,
				'sort_by'		=> $sort_by, 
				'sort_order'	=> $sort_order
			), 
			true 
		);
		/* PASS PARAMETER TO THE CENTER STAGE START */
		$data['code'] = $code;
		$data['sort_by'] = $sort_by;
		$data['sort_order'] = $sort_order;
		$data['limit'] = $limit;
		/* PASS PARAMETER TO THE CENTER STAGE END */
		
		/* CALL PAGINATION START */
		$this->load->library('pagination');
		$config['base_url']			= site_url( '/sales_orders/form_ajax/' . $code . '/'  . $sort_by . '/' . $sort_order . '/' . $limit );
		$config['total_rows']		= $total;
		$config['per_page']			= $limit;
		$config['uri_segment']		= 7;
		$this->pagination->initialize( $config );
		
		$data['pagination'] 	= $this->pagination->create_ajax_links('sales_order_list_table_container');
		/* CALL PAGINATION END */
		
		//SHOW SOME LOVE
		echo $this->partial( 'sales_orders/sales_order_list_table', $data );
	}

	public function search($code = 0, $sort_by = 'sales_orders.id', $sort_order = 'DESC', $limit = 10, $offset = 0) {
		$post = $this->input->post();
		$term = "";
		if( $post ) {
			$term	= json_encode( $post );
			$code	= $this->Search_model->record_term( $term );
			$result['code']	= $code;			
			$term	= (object) $post;			
		} elseif ( $code ) {
			$term	= $this->Search_model->get_term( $code );			
			$term	= json_decode( $term );		
		}
		unset($post['type']);
		$data['term'] = $term;
		$params = array(
			'search'		=> (object) $term,
			'sort_by' 		=> $sort_by,
			'sort_order' 	=> $sort_order,
			'limit' 		=> $limit,
			'offset' 		=> $offset,
		);
		$result['sales_orders'] = $this->Sales_order_model->get_all_sales_order($params);
		//echo $this->db->last_query();
		$config['total_rows'] = $this->Sales_order_model->get_all_sales_order( $params, true );

		$result['sort_order'] = $sort_order;		
		$result['limit'] = $limit;		
		$result['code'] = $code;		
		
		$this->load->library('pagination');
		$config['base_url']			= base_url( '/sales_orders/form_ajax/' . $code . '/'  . $sort_by . '/' . $sort_order . '/' . $limit );
		$config['per_page']			= $limit;
		$config['uri_segment']		= 7;
		$this->pagination->initialize( $config );
		$result['pagination'] 	= $this->pagination->create_ajax_links('sales_order_list_table_container');
		$list_table = $this->partial( 'sales_orders/sales_order_list_table', $result, true );
		$data['action'] = 'search';
		$data['list_table'] = $list_table;
		$this->update_developer( $data );
	}
}