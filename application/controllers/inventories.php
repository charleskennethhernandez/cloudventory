<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Inventories extends Back_Controller {
	private $folder = 'inventories';
	private $fields = array(
		array(
			'field'   => 'id', 
			'label'   => 'Id', 
			'rules'   => 'trim|int'
		),
		array(
			'field'   => 'name', 
			'label'   => 'name', 
 			'rules'   => 'trim|required'
 		),
	);
	public function __construct() {
		parent::__construct();
		$this->load->model( array('Sales_order_item_model','Inventory_model','Search_model','Brand_model','Inventory_category_model') );
	}
	public function index($form = 0, $code = 0, $sort_by = 'inventories.id', $sort_order = 'DESC', $limit = 10 ) {
		$data['folder'] = $this->folder;
		$data['title'] = "Inventories";
		$data['description'] = "Inventories Page";
		/* Inventory INITIAL VALUES START */
		$data['inventories'] = $this->Inventory_model->get_all_inventory();
		$data['brands'] = $this->Brand_model->get_all_brand(array('limit' => 50,'offset' => 0));
		$data['categories'] = $this->Inventory_category_model->get_all_inventory_category(array('limit' => 50,'offset' => 0));
		/* PASS PARAMETER TO THE CENTER STAGE START */
		$data['code'] = $code;
		$data['form'] = $form;
		$data['sort_by'] = $sort_by;
		$data['sort_order'] = $sort_order;
		$data['limit'] = $limit;
		/* PASS PARAMETER TO THE CENTER STAGE END */
		
		/* CALL PAGINATION START */
		$this->load->library('pagination');
		$config['base_url']			= base_url( '/inventories/form_ajax/0/'  . $code . '/'  . $sort_by . '/' . $sort_order . '/' . $limit );
		$config['total_rows']		= $this->Inventory_model->get_all_inventory( array(), true );
		$config['per_page']			= $limit;
		$config['uri_segment']		= 8;
		$this->pagination->initialize( $config );
		
		$data['pagination'] = $this->pagination->create_ajax_links('list_table_container');
		/* CALL PAGINATION END */

		/* Inventory INITIAL VALUES END */
		$this->form_validation->set_rules( $this->fields );

		if( $this->form_validation->run() ) {
			$post = $this->input->post();
			$post['created_by'] = $this->account['id'];
			$id = $this->Inventory_model->save( $post );
			if( ! empty( $post['id'] ) ) {
				$action = 'update';
			} else {
				$action = 'save';
			}
			if( $this->input->is_ajax_request() ) {
				$this->response( 'save' );
			} else {
				$this->session->set_flashdata( 'message', 'Inventory has been ' . $action . 'd!' );
				redirect( 'dashboard' );
			}
		} else {
			$this->view('dashboard/dashboard', $data );
		}
	}
	
	public function deactivate( $id = 0 ) {
		if( $id ) {
		
			$this->Inventory_model->deactivate( $id );
			
			$this->session->set_flashdata( 'message', 'Inventory has been deactivated' );
			
			redirect( 'dashboard' );
		
		}
		
		$id = $this->input->post( 'id' );
		
		if( $id ) {
		
			$this->Inventory_model->deactivate( $id );
			
			if( $this->input->is_ajax_request() ) {
			
				$this->response( 'deactivate' );
			
			} else {
			
				$this->session->set_flashdata( 'message', 'Inventory has been deactivated!' );
				
				redirect( 'dashboard' );
			
			}
		
		}
	}
	
	public function delete_selected( $id = 0 ) {
		$post = $this->input->post();
		if( ! empty( $post['selected_datas'] ) ) {
			foreach( $post['selected_datas'] as $key => $id ) {
				$this->Inventory_model->delete( $id );
			}
		}
		if( $this->input->is_ajax_request() ) {
			$this->response( 'delete' );
		} else {
			$this->session->set_flashdata( 'message', 'Inventory has been deleted!' );
			redirect( 'dashboard' );
		}
	}

	public function information($code = 0, $sort_by = 'inventories.id', $sort_order = 'DESC', $limit = 10) {
		$id = $this->input->post( 'id' );
		if( $id ) {
			$info['code'] = $code;
			$info['sort_by'] = $sort_by;
			$info['sort_order'] = $sort_order;
			$info['limit'] = $limit;
		
			$this->load->library('pagination');
			$config['base_url']			= base_url( '/sales_order_items/form_ajax/'  . $code . '/'  . $sort_by . '/' . $sort_order . '/' . $limit );
			$config['total_rows']		= $this->Sales_order_item_model->get_all_sales_order_item( array('inventory_id' => $id), true );
			$config['per_page']			= $limit;
			$config['uri_segment']		= 7;
			$this->pagination->initialize( $config );

			$inventory_information = array();

			$data = $this->Inventory_model->get_all_inventory(array( 'id' => $id ));
			$data['brands'] = $this->Brand_model->get_all_brand(array('limit' => 50,'offset' => 0));
			$data['categories'] = $this->Inventory_category_model->get_all_inventory_category(array('limit' => 50,'offset' => 0));
			$info['sales_order_items'] = $this->Sales_order_item_model->get_all_sales_order_item(array('inventory_id' => $id));

			$response['view'] = $this->partial('inventories/inventory_form',$data,true);
			$response['view_details'] = $this->partial('inventories/inventory_view',$info,true);
			$response['json'] = json_encode( $inventory_information );
			echo json_encode($response);
		}
	}

	private function response( $action = 'save', $form = 0, $code = 0, $sort_by = 'inventories.id', $sort_order = 'DESC', $limit = 10 ) {
		//GET CUSTOMER TABLE WITH RESULT
		$result['inventories'] = $this->Inventory_model->get_all_inventory();
		$result['brands'] = $this->Brand_model->get_all_brand(array('limit' => 50,'offset' => 0));
		$result['categories'] = $this->Inventory_category_model->get_all_inventory_category(array('limit' => 50,'offset' => 0));

		/* PASS PARAMETER TO THE CENTER STAGE START */
		$result['code'] = $code;
		$result['form'] = $form;
		$result['sort_by'] = $sort_by;
		$result['sort_order'] = $sort_order;
		$result['limit'] = $limit;
		/* PASS PARAMETER TO THE CENTER STAGE END */
		
		/* CALL PAGINATION START */
		$this->load->library('pagination');
		$config['base_url']			= base_url( '/inventories/form_ajax/' . $form . '/' . $code . '/'  . $sort_by . '/' . $sort_order . '/' . $limit );
		$config['total_rows']		= $this->Inventory_model->get_all_inventory( array(), true );
		$config['per_page']			= $limit;
		$config['uri_segment']		= 8;
		$this->pagination->initialize( $config );
		
		$result['pagination'] 	= $this->pagination->create_ajax_links('list_table_container');
		/* CALL PAGINATION END */
		
		$list_table = $this->partial( 'inventories/inventory_list_table', $result, true );
		$data['action'] = $action;
		$data['list_table'] = $list_table;
		$data['success'] = 1;
		$this->update_developer( $data );
	}

	public function form_ajax( $form = 0, $code = 0, $sort_by = 'inventories.id', $sort_order = 'DESC', $limit = 10, $offset = 0 ) {
		$term = array();
		if ( $code ) {
			$term	= $this->Search_model->get_term( $code );
			$term	= json_decode( $term );
		}
		//GET ALL CUSTOMER DEPENDING ON CONDITIONS
		$data['inventories'] 	= $this->Inventory_model->get_all_inventory(
			array(
				'search'		=> (object) $term,
				'sort_by' 		=> $sort_by,
				'sort_order' 	=> $sort_order,
				'limit' 		=> $limit,
				'offset' 		=> $offset
			)
		);

		// GET TOTAL BY CONTIDTION
		$total = $this->Inventory_model->get_all_inventory( 
			array( 
				'search'		=> (object) $term,
				'sort_by'		=> $sort_by, 
				'sort_order'	=> $sort_order
			), 
			true 
		);
		/* PASS PARAMETER TO THE CENTER STAGE START */
		$data['form'] = $form;
		$data['code'] = $code;
		$data['sort_by'] = $sort_by;
		$data['sort_order'] = $sort_order;
		$data['limit'] = $limit;
		/* PASS PARAMETER TO THE CENTER STAGE END */
		
		/* CALL PAGINATION START */
		$this->load->library('pagination');
		$config['base_url']			= site_url( '/inventories/form_ajax/' . $form . '/' . $code . '/'  . $sort_by . '/' . $sort_order . '/' . $limit );
		$config['total_rows']		= $total;
		$config['per_page']			= $limit;
		$config['uri_segment']		= 8;
		$this->pagination->initialize( $config );
		
		$data['pagination'] 	= $this->pagination->create_ajax_links('list_table_container');
		/* CALL PAGINATION END */
		
		//SHOW SOME LOVE
		echo $this->partial( 'inventories/inventory_list_table', $data );
	}

	public function search($form = 0, $code = 0, $sort_by = 'inventories.id', $sort_order = 'DESC', $limit = '10', $offset = 0) {
		$post = $this->input->post();
		$result['form'] = @$post['form'];
		$term = "";
		if( $post ) {
			$term	= json_encode( $post );
			$code	= $this->Search_model->record_term( $term );
			$result['code']	= $code;			
			$term	= (object) $post;			
		} elseif ( $code ) {
			$term	= $this->Search_model->get_term( $code );			
			$term	= json_decode( $term );		
		}
		unset($post['type']);
		$data['term'] = $term;
		$params = array(
			'search'		=> (object) $term,
			'sort_by' 		=> $sort_by,
			'sort_order' 	=> $sort_order,
			'limit' 		=> $limit,
			'offset' 		=> $offset,
		);
		$result['inventories'] = $this->Inventory_model->get_all_inventory($params);
		
		$config['total_rows'] = $this->Inventory_model->get_all_inventory( $params, true );

		$result['sort_order'] = $sort_order;		
		$result['limit'] = $limit;		
		$result['code'] = $code;		
		$this->load->library('pagination');
		$config['base_url']			= base_url( '/inventories/form_ajax/' . $form . '/' . $code . '/'  . $sort_by . '/' . $sort_order . '/' . $limit );
		$config['per_page']			= $limit;
		$config['uri_segment']		= 8;
		$this->pagination->initialize( $config );
		$result['pagination'] 	= $this->pagination->create_ajax_links('list_table_container');
		$list_table = $this->partial( 'inventories/inventory_list_table', $result, true );
		$data['action'] = 'search';
		$data['list_table'] = $list_table;

		$this->update_developer( $data );
	}

	public function extract_inventory() {
		#error_reporting(E_ALL);

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()
            		->setTitle("Worksheet")
            		->setSubject("Worksheet")
            		->setDescription("none");

		// Description
		//$objPHPExcel->getProperties()->setTitle("export")->setDescription("none");

        // Default Style			
		$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')->setSize(10);

		// Column
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A1', 'ID')
					->setCellValue('B1', 'Name')
					->setCellValue('C1', 'Description')
					->setCellValue('D1', 'SellingPrice') //selling
					->setCellValue('E1', 'CapitalPrice') //capital
					->setCellValue('F1', 'DealerPrice') //dealer
					->setCellValue('G1', 'Category')
					->setCellValue('H1', 'Brand')
					->setCellValue('I1', 'Status')
					->setCellValue('J1', 'Quantity');
		$style = array(
	        'alignment' => array(
	            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        )
	    );

	    // $objPHPExcel->getActiveSheet()->getStyle("B2:P2")->applyFromArray($style);
		$objPHPExcel->getActiveSheet()
		    ->getColumnDimension('A')
		    ->setAutoSize(true);

		$inventories =  $this->Inventory_model->get_all_record();
		// echo "<pre>";
		// print_r($inventories);
		// die();
		$row = 2;
		foreach($inventories as $key => $inventory ){
			$objPHPExcel->setActiveSheetIndex(0)            				
		        ->setCellValue('A'.$row, $inventory['id'])
				->setCellValue('B'.$row, $inventory['name'])
				->setCellValue('C'.$row, $inventory['description'])
				->setCellValue('D'.$row, $inventory['marked_price'])
				->setCellValue('E'.$row, $inventory['actual_price'])
				->setCellValue('F'.$row, $inventory['dealer_price'])
				->setCellValue('G'.$row, get_value_field($inventory['category_id'],'inventory_categories','name'))
				->setCellValue('H'.$row, get_value_field($inventory['brand_id'],'brands','name'))
				->setCellValue('I'.$row, $inventory['status_id'])
				->setCellValue('J'.$row, $inventory['quantity']);
			$row++;	
		}
		$objPHPExcel->setActiveSheetIndex(0);
		
		if (ob_get_contents()) ob_end_clean();

		#$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		// $objWriter->setDelimiter("|"); 
		
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Inventory'.date("YmdH").'.csv"');
		header('Cache-Control: max-age=0');
		
		$objWriter->save('php://output');
	}

	public function upload_csv_inventories() {
		
		if( !empty($_FILES) ) {
			if( $_FILES['file']['error'] ) {
				$this->session->set_flashdata('error','Please select a file to upload!');
				redirect("inventories");
			} else {

				$this->extract_inventories();
			}
			
		} else {
			$this->session->set_flashdata('error','Please select a file to upload!');
			redirect("inventories" );
		}
	}

	public function extract_inventories() {
		error_reporting(E_ALL);
		
		ini_set('max_execution_time', 3000);
		set_time_limit(0);
		$save	= array();
		$tmpName  = $_FILES['file']['tmp_name'];
		$row 	= 1;
		$count 	= 0;
		if ( ( $handle = fopen( $tmpName, 'r' ) ) !== FALSE ) {
			while ( ($data = fgetcsv($handle, 0, ",") ) !== FALSE ) {
				if( $row >= 2 )  {
					// if (empty($data[0])) {
					// 	continue;
					// }
					$datum = array(
					   'id' => $data[0],
					   'name' => $data[1],
					   'description' => $data[2],
					   'marked_price' => $data[3], // selling
					   'actual_price' => $data[4], // capital 
					   'dealer_price' => $data[5], // dealer
					   'category_id' => get_value_field($data[6],'inventory_categories','id','name'),
					   'brand_id' => get_value_field($data[7],'brands','id','name'),
					   'status_id' => $data[8],
					   'quantity' => $data[9],
					   'date_updated' => date('Y-m-d h:i:s'),
					);

					$table = "inventories";
					$query = $this->db->get_where($table, array('id' => $data[0]));
					$inventories = $query->row_array();

					if (empty($datum)) {
						continue;
					}

					if (isset($inventories) && !empty($inventories)) {
						$this->db->where('id', $inventories['id']);
						$this->db->update($table, $datum); 
						$document_id = $inventories['id'];
					} else {
					   	$datum['date_created'] = date('Y-m-d h:i:s');
						$this->db->insert($table, $datum); 
						$document_id = $this->db->insert_id();
					}
					// echo $this->db->last_query();
					// echo "<br>";
				}
				$row++;
			}
			// die();
			fclose( $handle );
			$this->session->set_flashdata('success','Data has been imported successfuly!');
			redirect("inventories");
		}
	}
}