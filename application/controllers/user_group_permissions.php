<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_group_permissions extends Back_Controller {
	private $folder = 'user_group_permissions';
	private $fields = array(
		array(
			'field'   => 'id', 
			'label'   => 'Id', 
			'rules'   => 'trim|int'
		),
		array(
			'field'   => 'name', 
			'label'   => 'Name', 
 			'rules'   => 'trim|required'
 		),
	);
	public function __construct() {
		parent::__construct();
		$this->load->model( array('User_group_permission_model','Search_model') );
	}
	public function index($code = 0, $sort_by = 'user_group_permissions.id', $sort_order = 'DESC', $limit = 10 ) {
		$data['folder'] = $this->folder;
		$data['title'] = "User groups";
		$data['description'] = "User groups Page";
		/* User_group_permission INITIAL VALUES START */
		$data['user_group_permissions'] = $this->User_group_permission_model->get_all_user_group_permission();
		/* PASS PARAMETER TO THE CENTER STAGE START */
		$data['code'] = $code;
		$data['sort_by'] = $sort_by;
		$data['sort_order'] = $sort_order;
		$data['limit'] = $limit;
		/* PASS PARAMETER TO THE CENTER STAGE END */
		
		/* CALL PAGINATION START */
		$this->load->library('pagination');
		$config['base_url']			= base_url( '/user_group_permissions/form_ajax/' . $code . '/'  . $sort_by . '/' . $sort_order . '/' . $limit );
		$config['total_rows']		= $this->User_group_permission_model->get_all_user_group_permission( array(), true );
		$config['per_page']			= $limit;
		$config['uri_segment']		= 7;
		$this->pagination->initialize( $config );
		
		$data['pagination'] = $this->pagination->create_ajax_links('list_table_container');
		/* CALL PAGINATION END */

		/* User_group_permission INITIAL VALUES END */
		$this->form_validation->set_rules( $this->fields );

		if( $this->form_validation->run() ) {
			$post = $this->input->post();
			$post['created_by'] = $this->account['id'];
			$id = $this->User_group_permission_model->save( $post );
			if( ! empty( $post['id'] ) ) {
				$action = 'update';
			} else {
				$action = 'save';
			}
			if( $this->input->is_ajax_request() ) {
				$this->response( 'save' );
			} else {
				$this->session->set_flashdata( 'message', 'User_group_permission has been ' . $action . 'd!' );
				redirect( 'dashboard' );
			}
		} else {
			$this->view('dashboard/dashboard', $data );
		}
	}

	public function update_selected( ) {
		$post = $this->input->post();
		// echo '<pre>'; print_r($post);echo '<pre>'; die();
		if( !empty( $post['user_group_permissions'] ) ) {
			//GET KEY
			foreach( $post['user_group_permissions'] as $key => $permission ) {
				$id_permissions = $key; break;
			}

			$personnel_group_id = $this->db->where('id',$id_permissions)->get('user_group_permissions')->result_array();
			$personnel_group_id = $personnel_group_id[0]['user_group_id'];
			//RESET ALL
			$data['view'] = 0;
			$data['add'] = 0;
			$data['edit'] = 0;
			$data['delete'] = 0;
			$this->db->where('user_group_id', $personnel_group_id);
			$this->db->update('user_group_permissions', $data); 

			foreach( $post['user_group_permissions'] as $key => $permission ) {
				// UPDATE CURRENT ID
				$data['id'] = $key;
				$data['view'] = 1;
				$data['add'] = 1;
				$data['edit'] = 1;
				$data['delete'] = 1;
				if (!isset($permission['view'])) {
					$data['view'] = 0;
				}
				if (!isset($permission['add'])) {
					$data['add'] = 0;
				}
				if (!isset($permission['edit'])) {
					$data['edit'] = 0;
				}
				if (!isset($permission['delete'])) {
					$data['delete'] = 0;
				}
				
				$this->User_group_permission_model->save( $data );
			}
		} else {	
			$personnel_group_id = $this->db->where('name',$post['name'])->get('user_group')->result_array();
			$personnel_group_id = $personnel_group_id[0]['id'];

			//RESET ALL
			$data['view'] = 0;
			$data['add'] = 0;
			$data['edit'] = 0;
			$data['delete'] = 0;
			$this->db->where('personnel_group_id', $personnel_group_id);
			$this->db->update('user_group_permissions', $data); 
		}
	}
	public function deactivate( $id = 0 ) {
		if( $id ) {
		
			$this->User_group_permission_model->deactivate( $id );
			
			$this->session->set_flashdata( 'message', 'User group has been deactivated' );
			
			redirect( 'dashboard' );
		
		}
		
		$id = $this->input->post( 'id' );
		
		if( $id ) {
		
			$this->User_group_permission_model->deactivate( $id );
			
			if( $this->input->is_ajax_request() ) {
			
				$this->response( 'deactivate' );
			
			} else {
			
				$this->session->set_flashdata( 'message', 'User group has been deactivated!' );
				
				redirect( 'dashboard' );
			
			}
		
		}
	}
	
	public function delete_selected( $id = 0 ) {
		$post = $this->input->post();
		if( ! empty( $post['selected_datas'] ) ) {
			foreach( $post['selected_datas'] as $key => $id ) {
				$this->User_group_permission_model->delete( $id );
			}
		}
		if( $this->input->is_ajax_request() ) {
			$this->response( 'delete' );
		} else {
			$this->session->set_flashdata( 'message', 'User_group_permission has been deleted!' );
			redirect( 'dashboard' );
		}
	}

	public function information() {
		$id = $this->input->post( 'id' );
		if( $id ) {
			$user_group_permission_information = array();
			$data['user_group_permissions'] = $this->User_group_permission_model->get_all_user_group_permission(array( 'user_group_id' => $id,'sort_by' => 'modules.id', 'sort_order' => 'ASC'  ));

			$response['view'] = $this->partial('user_groups/user_group_permission_table',$data,true);
			$response['json'] = json_encode( $data['user_group_permissions'] );
			echo json_encode($response);
		}
	}

	private function response( $action = 'save', $code = 0, $sort_by = 'user_group_permissions.id', $sort_order = 'DESC', $limit = 10 ) {
		//GET CUSTOMER TABLE WITH RESULT
		$result['user_group_permissions'] = $this->User_group_permission_model->get_all_user_group_permission( array('is_prospect'=>'1'));
		/* PASS PARAMETER TO THE CENTER STAGE START */
		$result['code'] = $code;
		$result['sort_by'] = $sort_by;
		$result['sort_order'] = $sort_order;
		$result['limit'] = $limit;
		/* PASS PARAMETER TO THE CENTER STAGE END */
		
		/* CALL PAGINATION START */
		$this->load->library('pagination');
		$config['base_url']			= base_url( '/user_group_permissions/form_ajax/' . $code . '/'  . $sort_by . '/' . $sort_order . '/' . $limit );
		$config['total_rows']		= $this->User_group_permission_model->get_all_user_group_permission( array(), true );
		$config['per_page']			= $limit;
		$config['uri_segment']		= 7;
		$this->pagination->initialize( $config );
		
		$result['pagination'] 	= $this->pagination->create_ajax_links('list_table_container');
		/* CALL PAGINATION END */
		$list_table = $this->partial( 'user_group_permissions/user_group_permission_list_table', $result, true );
		$data['action'] = $action;
		$data['list_table'] = $list_table;
		$data['success'] = 1;
		$this->update_developer( $data );
	}

	public function form_ajax( $code = 0, $sort_by = 'user_group_permissions.id', $sort_order = 'DESC', $limit = 10, $offset = 0 ) {
		$term = array();
		if ( $code ) {
			$term	= $this->Search_model->get_term( $code );
			$term	= json_decode( $term );
		}
		//GET ALL CUSTOMER DEPENDING ON CONDITIONS
		$data['user_group_permissions'] 	= $this->User_group_permission_model->get_all_user_group_permission(
			array(
				'search'		=> (object) $term,
				'sort_by' 		=> $sort_by,
				'sort_order' 	=> $sort_order,
				'limit' 		=> $limit,
				'offset' 		=> $offset
			)
		);
		// GET TOTAL BY CONTIDTION
		$total = $this->User_group_permission_model->get_all_user_group_permission( 
			array( 
				'search'		=> (object) $term,
				'sort_by'		=> $sort_by, 
				'sort_order'	=> $sort_order
			), 
			true 
		);
		/* PASS PARAMETER TO THE CENTER STAGE START */
		$data['code'] = $code;
		$data['sort_by'] = $sort_by;
		$data['sort_order'] = $sort_order;
		$data['limit'] = $limit;
		/* PASS PARAMETER TO THE CENTER STAGE END */
		
		/* CALL PAGINATION START */
		$this->load->library('pagination');
		$config['base_url']			= site_url( '/user_group_permissions/form_ajax/' . $code . '/'  . $sort_by . '/' . $sort_order . '/' . $limit );
		$config['total_rows']		= $total;
		$config['per_page']			= $limit;
		$config['uri_segment']		= 7;
		$this->pagination->initialize( $config );
		
		$data['pagination'] 	= $this->pagination->create_ajax_links('list_table_container');
		/* CALL PAGINATION END */
		
		//SHOW SOME LOVE
		echo $this->partial( 'user_group_permissions/user_group_permission_list_table', $data );
	}

	public function search($code = 0, $sort_by = 'user_group_permissions.id', $sort_order = 'DESC', $limit = 10, $offset = 0) {
		$post = $this->input->post();
		$term = "";

		if( $post ) {
			$term	= json_encode( $post );
			$code	= $this->Search_model->record_term( $term );
			$result['code']	= $code;			
			$term	= (object) $post;			
		} elseif ( $code ) {
			$term	= $this->Search_model->get_term( $code );			
			$term	= json_decode( $term );		
		}
		unset($post['type']);
		$data['term'] = $term;
		$params = array(
			'search'		=> (object) $term,
			'sort_by' 		=> $sort_by,
			'sort_order' 	=> $sort_order,
			'limit' 		=> $limit,
			'offset' 		=> $offset,
		);
		$result['user_group_permissions'] = $this->User_group_permission_model->get_all_user_group_permission($params);
		//echo $this->db->last_query();
		$config['total_rows'] = $this->User_group_permission_model->get_all_user_group_permission( $params, true );

		$result['sort_order'] = $sort_order;		
		$result['limit'] = $limit;		
		$result['code'] = $code;		
		
		$this->load->library('pagination');
		$config['base_url']			= base_url( '/user_group_permissions/form_ajax/' . $code . '/'  . $sort_by . '/' . $sort_order . '/' . $limit );
		$config['per_page']			= $limit;
		$config['uri_segment']		= 7;
		$this->pagination->initialize( $config );
		$result['pagination'] 	= $this->pagination->create_ajax_links('list_table_container');
		$list_table = $this->partial( 'user_group_permissions/user_group_permission_list_table', $result, true );
		$data['action'] = 'search';
		$data['list_table'] = $list_table;
		$this->update_developer( $data );
	}
}