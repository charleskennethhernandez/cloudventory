<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Security extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$url = base_url();
		$this->load->model('security_model');
		$this->lang->load('Security');
	}
	
	public function index() {
		show_404();
	}
	
	public function login() {
		if( $this->session->userdata('account') ) {	
			redirect( 'dashboard' );		
		}
		
		$data['title'] = 'Login';		
		$this->form_validation->set_rules('username', 'lang:username', 'trim|required');		
		$this->form_validation->set_rules('password', 'lang:password', 'trim|required');		
		if ( $this->form_validation->run() ) {		
			$username = $this->input->post('username');			
			$password = $this->input->post('password');			
			$account = $this->security_model->authenticate( $username, $password );		
				
			if ( $account ) {			
				redirect( 'dashboard' );
			} else {			
				$this->session->set_flashdata('error_login','Account don\'t exist!');		
				redirect( 'security/login' );
			}		
		} else {		
			$this->form_validation->set_error_delimiters('', '<br>');		
			$data['error'] = validation_errors();		
			$this->load->view('template/header',$data);			
			$this->load->view('login',$data);			
			$this->load->view('template/footer',$data);
		}
	}
	
	public function logout(){
	
		$this->session->unset_userdata( 'account' );
		
		// $this->session->unset_userdata( 'account_information' );
		
		// if ($_SERVER['SERVER_NAME'] == "localhost") {
		// 	redirect('http://localhost/cornerstone-developers/security/login');
		// } else if ($_SERVER['SERVER_NAME'] == "uni-asia-properties.com") {
		// 	redirect('http://uni-asia-properties.com/cornerstone/developers/security/login');	
		// } else {
		// 	redirect('http://lhoopapro.com/cornerstone/developers/security/login');	
		// }

		redirect('security/login');
	}
	
	public function change_password () {
	
		$post = $this->input->post();
		
		$account_exist = $this->Security_model->search_account( $post );
		
		if( $account_exist ) {
			
			if( $post['new_password'] == $post['confirm_password'] ) {
			
				//save new password
				$data = (array) $this->session->userdata( 'account' );
				
				$data['password'] = md5( $post['new_password'] );
				
				$this->Security_model->save( $data );
				
				$this->session->set_flashdata('message','Password changed!');
				
				redirect( $post['current_uri'] );
			
			} else {
			
				$this->session->set_flashdata('error','Password don\'t match');
			
				redirect( $post['current_uri'] );
			
			}
		
		} else {
		
			$this->session->set_flashdata('error','Invalid password!');
			
			redirect( $post['current_uri'] );
		
		}
	
	}
}
