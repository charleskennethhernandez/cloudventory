<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Invoices extends Back_Controller {
	private $folder = 'invoices';
	private $fields = array(
		array(
			'field'   => 'id', 
			'label'   => 'Id', 
			'rules'   => 'trim|int'
		),
		array(
			'field'   => 'overall_total_cost', 
			'label'   => 'Total Cost', 
 			'rules'   => 'trim|required'
 		),
	);
	public function __construct() {
		parent::__construct();
		$this->load->model( array('Sales_order_model','Sales_order_item_model','Search_model','Inventory_model','Customer_model') );
	}
	public function index($id=0) {
		if (!$id) {
			redirect('sales_orders');
		}
		$this->load->helper(array('dompdf'));
		$data['sales_order'] = $this->Sales_order_model->get_all_sales_order(array('id'=>$id));
		$this->Sales_order_model->get_all_sales_order(array('id'=>$id));
		echo "<pre>";

		$data['sales_order_items'] = $this->Sales_order_item_model->get_all_sales_order_item(array('sales_order_id'=>$id,'limit' =>14,'offset'=>0));
		// echo $this->db->last_query();

		$data['sales_order_items_2'] = $this->Sales_order_item_model->get_all_sales_order_item(array('sales_order_id'=>$id,'limit' =>14,'offset'=>14));
		// echo $this->db->last_query();

		$data['sales_order_items_3'] = $this->Sales_order_item_model->get_all_sales_order_item(array('sales_order_id'=>$id,'limit' =>14,'offset'=>28));
		// echo $this->db->last_query();

		$data['sales_order_items_4'] = $this->Sales_order_item_model->get_all_sales_order_item(array('sales_order_id'=>$id,'limit' =>14,'offset'=>42));
		// echo $this->db->last_query();

		$data['sales_order_items_5'] = $this->Sales_order_item_model->get_all_sales_order_item(array('sales_order_id'=>$id,'limit' =>14,'offset'=>56));
		// echo $this->db->last_query();

		$data['sales_order_items_6'] = $this->Sales_order_item_model->get_all_sales_order_item(array('sales_order_id'=>$id,'limit' =>14,'offset'=>70));
		// echo $this->db->last_query();


		
		// echo $this->db->last_query();
		// print_r($data);
		// die();

		$receiptHTML  = $this->load->view('template/invoice', $data, true);
		// echo $receiptHTML; die();

		if (!defined('DS'))
			define('DS', DIRECTORY_SEPARATOR);
		$relativepath = 'uploads' . DS . 'receipts';
		$absolutepath = FCPATH . $relativepath;
		if (!is_dir($absolutepath))
			mkdir($absolutepath);
		pdf_create($receiptHTML, $relativepath . DS . $id . '-AAR.pdf', 1, array('legal','landscape'));
	}
}