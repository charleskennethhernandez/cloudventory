<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Suppliers extends Back_Controller {
	private $folder = 'suppliers';
	private $fields = array(
		array(
			'field'   => 'id', 
			'label'   => 'Id', 
			'rules'   => 'trim|int'
		),
		array(
			'field'   => 'firstname', 
			'label'   => 'First Name', 
 			'rules'   => 'trim|required'
 		),
	);
	public function __construct() {
		parent::__construct();
		$this->load->model( array('Supplier_model','Search_model') );
	}
	public function index($code = 0, $sort_by = 'suppliers.id', $sort_order = 'DESC', $limit = 10 ) {
		$data['folder'] = $this->folder;
		$data['title'] = "Suppliers";
		$data['description'] = "Suppliers Page";
		/* Supplier INITIAL VALUES START */
		$data['suppliers'] = $this->Supplier_model->get_all_supplier();
		/* PASS PARAMETER TO THE CENTER STAGE START */
		$data['code'] = $code;
		$data['sort_by'] = $sort_by;
		$data['sort_order'] = $sort_order;
		$data['limit'] = $limit;
		/* PASS PARAMETER TO THE CENTER STAGE END */
		
		/* CALL PAGINATION START */
		$this->load->library('pagination');
		$config['base_url']			= base_url( '/suppliers/form_ajax/' . $code . '/'  . $sort_by . '/' . $sort_order . '/' . $limit );
		$config['total_rows']		= $this->Supplier_model->get_all_supplier( array(), true );
		$config['per_page']			= $limit;
		$config['uri_segment']		= 7;
		$this->pagination->initialize( $config );
		
		$data['pagination'] = $this->pagination->create_ajax_links('list_table_container');
		/* CALL PAGINATION END */

		/* Supplier INITIAL VALUES END */
		$this->form_validation->set_rules( $this->fields );

		if( $this->form_validation->run() ) {
			$post = $this->input->post();
			$post['created_by'] = $this->account['id'];
			$id = $this->Supplier_model->save( $post );
			if( ! empty( $post['id'] ) ) {
				$action = 'update';
			} else {
				$action = 'save';
			}
			if( $this->input->is_ajax_request() ) {
				$this->response( 'save' );
			} else {
				$this->session->set_flashdata( 'message', 'Supplier has been ' . $action . 'd!' );
				redirect( 'dashboard' );
			}
		} else {
			$this->view('dashboard/dashboard', $data );
		}
	}
	
	public function deactivate( $id = 0 ) {
		if( $id ) {
		
			$this->Supplier_model->deactivate( $id );
			
			$this->session->set_flashdata( 'message', 'Supplier has been deactivated' );
			
			redirect( 'dashboard' );
		
		}
		
		$id = $this->input->post( 'id' );
		
		if( $id ) {
		
			$this->Supplier_model->deactivate( $id );
			
			if( $this->input->is_ajax_request() ) {
			
				$this->response( 'deactivate' );
			
			} else {
			
				$this->session->set_flashdata( 'message', 'Supplier has been deactivated!' );
				
				redirect( 'dashboard' );
			
			}
		
		}
	}
	
	public function delete_selected( $id = 0 ) {
		$post = $this->input->post();
		if( ! empty( $post['selected_datas'] ) ) {
			foreach( $post['selected_datas'] as $key => $id ) {
				$this->Supplier_model->delete( $id );
			}
		}
		if( $this->input->is_ajax_request() ) {
			$this->response( 'delete' );
		} else {
			$this->session->set_flashdata( 'message', 'Supplier has been deleted!' );
			redirect( 'dashboard' );
		}
	}

	public function information() {
		$id = $this->input->post( 'id' );
		if( $id ) {
			$supplier_information = array();
			$supplier_information = $this->Supplier_model->get_all_supplier(array( 'id' => $id ));
			$response['view'] = $this->partial('suppliers/supplier_form',$supplier_information,true);
			$response['json'] = json_encode( $supplier_information );
			echo json_encode($response);
		}
	}

	private function response( $action = 'save', $code = 0, $sort_by = 'suppliers.id', $sort_order = 'DESC', $limit = 10 ) {
		//GET CUSTOMER TABLE WITH RESULT
		$result['suppliers'] = $this->Supplier_model->get_all_supplier( array('is_prospect'=>'1'));
		/* PASS PARAMETER TO THE CENTER STAGE START */
		$result['code'] = $code;
		$result['sort_by'] = $sort_by;
		$result['sort_order'] = $sort_order;
		$result['limit'] = $limit;
		/* PASS PARAMETER TO THE CENTER STAGE END */
		
		/* CALL PAGINATION START */
		$this->load->library('pagination');
		$config['base_url']			= base_url( '/suppliers/form_ajax/' . $code . '/'  . $sort_by . '/' . $sort_order . '/' . $limit );
		$config['total_rows']		= $this->Supplier_model->get_all_supplier( array(), true );
		$config['per_page']			= $limit;
		$config['uri_segment']		= 7;
		$this->pagination->initialize( $config );
		
		$result['pagination'] 	= $this->pagination->create_ajax_links('list_table_container');
		/* CALL PAGINATION END */
		$list_table = $this->partial( 'suppliers/supplier_list_table', $result, true );
		$data['action'] = $action;
		$data['list_table'] = $list_table;
		$data['success'] = 1;
		$this->update_developer( $data );
	}

	public function form_ajax( $code = 0, $sort_by = 'suppliers.id', $sort_order = 'DESC', $limit = 10, $offset = 0 ) {
		$term = array();
		if ( $code ) {
			$term	= $this->Search_model->get_term( $code );
			$term	= json_decode( $term );
		}
		//GET ALL CUSTOMER DEPENDING ON CONDITIONS
		$data['suppliers'] 	= $this->Supplier_model->get_all_supplier(
			array(
				'search'		=> (object) $term,
				'sort_by' 		=> $sort_by,
				'sort_order' 	=> $sort_order,
				'limit' 		=> $limit,
				'offset' 		=> $offset
			)
		);
		// GET TOTAL BY CONTIDTION
		$total = $this->Supplier_model->get_all_supplier( 
			array( 
				'search'		=> (object) $term,
				'sort_by'		=> $sort_by, 
				'sort_order'	=> $sort_order
			), 
			true 
		);
		/* PASS PARAMETER TO THE CENTER STAGE START */
		$data['code'] = $code;
		$data['sort_by'] = $sort_by;
		$data['sort_order'] = $sort_order;
		$data['limit'] = $limit;
		/* PASS PARAMETER TO THE CENTER STAGE END */
		
		/* CALL PAGINATION START */
		$this->load->library('pagination');
		$config['base_url']			= site_url( '/suppliers/form_ajax/' . $code . '/'  . $sort_by . '/' . $sort_order . '/' . $limit );
		$config['total_rows']		= $total;
		$config['per_page']			= $limit;
		$config['uri_segment']		= 7;
		$this->pagination->initialize( $config );
		
		$data['pagination'] 	= $this->pagination->create_ajax_links('list_table_container');
		/* CALL PAGINATION END */
		
		//SHOW SOME LOVE
		echo $this->partial( 'suppliers/supplier_list_table', $data );
	}

	public function search($code = 0, $sort_by = 'suppliers.id', $sort_order = 'DESC', $limit = 10, $offset = 0) {
		$post = $this->input->post();
		$term = "";

		if( $post ) {
			$term	= json_encode( $post );
			$code	= $this->Search_model->record_term( $term );
			$result['code']	= $code;			
			$term	= (object) $post;			
		} elseif ( $code ) {
			$term	= $this->Search_model->get_term( $code );			
			$term	= json_decode( $term );		
		}
		unset($post['type']);
		$data['term'] = $term;
		$params = array(
			'search'		=> (object) $term,
			'sort_by' 		=> $sort_by,
			'sort_order' 	=> $sort_order,
			'limit' 		=> $limit,
			'offset' 		=> $offset,
		);
		$result['suppliers'] = $this->Supplier_model->get_all_supplier($params);
		//echo $this->db->last_query();
		$config['total_rows'] = $this->Supplier_model->get_all_supplier( $params, true );

		$result['sort_order'] = $sort_order;		
		$result['limit'] = $limit;		
		$result['code'] = $code;		
		
		$this->load->library('pagination');
		$config['base_url']			= base_url( '/suppliers/form_ajax/' . $code . '/'  . $sort_by . '/' . $sort_order . '/' . $limit );
		$config['per_page']			= $limit;
		$config['uri_segment']		= 7;
		$this->pagination->initialize( $config );
		$result['pagination'] 	= $this->pagination->create_ajax_links('list_table_container');
		$list_table = $this->partial( 'suppliers/supplier_list_table', $result, true );
		$data['action'] = 'search';
		$data['list_table'] = $list_table;
		$this->update_developer( $data );
	}
}