<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sales_reports extends Back_Controller {
	private $folder = 'sales_reports';
	private $fields = array(
		array(
			'field'   => 'id', 
			'label'   => 'Id', 
			'rules'   => 'trim|int'
		),
		array(
			'field'   => 'name', 
			'label'   => 'Name', 
 			'rules'   => 'trim|required'
 		),
	);
	public function __construct() {
		parent::__construct();
		$this->load->model( array('Sales_order_item_model','Sales_order_model','Sales_report_model','Search_model') );
	}
	public function download() {
		$post = $this->input->post();
		if ($post['id'] == "1") {
			$this->sales_order_status($post);
		} else if ($post['id'] == "2") {
			$this->top_items($post);
		}
	}
	public function sales_order_status($post = array()){
		#error_reporting(E_ALL);
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()
            		->setTitle("Worksheet")
            		->setSubject("Worksheet")
            		->setDescription("none");

		// Description
		//$objPHPExcel->getProperties()->setTitle("export")->setDescription("none");

        // Default Style			
		$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')->setSize(10);

		// Column
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A1', 'ID')
					->setCellValue('B1', 'Customer Name')
					->setCellValue('C1', 'Total Amount')
					->setCellValue('D1', 'Paid Paid') 
					->setCellValue('E1', 'Date Created') 
					->setCellValue('F1', 'Status');
		$style = array(
	        'alignment' => array(
	            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        )
	    );

	    // $objPHPExcel->getActiveSheet()->getStyle("B2:P2")->applyFromArray($style);
		$objPHPExcel->getActiveSheet()
		    ->getColumnDimension('A')
		    ->setAutoSize(true);

		$date_from = $post['date_from'];
		$date_to = $post['date_to'];

		$sales_params['where'] = array(
			'sales_orders.date_created >=' => date('Y-m-d', strtotime($date_from) ).' 00:00:00',
			'sales_orders.date_created <=' => date('Y-m-d', strtotime($date_to) ).' 23:59:59',
		);
		$sales_params['limit'] = '10000';
		$sales_params['offset'] = '0';
		$sales_orders =  $this->Sales_order_model->get_all_sales_order($sales_params);
		// echo "<pre>";
		// echo $this->db->last_query();
		// print_r($sales_orders);
		// die();
		$row = 2;
		if ($sales_orders) {
			$count = count($sales_orders) + 2;
			$a_paid = 0;
			$g_total = 0;
			foreach($sales_orders as $key => $sales_order ){
				$objPHPExcel->setActiveSheetIndex(0)            				
			        ->setCellValue('A'.$row, $sales_order['id'])
					->setCellValue('B'.$row, $sales_order['firstname'].' '.$sales_order['lastname'])
					->setCellValue('C'.$row, $sales_order['grand_total'])
					->setCellValue('D'.$row, $sales_order['amount_paid'])
					->setCellValue('E'.$row, $sales_order['date_created'])
					->setCellValue('F'.$row, $sales_order['status_name']);
				$row++;	
				$g_total = $g_total + $sales_order['grand_total'];
				$a_paid = $a_paid + $sales_order['amount_paid'];
			}
			$objPHPExcel->setActiveSheetIndex(0)            				
			        ->setCellValue('A'.$count, '')
					->setCellValue('B'.$count, '')
					->setCellValue('C'.$count, $g_total)
					->setCellValue('D'.$count, $a_paid)
					->setCellValue('E'.$count, '')
					->setCellValue('F'.$count, '');
		}
		
		$objPHPExcel->setActiveSheetIndex(0);
		
		if (ob_get_contents()) ob_end_clean();

		#$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		// $objWriter->setDelimiter("|"); 
		
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="SalesOrderStatus'.date("YmdH").'.csv"');
		header('Cache-Control: max-age=0');
		
		$objWriter->save('php://output');
	}
	public function top_items($post = array()){
		#error_reporting(E_ALL);
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()
            		->setTitle("Worksheet")
            		->setSubject("Worksheet")
            		->setDescription("none");

		// Description
		//$objPHPExcel->getProperties()->setTitle("export")->setDescription("none");

        // Default Style			
		$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')->setSize(10);

		// Column
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A1', 'Product Name')
					->setCellValue('B1', 'Unit Cost')
					->setCellValue('C1', 'Total Quantity')
					->setCellValue('D1', 'Total Cost') 
					->setCellValue('E1', 'Date From') 
					->setCellValue('F1', 'Date To');
		$style = array(
	        'alignment' => array(
	            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        )
	    );

	    // $objPHPExcel->getActiveSheet()->getStyle("B2:P2")->applyFromArray($style);
		$objPHPExcel->getActiveSheet()
		    ->getColumnDimension('A')
		    ->setAutoSize(true);

		$date_from = $post['date_from'];
		$date_to = $post['date_to'];

		$sales_params['where'] = array(
			'sales_order_items.date_created >=' => date('Y-m-d', strtotime($date_from) ).' 00:00:00',
			'sales_order_items.date_created <=' => date('Y-m-d', strtotime($date_to) ).' 23:59:59',
		);
		$sales_params['limit'] = '10000';
		$sales_params['offset'] = '0';
		$sales_order_items =  $this->Sales_order_item_model->get_all_top_items($sales_params);

		$row = 2;
		foreach($sales_order_items as $key => $sales_order_item ){
			$objPHPExcel->setActiveSheetIndex(0)            				
		        ->setCellValue('A'.$row, $sales_order_item['inventory_name'])
				->setCellValue('B'.$row, $sales_order_item['unit_cost'])
				->setCellValue('C'.$row, $sales_order_item['total_quantity'])
				->setCellValue('D'.$row, $sales_order_item['total_cost'])
				->setCellValue('E'.$row, @$date_from)
				->setCellValue('F'.$row, @$date_to);
			$row++;	
		}
		$objPHPExcel->setActiveSheetIndex(0);
		
		if (ob_get_contents()) ob_end_clean();

		#$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		// $objWriter->setDelimiter("|"); 
		
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="SalesOrderTopItems'.date("YmdH").'.csv"');
		header('Cache-Control: max-age=0');
		
		$objWriter->save('php://output');
	}
	public function index($code = 0, $sort_by = 'sales_reports.id', $sort_order = 'DESC', $limit = 10 ) {
		$data['folder'] = $this->folder;
		$data['title'] = "Sales reports";
		$data['description'] = "Sales reports Page";
		/* Sales_report INITIAL VALUES START */
		$data['sales_reports'] = $this->Sales_report_model->get_all_sales_report();
		/* PASS PARAMETER TO THE CENTER STAGE START */
		$data['code'] = $code;
		$data['sort_by'] = $sort_by;
		$data['sort_order'] = $sort_order;
		$data['limit'] = $limit;
		/* PASS PARAMETER TO THE CENTER STAGE END */
		
		/* CALL PAGINATION START */
		$this->load->library('pagination');
		$config['base_url']			= base_url( '/sales_reports/form_ajax/' . $code . '/'  . $sort_by . '/' . $sort_order . '/' . $limit );
		$config['total_rows']		= $this->Sales_report_model->get_all_sales_report( array(), true );
		$config['per_page']			= $limit;
		$config['uri_segment']		= 7;
		$this->pagination->initialize( $config );
		
		$data['pagination'] = $this->pagination->create_ajax_links('list_table_container');
		/* CALL PAGINATION END */

		/* Sales_report INITIAL VALUES END */
		$this->form_validation->set_rules( $this->fields );

		if( $this->form_validation->run() ) {
			$post = $this->input->post();
			$post['created_by'] = $this->account['id'];
			$id = $this->Sales_report_model->save( $post );
			if( ! empty( $post['id'] ) ) {
				$action = 'update';
			} else {
				$action = 'save';
			}
			if( $this->input->is_ajax_request() ) {
				$this->response( 'save' );
			} else {
				$this->session->set_flashdata( 'message', 'Sales_report has been ' . $action . 'd!' );
				redirect( 'dashboard' );
			}
		} else {
			$this->view('dashboard/dashboard', $data );
		}
	}
	
	public function deactivate( $id = 0 ) {
		if( $id ) {
		
			$this->Sales_report_model->deactivate( $id );
			
			$this->session->set_flashdata( 'message', 'Sales_report has been deactivated' );
			
			redirect( 'dashboard' );
		
		}
		
		$id = $this->input->post( 'id' );
		
		if( $id ) {
		
			$this->Sales_report_model->deactivate( $id );
			
			if( $this->input->is_ajax_request() ) {
			
				$this->response( 'deactivate' );
			
			} else {
			
				$this->session->set_flashdata( 'message', 'Sales_report has been deactivated!' );
				
				redirect( 'dashboard' );
			
			}
		
		}
	}
	
	public function delete_selected( $id = 0 ) {
		$post = $this->input->post();
		if( ! empty( $post['selected_datas'] ) ) {
			foreach( $post['selected_datas'] as $key => $id ) {
				$this->Sales_report_model->delete( $id );
			}
		}
		if( $this->input->is_ajax_request() ) {
			$this->response( 'delete' );
		} else {
			$this->session->set_flashdata( 'message', 'Sales_report has been deleted!' );
			redirect( 'dashboard' );
		}
	}

	public function information() {
		$id = $this->input->post( 'id' );
		if( $id ) {
			$sales_report_information = array();
			$sales_report_information = $this->Sales_report_model->get_all_sales_report(array( 'id' => $id ));
			$response['view'] = $this->partial('sales_reports/sales_report_form',$sales_report_information,true);
			$response['json'] = json_encode( $sales_report_information );
			echo json_encode($response);
		}
	}

	private function response( $action = 'save', $code = 0, $sort_by = 'sales_reports.id', $sort_order = 'DESC', $limit = 10 ) {
		//GET CUSTOMER TABLE WITH RESULT
		$result['sales_reports'] = $this->Sales_report_model->get_all_sales_report( array('is_prospect'=>'1'));
		/* PASS PARAMETER TO THE CENTER STAGE START */
		$result['code'] = $code;
		$result['sort_by'] = $sort_by;
		$result['sort_order'] = $sort_order;
		$result['limit'] = $limit;
		/* PASS PARAMETER TO THE CENTER STAGE END */
		
		/* CALL PAGINATION START */
		$this->load->library('pagination');
		$config['base_url']			= base_url( '/sales_reports/form_ajax/' . $code . '/'  . $sort_by . '/' . $sort_order . '/' . $limit );
		$config['total_rows']		= $this->Sales_report_model->get_all_sales_report( array(), true );
		$config['per_page']			= $limit;
		$config['uri_segment']		= 7;
		$this->pagination->initialize( $config );
		
		$result['pagination'] 	= $this->pagination->create_ajax_links('list_table_container');
		/* CALL PAGINATION END */
		$list_table = $this->partial( 'sales_reports/sales_report_list_table', $result, true );
		$data['action'] = $action;
		$data['list_table'] = $list_table;
		$data['success'] = 1;
		$this->update_developer( $data );
	}

	public function form_ajax( $code = 0, $sort_by = 'sales_reports.id', $sort_order = 'DESC', $limit = 10, $offset = 0 ) {
		$term = array();
		if ( $code ) {
			$term	= $this->Search_model->get_term( $code );
			$term	= json_decode( $term );
		}
		//GET ALL CUSTOMER DEPENDING ON CONDITIONS
		$data['sales_reports'] 	= $this->Sales_report_model->get_all_sales_report(
			array(
				'search'		=> (object) $term,
				'sort_by' 		=> $sort_by,
				'sort_order' 	=> $sort_order,
				'limit' 		=> $limit,
				'offset' 		=> $offset
			)
		);
		// GET TOTAL BY CONTIDTION
		$total = $this->Sales_report_model->get_all_sales_report( 
			array( 
				'search'		=> (object) $term,
				'sort_by'		=> $sort_by, 
				'sort_order'	=> $sort_order
			), 
			true 
		);
		/* PASS PARAMETER TO THE CENTER STAGE START */
		$data['code'] = $code;
		$data['sort_by'] = $sort_by;
		$data['sort_order'] = $sort_order;
		$data['limit'] = $limit;
		/* PASS PARAMETER TO THE CENTER STAGE END */
		
		/* CALL PAGINATION START */
		$this->load->library('pagination');
		$config['base_url']			= site_url( '/sales_reports/form_ajax/' . $code . '/'  . $sort_by . '/' . $sort_order . '/' . $limit );
		$config['total_rows']		= $total;
		$config['per_page']			= $limit;
		$config['uri_segment']		= 7;
		$this->pagination->initialize( $config );
		
		$data['pagination'] 	= $this->pagination->create_ajax_links('list_table_container');
		/* CALL PAGINATION END */
		
		//SHOW SOME LOVE
		echo $this->partial( 'sales_reports/sales_report_list_table', $data );
	}

	public function search($code = 0, $sort_by = 'sales_reports.id', $sort_order = 'DESC', $limit = 10, $offset = 0) {
		$post = $this->input->post();
		$term = "";

		if( $post ) {
			$term	= json_encode( $post );
			$code	= $this->Search_model->record_term( $term );
			$result['code']	= $code;			
			$term	= (object) $post;			
		} elseif ( $code ) {
			$term	= $this->Search_model->get_term( $code );			
			$term	= json_decode( $term );		
		}
		unset($post['type']);
		$data['term'] = $term;
		$params = array(
			'search'		=> (object) $term,
			'sort_by' 		=> $sort_by,
			'sort_order' 	=> $sort_order,
			'limit' 		=> $limit,
			'offset' 		=> $offset,
		);
		$result['sales_reports'] = $this->Sales_report_model->get_all_sales_report($params);
		//echo $this->db->last_query();
		$config['total_rows'] = $this->Sales_report_model->get_all_sales_report( $params, true );

		$result['sort_order'] = $sort_order;		
		$result['limit'] = $limit;		
		$result['code'] = $code;		
		
		$this->load->library('pagination');
		$config['base_url']			= base_url( '/sales_reports/form_ajax/' . $code . '/'  . $sort_by . '/' . $sort_order . '/' . $limit );
		$config['per_page']			= $limit;
		$config['uri_segment']		= 7;
		$this->pagination->initialize( $config );
		$result['pagination'] 	= $this->pagination->create_ajax_links('list_table_container');
		$list_table = $this->partial( 'sales_reports/sales_report_list_table', $result, true );
		$data['action'] = 'search';
		$data['list_table'] = $list_table;
		$this->update_developer( $data );
	}
}