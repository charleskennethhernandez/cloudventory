<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package     CodeIgniter
 * @author      ExpressionEngine Dev Team
 * @copyright   Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license     http://codeigniter.com/user_guide/license.html
 * @link        http://codeigniter.com
 * @since       Version 1.0
 * @filesource
 */


function pdf_create($html, $filename='', $stream=1, $paper=array('letter', 'landscape')) 
{
    error_reporting(0);
    require_once("dompdf/dompdf_config.inc.php");
    
    $dompdf = new DOMPDF();
    $dompdf->load_html($html);
    $dompdf->set_paper($paper[0], $paper[1]);
    $dompdf->render();
    if ($stream === 1 || $stream === TRUE) {
        $dompdf->stream(array(
            'compress' => true,
            'Attachment' => true
        ));
    } elseif ($stream === 2) {
        //save the pdf file on the server
        unlink(FCPATH . $filename);
        file_put_contents(FCPATH . $filename, $dompdf->output());

        $absoluteurl = base_url(str_replace('\\', '/', $filename));

        echo '<script>window.location = "' . $absoluteurl . '"';
    } elseif ($stream === 3) {
        return $dompdf->output();
    } elseif ($stream === 4) {
        $dompdf->stream($filename . ".pdf");
    }
}
?>