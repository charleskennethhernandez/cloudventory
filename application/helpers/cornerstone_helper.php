<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function get_field($table,$field,$id){
	$CI = &get_instance();
	$query = $CI->db->get_where($table, array('id' => $id));
	$result = $query->row_array();
	return $result[$field];
}
function pr($data, $exit=FALSE) {
	echo "<pre>";
	print_r($data);
	echo "</pre>";
	if ($exit)
		exit();
}
function vd($data, $exit=FALSE) {
	echo "<pre>";
	var_dump($data);
	echo "</pre>";
	if ($exit)
		exit();
}
function get_recipient($recipient_user_id,$recipient_user_type_id){
	$CI 	 = &get_instance();
	if ($recipient_user_type_id == "1") {
		$table = "cornerstone_administrators";
	} else if ($recipient_user_type_id == "2") {
		$table = "cornerstone_developers";
	}  else if ($recipient_user_type_id == "3") {
		$table = "cornerstone_brokers";
	} else if ($recipient_user_type_id == "4") {
		$table = "cornerstone_agents";
	} else if ($recipient_user_type_id == "5") {
		$table = "cornerstone_clients";
	} else if ($recipient_user_type_id == "6") {
		$table = "cornerstone_personnels";
	}
	$query = $CI->db->get_where($table, array('id' => $recipient_user_id));
	$result = $query->row_array();
	if ($recipient_user_type_id == 2) {
		return @$result['name'];
	} else {
		return @$result['firstname'].' '.@$result['lastname'];
	}
}
function get_all_modules($id = ""){
	$CI = &get_instance();

	if ($id) {
		$CI->db->select( '*, modules.id as id' );
		$params = array('view' => 1, 'user_group_permissions.user_group_id' => $id);
		$CI->db->join('user_group_permissions', 'user_group_permissions.module_id = modules.id');
	} else {
		$params = array();
	}

	$CI->db->order_by("parent", "asc");
	$CI->db->order_by("key", "asc");
	$query = $CI->db->get_where('modules',$params);
	$results = $query->result_array();
	return $results;
}

function date_time_format($date){
	return date("M d Y g:i A", strtotime($date));
}
function get_value_field($id,$table,$field,$extra_id = 0){
	$CI = &get_instance();
	if ($extra_id) {
		$id_f = $extra_id;
	} else {
		$id_f = 'id';
	}
	$query = $CI->db->get_where($table, array($id_f => $id));
	$results = $query->row_array();
	if ($results) {
		return $results[$field];
	} else {
		return "0";
	}
}
function user_permission($personnel_group_id,$module_name,$action){
	$CI = &get_instance();
	$CI->db->select( '*, modules.id as id' );
	$CI->db->join('modules', 'user_group_permissions.module_id = modules.id');
	$query = $CI->db->get_where('user_group_permissions', array('user_group_id' => $personnel_group_id,'modules.module' => $module_name));
	$row = $query->row_array();
	// echo "<pre>";
	// echo $CI->db->last_query();
	// print_r($row);
	// die();
	if ($personnel_group_id) {
		if ($row) {
			return $row[$action];
		} else {
			return 1;
		}
	} else {
		return 1;
	}
}
/** END **/