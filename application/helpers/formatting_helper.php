<?php 
function format_address($fields, $br=false)
{
	if(empty($fields))
	{
		return ;
	}
	
	// Default format
	$default = "{firstname} {lastname}\n{company}\n{address_1}\n{address_2}\n{city}, {zone} {postcode}\n{country}";
	
	// Fetch country record to determine which format to use
	$CI = &get_instance();
	$CI->load->model('location_model');
	$c_data = $CI->location_model->get_country($fields['country_id']);
	
	if(empty($c_data->address_format))
	{
		$formatted	= $default;
	} else {
		$formatted	= $c_data->address_format;
	}

	$formatted		= str_replace('{firstname}', $fields['firstname'], $formatted);
	$formatted		= str_replace('{lastname}',  $fields['lastname'], $formatted);
	$formatted		= str_replace('{company}',  $fields['company'], $formatted);
	
	$formatted		= str_replace('{address_1}', $fields['address1'], $formatted);
	$formatted		= str_replace('{address_2}', $fields['address2'], $formatted);
	$formatted		= str_replace('{city}', $fields['city'], $formatted);
	$formatted		= str_replace('{zone}', $fields['zone'], $formatted);
	$formatted		= str_replace('{postcode}', $fields['zip'], $formatted);
	$formatted		= str_replace('{country}', $fields['country'], $formatted);
	
	// remove any extra new lines resulting from blank company or address line
	$formatted		= preg_replace('`[\r\n]+`',"\n",$formatted);
	if($br)
	{
		$formatted	= nl2br($formatted);
	}
	return $formatted;
	
}

function format_currency($value, $symbol=true)
{

	if(!is_numeric($value))
	{
		return;
	}
	
	$CI = &get_instance();
	
	if($value < 0 )
	{
		$neg = '- ';
	} else {
		$neg = '';
	}
	
	if($symbol)
	{
		$formatted	= number_format(abs($value), 2, $CI->config->item('currency_decimal'), $CI->config->item('currency_thousands_separator'));
		
		if($CI->config->item('currency_symbol_side') == 'left')
		{
			$formatted	= $neg.$CI->config->item('currency_symbol').$formatted;
		}
		else
		{
			$formatted	= $neg.$formatted.$CI->config->item('currency_symbol');
		}
	}
	else
	{
		//traditional number formatting
		$formatted	= number_format(abs($value), 2, '.', ',');
	}
	
	return $formatted;
}

function format_date($date){
	return date('F d Y',strtotime($date));
}

function concat_address($address=array()) {
	$text = '';
	if (!empty($address)) {

		if (!empty($address['address_unit']))
			$text .= $address['address_unit'] . ' '; 
		if (!empty($address['address_block']))
			$text .= $address['address_block'] . ' '; 
		if (!empty($address['address_street']))
			$text .= $address['address_street'] . ', '; 
		if (!empty($address['barangay']))
			$text .= $address['barangay'] . ', '; 
		if (!empty($address['city']))
			$text .= $address['city'] . ', '; 
		if (!empty($address['province']))
			$text .= $address['province'] . ', '; 
		if (!empty($address['state']))
			$text .= $address['state'] . ', '; 
		if (!empty($address['country']))
			$text .= $address['country'] . ', '; 
		if (!empty($address['zipcode']))
			$text .= $address['zipcode'];
	}

	return trim(trim($text, ' '), ', ');
}

function concat_contact($contact=array()) {
	$text = '';
	if (!empty($contact)) {

		if (!empty($contact['contact_country_code'])) $text .= $contact['contact_country_code']; 
		if (!empty($contact['contact_area_code']) && !empty($text)) {
			$text .= ltrim($contact['contact_area_code'], '0');
		} elseif (!empty($contact['contact_area_code']) && empty($text))  {
			$text .= '0' . ltrim($contact['contact_area_code'], '0');
		}
		if (!empty($contact['value'])) $text .= $contact['value'] . ' '; 
		if (!empty($contact['contact_local'])) $text .= $contact['contact_local'];
	}

	return trim($text);
}