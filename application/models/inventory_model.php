<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Inventory_model extends Cloudventory_model {
	public function __construct() {
		parent::__construct();
		$this->table = 'inventories';
	}
	public function get_all_inventory( $params = array(), $count = false ) {
		$this->db->select( $this->table.'.*,inventories.id as inventory_id,brands.name as brand_name,inventory_categories.name as inventory_category_name', FALSE );		
		$this->db->from( $this->table );
		$this->db->join( 'brands', 'brands.id = '.$this->table.'.brand_id', 'left' );
		$this->db->join( 'inventory_categories', 'inventory_categories.id = '.$this->table.'.category_id', 'left' );

		if ( isset( $params['search'] ) && !empty( $params['search'] ) ) {
		
			if( isset( $params['search']->term ) &&  !empty( $params['search']->term ) ) {
			
				$search_fields = array(
					'inventories.name',
					'inventories.description',
					'brands.name'
				);
				$term = explode(' ', $params['search']->term );
				
				foreach( $term as $t ) {
				
					$not		= '';
					
					$operator	= 'OR';
					
					if( substr( $t, 0, 1 ) == '-' ) {
					
						$not		= 'NOT ';
						
						$operator	= 'AND';
						
						$t		= substr( $t, 1,strlen( $t ) );
						
					}
					$index	 = 0;
					
					$like	 = '';
					
					$like	.= "(";
					
					foreach ( $search_fields as $field ) {
					
						$index++;
						
						$like .= " ".$field." ".$not."LIKE '%".$t."%' ";
						
						$like .= ( count( $search_fields ) <= $index ) ? "" : $operator;
						
					}
					
					$like	.= ") ";
					$this->db->where( $like );
					
				}
				
			}
			
		}
		
		if( isset( $params['search']->firstname ) && !empty( $params['search']->firstname ) ) {
			$this->db->where(  $this->table.'.firstname', $params['search']->firstname );
		}
		
		//BROKERS ACTIVE AND NOT DELETED
		$this->db->where( $this->table.'.is_active', 1 ); 
		$this->db->where( $this->table.'.is_deleted', 0 ); 
		
		//ORDER BY
		if( isset( $params['sort_by'] ) && !empty( $params['sort_by'] ) ) {
			$this->db->order_by( $params['sort_by'], $params['sort_order'] );
		} else {
			$this->db->order_by(  $this->table.'.'.$this->sort_by, $this->sort_order );
		}
		//TO GET ALL RECORD
		if( $count ){
			return $this->db->count_all_results();
		} else {
			if( isset( $params['id'] ) && !empty( $params['id'] ) ) {
				$this->db->where( $this->table.'.'.'id', $params['id'] );
				return $this->db->get()->row_array();
			} else {
				//LIMIT
				if( isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
					$this->db->limit( $params['limit'], $params['offset'] );
				} else {
					$this->db->limit( $this->limit );				
				}
				return $this->db->get()->result_array();
			}
		}
	}
}