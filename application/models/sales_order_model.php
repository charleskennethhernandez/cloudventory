<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sales_order_model extends Cloudventory_model {
	public function __construct() {
		parent::__construct();
		$this->table = 'sales_orders';
	}
	public function get_all_sales_order( $params = array(), $count = false ) {
		$this->db->select( $this->table.'.*,
			customers.firstname,
			customers.lastname,
			customers.middlename,
			users.firstname as user_firstname,
			users.lastname as user_lastname,
			users.middlename as user_middlename,
			sales_order_status.name as status_name,
		', FALSE );		
		$this->db->from( $this->table );
		$this->db->join( 'customers', 'customers.id = '.$this->table.'.customer_id', 'left' );
		$this->db->join( 'users', 'users.id = '.$this->table.'.created_by', 'left' );
		$this->db->join( 'sales_order_status', 'sales_order_status.id = '.$this->table.'.status', 'left' );

		if ( isset( $params['search'] ) && !empty( $params['search'] ) ) {
			if( isset( $params['search']->term ) &&  !empty( $params['search']->term ) ) {
				$search_fields = array(
					'customers.firstname',
					'customers.lastname',
					'customers.firstname',
					'sales_order_status.name',
				);
				$term = explode(' ', $params['search']->term );
				foreach( $term as $t ) {
					$not		= '';
					$operator	= 'OR';
					if( substr( $t, 0, 1 ) == '-' ) {
						$not		= 'NOT ';
						$operator	= 'AND';
						$t		= substr( $t, 1,strlen( $t ) );
					}
					$index	 = 0;
					$like	 = '';
					$like	.= "(";
					foreach ( $search_fields as $field ) {
						$index++;
						$like .= " ".$field." ".$not."LIKE '%".$t."%' ";
						$like .= ( count( $search_fields ) <= $index ) ? "" : $operator;
					}
					$like	.= ") ";
					$this->db->where( $like );
				}
			}
		}
		
		if( isset( $params['search']->firstname ) && !empty( $params['search']->firstname ) ) {
			$this->db->where(  $this->table.'.firstname', $params['search']->firstname );
		}
		
		if(!empty($params['where'])){
			foreach ($params['where'] as $key => $value) {
				$this->db->where($key, $value);
			}
		}
		
		//BROKERS ACTIVE AND NOT DELETED
		$this->db->where( $this->table.'.is_active', 1 ); 
		$this->db->where( $this->table.'.is_deleted', 0 ); 
		
		//ORDER BY
		if( isset( $params['sort_by'] ) && !empty( $params['sort_by'] ) ) {
			$this->db->order_by( $params['sort_by'], $params['sort_order'] );
		} else {
			$this->db->order_by(  $this->table.'.'.$this->sort_by, $this->sort_order );
		}
		//TO GET ALL RECORD
		if( $count ){
			return $this->db->count_all_results();
		} else {
			if( isset( $params['id'] ) && !empty( $params['id'] ) ) {
				$this->db->where( $this->table.'.'.'id', $params['id'] );
				return $this->db->get()->row_array();
			} else {
				//LIMIT
				if( isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
					$this->db->limit( $params['limit'], $params['offset'] );
				} else {
					$this->db->limit( $this->limit );				
				}
				return $this->db->get()->result_array();
			}
		}
	}
	function get_sales($params = array()){
		$this->db->select_sum('grand_total');

		if(!empty($params['where'])){
			foreach ($params['where'] as $key => $value) {
				$this->db->where($key, $value);
			}
		}

		$query = $this->db->get($this->table);
		$result = $query->row_array();

		if(!empty($params['average'])){
			if ($params['average'] == "daily") {
				$result = $result['grand_total'] / 365;
			} 
			if ($params['average'] == "monthly") {
				$result = $result['grand_total'] / 12;
			}
			if ($params['average'] == "yearly") {
				$result = $result['grand_total'];
			}
			return $result;
		} else {
			return $result;
		}
	}
}