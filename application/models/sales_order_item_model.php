<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sales_order_item_model extends Cloudventory_model {
	public function __construct() {
		parent::__construct();
		$this->table = 'sales_order_items';
	}
	public function get_all_sales_order_item( $params = array(), $count = false ) {
		$this->db->select( $this->table.'.*,
			inventories.name,
			inventories.description,
			inventories.quantity as inventory_quantity,
			customers.firstname as firstname,
			customers.lastname as lastname,
			users.firstname as user_firstname,
			users.lastname as user_lastname,
			sales_order_items.id as sales_order_item_id,
		', FALSE );		
		$this->db->from( $this->table );
		$this->db->join( 'sales_orders', 'sales_orders.id = '.$this->table.'.sales_order_id', 'left' );
		$this->db->join( 'inventories', 'inventories.id = '.$this->table.'.inventory_id', 'left' );
		$this->db->join( 'customers', 'customers.id = sales_orders.customer_id', 'left' );
		$this->db->join( 'users', 'users.id = sales_orders.created_by', 'left' );

		if ( isset( $params['search'] ) && !empty( $params['search'] ) ) {
			if( isset( $params['search']->term ) &&  !empty( $params['search']->term ) ) {
				$search_fields = array(
					'inventories.name',
					'customers.firstname',
					'customers.lastname',
				);
				$term = explode(' ', $params['search']->term );
				foreach( $term as $t ) {
					$not		= '';
					$operator	= 'OR';
					if( substr( $t, 0, 1 ) == '-' ) {
						$not		= 'NOT ';
						$operator	= 'AND';
						$t		= substr( $t, 1,strlen( $t ) );
					}
					$index	 = 0;
					$like	 = '';
					$like	.= "(";
					foreach ( $search_fields as $field ) {
						$index++;
						$like .= " ".$field." ".$not."LIKE '%".$t."%' ";
						$like .= ( count( $search_fields ) <= $index ) ? "" : $operator;
					}
					$like	.= ") ";
					$this->db->where( $like );
				}
			}
		}
		
		if( isset( $params['search']->firstname ) && !empty( $params['search']->firstname ) ) {
			$this->db->where(  $this->table.'.firstname', $params['search']->firstname );
		}
		
		//BROKERS ACTIVE AND NOT DELETED
		$this->db->where( $this->table.'.is_active', 1 ); 
		$this->db->where( $this->table.'.is_deleted', 0 ); 
		
		//ORDER BY
		if( isset( $params['sort_by'] ) && !empty( $params['sort_by'] ) ) {
			$this->db->order_by( $params['sort_by'], $params['sort_order'] );
		} else {
			$this->db->order_by(  $this->table.'.'.$this->sort_by, $this->sort_order );
		}
		//TO GET ALL RECORD
		if( $count ){
			if (isset( $params['inventory_id'] ) && !empty( $params['inventory_id'] ) ) {
				$this->db->where( $this->table.'.'.'inventory_id', $params['inventory_id'] );
			}
			
			return $this->db->count_all_results();
		} else {
			if( isset( $params['id'] ) && !empty( $params['id'] ) ) {
				$this->db->where( $this->table.'.'.'id', $params['id'] );
				return $this->db->get()->row_array();
			} else if( isset( $params['sales_order_id'] ) && !empty( $params['sales_order_id'] ) ) {
				if( isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
					$this->db->limit( $params['limit'], $params['offset'] );
				} else {
					$this->db->limit( $this->limit );				
				}
				$this->db->where( $this->table.'.'.'sales_order_id', $params['sales_order_id'] );
				return $this->db->get()->result_array();
			} else if( isset( $params['inventory_id'] ) && !empty( $params['inventory_id'] ) ) {
				if( isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
					$this->db->limit( $params['limit'], $params['offset'] );
				} else {
					$this->db->limit( $this->limit );				
				}
				$this->db->where( $this->table.'.'.'inventory_id', $params['inventory_id'] );
				return $this->db->get()->result_array();
			} else {
				//LIMIT
				if( isset( $params['limit'] ) && !empty( $params['limit'] ) && $params['limit'] > 0 ) {
					$this->db->limit( $params['limit'], $params['offset'] );
				} else {
					$this->db->limit( $this->limit );				
				}
				return $this->db->get()->result_array();
			}
		}
	}
	public function get_all_top_items($params){

		$this->db->select("
			SUM(sales_order_items.quantity) as total_quantity, 
			SUM(sales_order_items.total_cost) as total_cost, 
			inventories.name as inventory_name,
			sales_order_items.unit_cost as unit_cost,
		"
		, FALSE); 

		$this->db->join( 'inventories', 'inventories.id = sales_order_items.inventory_id', 'left' );


		if(!empty($params['where'])){
			foreach ($params['where'] as $key => $value) {
				$this->db->where($key, $value);
			}
		}

		$this->db->group_by('sales_order_items.inventory_id');
		$this->db->order_by(  'total_quantity', 'DESC' );

		$query = $this->db->get('sales_order_items');
		$result = $query->result_array();

		return $result;
	}
}