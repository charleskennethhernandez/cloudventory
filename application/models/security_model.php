<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Security_model extends CI_Model {
	protected $table = 'users';
	public function __construct(){
		parent::__construct();
	}
	
	public function save( $data = array() ) {
	
		if( empty ( $data ) ) {
		
			return;
		
		}
		
		if( ! isset ( $data['id'] ) ) {
			$username = $this->db->where( 'username', $data['username'] )->get( $this->table )->row_array();
		
			if( ! empty ( $username ) ) {
			
				return array( 'error' => 'Username already exist!' );
			
			}
		
			$this->db->insert( $this->table, $data );
			
			return $this->db->insert_id();
		
		} else {
		
			$this->db->where( 'id', $data['id'] );
		
			$this->db->update( $this->table, $data ); 
			
			return $data['id'];
			
		}
	
	}
	
	public function delete( $id = 0 ) {
	
		if( $id ) {
		
			$this->db->where( 'id',$id );
			
			$this->db->update( $this->table, array( 'is_deleted', 1 ) );
			
			return true;
		
		}
		
		return false;
	
	}
	
	public function get_account( $id = 0 ) {
	
		if( $id ) {
		
			$this->db->where( 'id', $id );
			
			$this->db->where( 'is_active',1 );
			
			$this->db->where( 'is_deleted', 0 );
			return $this->db->get( $this->table )->row_array();
		
		}
		
		return;
	
	}
	
	public function get_accounts( $limit = 0 ) {
	
		if( $limit ) {
		
			$this->db->limit( $limit );
		
		}
		
		$this->db->where( 'is_active', 1 );
		
		$this->db->where( 'is_deleted', 0 );
	
		return $this->db->order_by( 'id', 'DESC' )->get( $this->table )->result_array();
	
	}
	
	public function authenticate( $username = '', $password = '' ) {
		if( $username == '' && $password == '' ) {
			return;
		}
	
		//CREATED GLOBAL ACCOUNT
		$this->db->where( 'username', $username );
		$this->db->where( 'password', md5( $password ) );
		$this->db->where( 'is_active', 1 );
		$this->db->where( 'is_deleted', 0 );
		$account =  $this->db->get( $this->table )->row_array();
		// echo $this->db->last_query();
		// die();
		if( $account ) {
			$accts = $account;
			$this->session->set_userdata('account_information', $accts);		
			$this->session->set_userdata('account', $accts);		
			return $account;
		} else {
			return array();
		}
	}
	
	public function search_account( $data = array() ) {
	
		if( ! empty( $data ) ) {
		
			//when login
			$account = $this->session->userdata( 'account' );
			
			$this->db->where( 'id', $account->id );
			
			$this->db->where( 'is_active', 1 );
		
			$this->db->where( 'is_deleted', 0 );
			
			$existing_account = $this->db->get( $this->table )->row_array();
			
			if( $existing_account ) {
			
				return true;
			
			} else {
			
				return false;
			
			}
		
		}
		
		return false;
	
	}
}