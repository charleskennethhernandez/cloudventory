<?php if($this->session->flashdata('message_login') || $this->session->flashdata('error_login')): ?>
    <?php
        if($this->session->flashdata('message_login')){
            $message    = $this->session->flashdata('message_login');
        }
        
        if($this->session->flashdata('error_login')){
            $error  = $this->session->flashdata('error_login');
        }
        
        if(function_exists('validation_errors') && validation_errors() != ''){
            $error  = validation_errors();
        }
    ?>        
    <?php if (!empty($message)): ?>
        <div class="alert alert-success a-float" style="top: 2%; left: 0px;">
            <a class="close" data-dismiss="alert">×</a>
            <?php echo $message; ?>
        </div>
    <?php endif; ?>

    <?php if (!empty($error)): ?>
        <div class="alert alert-danger a-float" style="top: 2%; left: 0px;">
            <a class="close" data-dismiss="alert">×</a>
            <?php echo $error; ?>
        </div>
    <?php endif; ?>
<?php endif; ?>
<div class="bg-white pulldown">
    <div class="content content-boxed overflow-hidden">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
                <div class="push-30-t push-50 animated fadeIn">
                    <!-- Login Title -->
                    <div class="text-center">
                        <i class="fa fa-2x fa-circle-o-notch text-primary"></i>
                        <p class="text-muted push-15-t">Best solution for your Inventory and Point of Sale Business</p>
                    </div>
                    <!-- END Login Title -->

                    <!-- Login Form -->
                    <!-- jQuery Validation (.js-validation-login class is initialized in js/pages/base_pages_login.js) -->
                    <!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->
                    <!-- <form class="xjs-validation-login form-horizontal push-30-t" name="frmLogin" method="post"> -->
                    <?php echo form_open( 'security/login', array( 'id'=>'login_form','role'=>'form','class'=>'xjs-validation-login form-horizontal push-30-t' ) ); ?>

                        <div class="form-group">
                            <div class="col-xs-12">
                                <div class="form-material form-material-primary floating">
                                    <input class="form-control" type="text" id="login-username" name="username">
                                    <label for="login-username">Username</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <div class="form-material form-material-primary floating">
                                    <input class="form-control" type="password" id="login-password" name="password">
                                    <label for="login-password">Password</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group hide">
                            <div class="col-xs-6">
                                <label class="css-input switch switch-sm switch-primary">
                                    <input type="checkbox" id="login-remember-me" name="login-remember-me"><span></span> Remember Me?
                                </label>
                            </div>
                            <div class="col-xs-6">
                                <div class="font-s13 text-right push-5-t">
                                    <a href="base_pages_reminder_v2.html">Forgot Password?</a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group push-30-t">
                            <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                                <button class="btn btn-sm btn-block btn-primary" btnLogin>Log in</button>
                            </div>
                        </div>
                    <!-- </form> -->
                    <?php echo form_close(); ?>

                    <!-- END Login Form -->
                </div>
            </div>
        </div>
    </div>
</div>