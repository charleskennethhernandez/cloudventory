
<div class="row">
	<div class="col-md-6 form-horizontal">
		<div class="form-group">
			<label style="margin-top:7px" class="col-md-3 fld-name"><b>Project List</b></label>       
			<div class="col-md-9">
				<select class="form-control select2" id="select_project" tabindex="-1" aria-hidden="true">
					<?php
						for ($i=0; $i < count($projects); $i++) { 
					?>

					<option value="<?php echo $projects[$i]['id']; ?>"><?php echo $projects[$i]['name']; ?> (<?php echo $projects[$i]['project_code']; ?>)</option>
					<?php
						}
					?>

				</select>
			</div>
		</div>
	</div>
</div>

<div id="help_container_list">
	<h3 class="title">Help and Faqs </h3>
	<div class="faq_container">
	</div>
</div>

<div id="help_container_list">
  <H3 class="title">Help and Faqs by superadmin
  </H3>
  <div class="faq_container">
  <table cellspacing="10px" cellpadding="0" border="0" style="width:100%">
    <tbody>
      <tr>
        <td><strong>1. GENERAL</strong></td>
        <td>&nbsp;</td>
      </tr>
      <?php foreach ($faqs as $key => $faq) { ?>
      <tr>
        <td> <?php echo $faq['question']; ?></td>
        <td><img alt="up.png" src="assets/img/cornerstone/up.png" style="float:right; margin:8px 0px 2px 10px" onerror="this.src = '<?php echo base_url(); ?>assets/img/cornerstone/placeholder.png';"></td>
      </tr>
      <tr>
        <td colspan="2" style="display: table-cell;" class="desc-faq">
          <?php echo $faq['answer']; ?>
        </td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
  </div>
</div>

<script>
	$(function() {
		$('select#select_project.select2')
			.select2({
				allowClear : true,
				placeholder : 'Select a Project'
			})
			.val('')
			.trigger('change')
			.on('change', function(e) {
				var $this = $(this); 
				var id = $this.val();
				var $container = $('.faq_container');
				$.get(site_url + 'help/get_list', {
					'project_id' : id
				}, function(data) {
					data = $.parseJSON(data);
					if (data.result) {
						$container.html(data.html);
						for_faq();
					} else {
						modal_trigger('Error', data.message);
					}
				})
				e.stopImmediatePropagation();
			});
	});
</script>
<style>

.faq_container strong {
		color: #38abbe;
		font-size: 13px;
		font-weight: 400;
}
.faq_container p {
		color: #999999;
		font-size: 13px;
		margin: 0;
		text-decoration: none;
}
.faq_container td {
		color: #999999;
		cursor: pointer;
		font-size: 13px;
		padding: 5px 0;
		text-decoration: none;
}
.faq_container .desc-faq {
		color: #676767 !important;
		cursor: default;
		font-size: 13px;
		text-indent: 20px;
}
.faq_container .desc-faq p {
		color: #676767 !important;
		cursor: default;
}

</style>

<script>
$("td[colspan=2]").hide();
    $("td[colspan=2]").addClass('desc-faq');
    $(".faq_container table").click(function(event) {
    event.stopPropagation();
    var $target = $(event.target);
    if ( $target.closest("td").attr("colspan") > 1 ) {
    //$target.slideUp();
    //$target.closest("tr").prev().find("td:first").html("+");
    } else {
    $target.closest("tr").next().find("td[colspan=2]").toggle('fast');
    if ($target.closest("tr").find("td:last-child").html() == '<img style="float:right; margin:8px 0px 2px 10px" src="assets/img/cornerstone/up.png" alt="up.png" onerror="this.src = \'<?php echo base_url(); ?>assets/img/cornerstone/placeholder.png\';">')
    $target.closest("tr").find("td:last-child").html('<img style="float:right; margin:8px 0px 2px 10px" src="assets/img/cornerstone/down.png" alt="down.png" onerror="this.src = \'<?php echo base_url(); ?>assets/img/cornerstone/placeholder.png\';">');
    else
    $target.closest("tr").find("td:last-child").html('<img style="float:right; margin:8px 0px 2px 10px" src="assets/img/cornerstone/up.png" alt="up.png" onerror="this.src = \'<?php echo base_url(); ?>assets/img/cornerstone/placeholder.png\';">');
    }
    });
</script>