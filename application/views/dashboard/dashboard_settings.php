<div id="settings_container_list">
  <H3 class="title">Change Password</H3>
</div>
<div id="settings_container_view">
  <form enctype='multipart/form-data' method="POST" action="<?php echo base_url();?>settings/change_password">
      <div class="col-md-6">
        <div class="row" style="margin:10px 0px">
          <div class="form-group">
            <label for="new_password" class="col-sm-4 control-label"><span class="fld-name">New Password *</span></label>
            <div class="col-sm-6">
              <input type="password" class="form-control" name="new_password" id="new_password" placeholder="">
            </div>
          </div>
        </div>
        <div class="row" style="margin:10px 0px">
          <div class="form-group">
            <label for="secondpassword" class="col-sm-4 control-label"><span class="fld-name">Repeat Password *</span></label>
            <div class="col-sm-6">
              <input type="password" class="form-control" name="secondpassword" id="secondpassword" placeholder="">
            </div>
          </div>
        </div>
        <div class="row" style="margin:10px 0px">
          <div class="form-group">
            <label class="col-sm-4 control-label"></label>
            <div class="col-sm-6">
              <button style="margin-right:10px;" id="btn-changepassword" type="submit" class="btn btn-blue-green pull-left">SAVE</button>
            </div>
          </div>
        </div>
    </form>
</div>
<div id="settings_container_add" class="hide">

</div>
<div id="settings_container_edit">

</div>