<h3 class="title" > 
	My Mortgage
	<a class="btn close-btn pull-right close_mortgage_view"></a>
</h3>

<div class="container-fluid">
	<div class="row" style="margin-row">
		<div class="row">
			
			<!-- yearly payment breakdown start -->
			<div class="col-sm-12 col-md-6 col-lg-6">
				<?php echo $this->load->view('tables/mortgage/yearly_payment_breakdown_table'); ?>
			</div>
			<!-- yearly payment breakdown end -->
			
			<!-- monthly payment breakdown start -->
			<div class="col-sm-12 col-md-6 col-lg-6">
				<?php echo $this->load->view('tables/mortgage/monthly_payment_breakdown_table'); ?>
			</div>
			<!-- monthly payment breakdown end -->

		</div>
</div>