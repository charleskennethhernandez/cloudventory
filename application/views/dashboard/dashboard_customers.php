<div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed bg-gray">
	<?php echo $this->load->view( 'template/side_menu' ); ?>
    <!-- Main Container -->
    <main id="main-container">
        <div class="content" id="system_message_container"></div>
        <?php echo $this->load->view('customers/index'); ?>
    </main>
    <!-- END Main Container -->
    
</div>
