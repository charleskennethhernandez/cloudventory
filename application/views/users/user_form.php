<?php echo asset_js('validation/user_validation.js', true);?>
<div class="col-md-12">
    <!--Labels -->
    <div class="block">
        <div class="block-header">
            <ul class="block-options">
                <li>
                    <button type="button" btnClose><i class="si si-close"></i></button>
                </li>
            </ul>
            <h3 class="block-title">Add New Record</h3>
        </div>
        <div class="block-content block-content-narrow">
            <form id="user_form" class="form-horizontal push-10-t" name="frmuser" method="post" onsubmit="return false;">
                
                <div class="form-group">
                           

                            <div class="col-sm-6">
                                <div class="form-material form-material-success ">
                                    <input class="form-control" type="text" id="firstname" name="firstname" value="<?php echo @$firstname; ?>">
                                    <input class="form-control" type="hidden" id="id" name="id" value="<?php echo @$id; ?>">
                                    <label for="firstname">First Name</label>
                                </div>
                            </div>

                             
                           <div class="col-sm-6">
                                <div class="form-material form-material-success ">
                                    <input class="form-control" type="text" id="middlename" name="middlename" value="<?php echo @$middlename; ?>">
                                    <label for="middlename">Middle Name</label>
                                </div>
                            </div>

                        </div>

                        <div class="form-group">

                             <div class="col-sm-6">
                                <div class="form-material form-material-success ">
                                    <input class="form-control" type="text" id="lastname" name="lastname" value="<?php echo @$lastname; ?>">
                                    <label for="lastname">Last Name</label>
                                </div>
                            </div>
                        </div>

                        <br>

                        <div class="form-group">
                            <div class="col-sm-6">
                                <div class="form-material form-material-success ">
                                    <input class="form-control" type="text" id="phone" name="phone" value="<?php echo @$phone; ?>">
                                    <label for="phone">Phone Number</label>
                                </div>
                            </div>

                             <div class="col-sm-6">
                                <div class="form-material form-material-success">
                                    <select class="form-control" name="user_group_id">
                                        <option value="0">Select User Group</option>
                                        <?php if ($user_groups) { ?>
                                            <?php foreach ($user_groups as $key => $user_group) { ?>
                                                <option <?php if(isset($user_group_id) && ($user_group_id == $user_group['id'])){ echo "selected"; } ?> value="<?php echo $user_group['id']; ?>">
                                                    <?php echo $user_group['name']; ?>
                                                </option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                    <label for="user_group_id">User Group</label>
                                </div>
                            </div>
                        </div>
                        <br>
                         <div class="form-group">
                           
                            <div class="col-sm-6">
                                <div class="form-material form-material-success">
                                    <textarea class="form-control" type="text" id="address" name="address">
                                        <?php echo @$address; ?>
                                    </textarea>
                                    <label for="address">Address</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">


                            <div class="col-sm-6">
                                <div class="form-material form-material-success ">
                                    <input class="form-control" type="text" id="username" name="username" value="<?php echo @$username; ?>">
                                    <label for="username">Username</label>
                                </div>
                            </div>


                            <div class="col-sm-6">
                                <div class="form-material form-material-success ">
                                    <input class="form-control" type="password" id="password" name="password">
                                    <label for="password">Password</label>
                                </div>
                            </div>

                            
                            <div class="form-group">
                            <div class="col-sm-9">
                                <button class="btn btn-sm btn-primary" type="submit">Submit</button>
                            </div>
                        </div>
                            
                        </div>
            </form>
        </div>
    </div>
    <!-- END  Labels -->
</div>