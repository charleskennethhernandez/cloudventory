<div class="col-md-12">
    <!--Labels -->
    <div class="block">
        <div class="block-header">
            <ul class="block-options">
                <li>
                    <button type="button" btnClose><i class="si si-close"></i></button>
                </li>
            </ul>
            <h3 class="block-title">View Record</h3>
        </div>
        <div class="block-content block-content-narrow">

            <div class="row">
                <div class="form-group">
                    <div class="col-sm-6">
                        <div class="form-material form-material-success ">
                            <span><?php echo $name; ?></span>
                            <label for="name">Name</label>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-material form-material-success ">
                            <span><?php echo format_currency($actual_price); ?></span>
                            <label for="description">Capital Price</label>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">

                    <div class="col-sm-6">
                        <div class="form-material form-material-success ">
                            <span><?php echo $description; ?></span>
                            <label for="description">Optional Description</label>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-material form-material-success ">
                            <span><?php echo format_currency($marked_price); ?></span>
                            <label for="description">Selling Price</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">

                    <div class="col-sm-6">
                        <div class="form-material form-material-success ">
                            <span><?php echo $brand_name; ?></span>
                            <label for="description">Inventory Brand</label>
                        </div>
                    </div>

                    

                    <div class="col-sm-6">
                        <div class="form-material form-material-success ">
                            <span><?php echo format_currency($dealer_price); ?></span>
                            <label for="description">Dealer Price</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">

                    <div class="col-sm-6">
                        <div class="form-material form-material-success ">
                            <span><?php echo $inventory_category_name; ?></span>
                            <label for="description">Inventory Category</label>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-material form-material-success ">
                            <span><?php echo $threshold; ?></span>
                            <label for="threshold">Threshold</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">

                    <div class="col-sm-6">
                        <div class="form-material form-material-success ">
                            <span><?php echo $status_id; ?></span>
                            <label for="description">Inventory Status</label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-material form-material-success ">
                            <span><?php echo $quantity; ?></span>
                            <label for="quantity">Initial Quantity</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div id="sales_order_item_list_table_container" class="col-md-12">
                  <?php echo $this->load->view('sales_order_items/sales_order_item_list_table'); ?>
                </div>
            <div>
                
        </div>
    </div>
    <!-- END  Labels -->
</div>