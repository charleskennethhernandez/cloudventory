<?php if (!empty($inventories)) { ?>
    <?php echo form_open( 'inventories/delete_selected', array( 'id' => 'delete_selected', 'data-module' => 'inventories' ) ); ?>
    <?php   
        if( $sort_order == 'ASC' ) {        
            $sort_order = 'DESC';           
        } else {    
            $sort_order = 'ASC';    
        }
    ?>
    <table class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <td class="text-center col-md-"> <input type="checkbox" class="checkall" /> </td>
                <th class="hidden-xs col-md-2"><a href="#" onclick="event.preventDefault(); $('#list_table_container').fadeOut(); $('#list_table_container').load('<?php echo base_url('inventories/form_ajax/'.$form.'/'.$code.'/inventories.name/'.$sort_order); ?>').fadeIn();">Product</a></th>
                <th class="hidden-xs col-md-1"><a href="#" onclick="event.preventDefault(); $('#list_table_container').fadeOut(); $('#list_table_container').load('<?php echo base_url('inventories/form_ajax/'.$form.'/'.$code.'/inventories.description/'.$sort_order); ?>').fadeIn();">Code</a></th>
                <th class="hidden-xs col-md-1"><a href="#" onclick="event.preventDefault(); $('#list_table_container').fadeOut(); $('#list_table_container').load('<?php echo base_url('inventories/form_ajax/'.$form.'/'.$code.'/brands.name/'.$sort_order); ?>').fadeIn();">Brand</a></th>
                <th class="hidden-xs col-md-1"><a href="#" onclick="event.preventDefault(); $('#list_table_container').fadeOut(); $('#list_table_container').load('<?php echo base_url('inventories/form_ajax/'.$form.'/'.$code.'/inventories.status_id/'.$sort_order); ?>').fadeIn();">Status</a></th>
                <th class="hidden-xs col-md-1"><a href="#" onclick="event.preventDefault(); $('#list_table_container').fadeOut(); $('#list_table_container').load('<?php echo base_url('inventories/form_ajax/'.$form.'/'.$code.'/inventories.marked_price/'.$sort_order); ?>').fadeIn();">Selling Price</a></th>
                <?php if ((!isset($form)) || empty($form)): ?>
                    <th class="hidden-xs col-md-1"><a href="#" onclick="event.preventDefault(); $('#list_table_container').fadeOut(); $('#list_table_container').load('<?php echo base_url('inventories/form_ajax/'.$form.'/'.$code.'/inventories.actual_price/'.$sort_order); ?>').fadeIn();">Capital Price</a></th>
                <?php endif; ?>
                <th class="hidden-xs col-md-1"><a href="#" onclick="event.preventDefault(); $('#list_table_container').fadeOut(); $('#list_table_container').load('<?php echo base_url('inventories/form_ajax/'.$form.'/'.$code.'/inventories.dealer_price/'.$sort_order); ?>').fadeIn();">Dealer Price</a></th>
                <th class="hidden-xs col-md-1"><a href="#" onclick="event.preventDefault(); $('#list_table_container').fadeOut(); $('#list_table_container').load('<?php echo base_url('inventories/form_ajax/'.$form.'/'.$code.'/inventories.quantity/'.$sort_order); ?>').fadeIn();">Quantity</a></th>
                <?php if ((!isset($form)) || empty($form)): ?>
                     <th class="hidden-xs col-md-1"><a href="#" onclick="event.preventDefault(); $('#list_table_container').fadeOut(); $('#list_table_container').load('<?php echo base_url('inventories/form_ajax/'.$form.'/'.$code.'/inventories.date_created/'.$sort_order); ?>').fadeIn();">Date Created</a></th>
                <?php endif; ?>
               
                <th class="text-center">Actions</th>
            </tr>
        </thead>
        <tbody>
        	
                <?php foreach($inventories as $_row):?>
                    <tr>
                        <td  class="text-center"> <input type="checkbox" name="inventories[]"  value="<?php echo $_row['id']; ?>" /> </td>
                        <td class="hidden-xs"><?php echo $_row['name'] ?></td>
                        <td class="hidden-xs"><?php echo $_row['description'] ?></td>
                        <td class="hidden-xs"><?php echo get_value_field($_row['brand_id'],'brands','name'); ?></td>
                        <td class="hidden-xs">
                            <?php if ($_row['status_id'] == "1"){
                                $status = "success";
                                $status_name = "Available";
                            } else {
                                $status = "danger";
                                $status_name = "Out of Stock";
                            } ?>
                            <span class="label label-<?php echo $status; ?>">
                                <?php echo $status_name?>
                            </span>
                        </td>
                        <td class="hidden-xs"><strong>PHP <?php echo $_row['marked_price']?></strong></td>
                        <?php if ((!isset($form)) || empty($form)): ?>
                        <td class="hidden-xs"><strong>PHP <?php echo $_row['actual_price']?></strong></td>
                        <?php endif; ?>
                        
                        <td class="hidden-xs"><strong>PHP <?php echo $_row['dealer_price']?></strong></td>
                        <td class="hidden-xs"><strong><?php echo $_row['quantity']?></strong></td>
                        <?php if ((!isset($form)) || empty($form)): ?>
                            <td class="hidden-xs"><?php echo date_time_format($_row['date_created']); ?></td>
                        <?php endif; ?>
                        <!-- <td class="hidden-xs"><?php //echo date_time_format($_row['date_updated)  ?></td> -->
                        <td class="text-center">
                            <div class="btn-group">
                                <?php if ((!isset($form)) || empty($form)): ?>
                                    <?php if (user_permission($account['user_group_id'],'inventories','edit')): ?>
                                        <button data-id="<?php echo $_row['id']?>" btnedit class="btn btn-primary" type="button" data-toggle="tooltip" title="Edit Item" data-module="inventories">
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                    <?php endif; ?>
                                      <?php if (user_permission($account['user_group_id'],'inventories','view')): ?>
                                        <button data-id="<?php echo $_row['id']?>" btnview class="btn btn-warning" type="button" data-toggle="tooltip" title="View Item" data-module="inventories">
                                            <i class="fa fa-eye"></i>
                                        </button>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <button data-quantity="<?php echo $_row['quantity']; ?>" data-id="<?php echo $_row['id']?>" btnAddsalesitem class="btn btn-success" type="button" data-toggle="tooltip" title="Add to Cart">
                                     <i class="fa fa-shopping-cart"></i>
                                    </button>
                                <?php endif; ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach;?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="11">
                        <div class="col-sm-12 col-md-6 col-lg-6">
                            <?php if ((!isset($form)) || empty($form)): ?>
                                <?php if (user_permission($account['user_group_id'],'inventories','delete')): ?>
                                    <button type="submit" class="btn btn-danger btn-sm delete-selected-btn"> <i class="glyphicon glyphicon-trash"></i>Delete Selected</button>
                                <?php endif ?>
                            <?php endif; ?>
                        </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 text-right">
                        <?php echo ( isset( $pagination ) ? $pagination : '' ); ?>
                    </div>
                </td>
            </tr>
        </tfoot>
    </table>
    <?php echo form_close(); ?>
<?php } else { ?>
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h3 class="font-w300 push-15">Warning</h3>
        <p>There are no inventories right now!</p>
    </div>
<?php } ?>