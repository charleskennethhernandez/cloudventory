<?php echo asset_js('validation/inventory_validation.js', true);?>
<div class="col-md-12">
    <!--Labels -->
    <div class="block">
        <div class="block-header">
            <ul class="block-options">
                <li>
                    <button type="button" btnClose><i class="si si-close"></i></button>
                </li>
            </ul>
            <h3 class="block-title">Add New Record</h3>
        </div>
        <div class="block-content block-content-narrow">
            <form id="inventory_form" class="form-horizontal push-10-t" name="frminventory" method="post" onsubmit="return false;">
                
                         <div class="form-group">
                            <div class="col-sm-6">
                                <div class="form-material form-material-success ">
                                    <input class="form-control" type="text" id="name" name="name" value="<?php echo @$name; ?>">
                                    <input class="form-control" type="hidden" id="id" name="id" value="<?php echo @$id; ?>">
                                    <label for="name">Name</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-material form-material-success ">
                                   <div class="input-group">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button">Php</button>
                                        </span>
                                        <input class="form-control" type="text" id="actual_price" name="actual_price" value="<?php echo @$actual_price; ?>">
                                    </div>
                                    <label for="description">Purchase Price per Unit</label>
                                </div>
                            </div>

                        </div>
                        
                        <div class="form-group">

                            <div class="col-sm-6">
                                <div class="form-material form-material-success ">
                                    <textarea class="form-control" id="description" name="description">
                                        <?php echo @$description; ?>
                                    </textarea>
                                    <label for="description">Optional Description</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-material form-material-success ">
                                   <div class="input-group">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button">Php</button>
                                        </span>
                                        <input class="form-control" type="text" id="marked_price" name="marked_price" value="<?php echo @$marked_price; ?>">
                                    </div>
                                    <label for="description">Selling Price per Unit</label>
                                </div>
                            </div>

                        </div>

                        <div class="form-group">

                            <div class="col-sm-6">
                                <div class="form-material form-material-success ">
                                    <select name="brand_id" id="brand_id" class="form-control">
                                        <option value="0">Select Brand</option>
                                        <?php foreach ($brands as $key => $brand) { ?>
                                            <option <?php if(isset($brand_id) && ($brand_id == $brand['id'])) { echo "selected"; } ?> value="<?php echo $brand['id']; ?>"><?php echo $brand['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <label for="description">Inventory Brand</label>
                                </div>
                            </div>

                            

                            <div class="col-sm-6">
                                <div class="form-material form-material-success ">
                                   <div class="input-group">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button">Php</button>
                                        </span>
                                        <input class="form-control" type="text" id="dealer_price" name="dealer_price" value="<?php echo @$dealer_price; ?>">
                                    </div>
                                    <label for="description">Dealer Price per Unit</label>
                                </div>
                            </div>
                        
                        </div>

                        <div class="form-group">

                            <div class="col-sm-6">
                                <div class="form-material form-material-success ">
                                    <select name="category_id" id="category_id" class="form-control">
                                        <option value="0">Select Category</option>
                                        <?php foreach ($categories as $key => $category) { ?>
                                            <option <?php if(isset($category_id) && ($category_id == $category['id'])) { echo "selected"; } ?> value="<?php echo $category['id']; ?>"><?php echo $category['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <label for="description">Inventory Category</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-material form-material-success ">
                                    <input class="form-control" type="text" id="threshold" name="threshold" value="<?php echo @$threshold; ?>">
                                    <label for="threshold">Threshold</label>
                                </div>
                            </div>

                        </div>
                        <div class="form-group">

                            <div class="col-sm-6">
                                <div class="form-material form-material-success ">
                                    <select name="status_id" id="status_id" class="form-control">
                                        <option <?php if(isset($status_id) && ($status_id == "0")){ echo "selected"; } ?>value="0">Select Status</option>
                                        <option <?php if(isset($status_id) && ($status_id == "1")){ echo "selected"; } ?>value="1">Available</option>
                                        <option <?php if(isset($status_id) && ($status_id == "2")){ echo "selected"; } ?>value="2">Out of Stock</option>
                                    </select>
                                    <label for="description">Inventory Status</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-material form-material-success ">
                                    <input class="form-control" type="text" id="quantity" name="quantity" value="<?php echo @$quantity; ?>">
                                    <label for="quantity">Initial Quantity</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-9">
                                <button class="btn btn-sm btn-primary" btnSubmit type="submit">Submit</button>
                            </div>
                        </div>
                    </form>

                    <div id="video_url_container" class="hide">
                        
                    </div>

                    <div id="image_url_container" class="hide">
                        
                    </div>

                    <div id="file_container" class="hide">

                    </div>
            </form>
        </div>
    </div>
    <!-- END  Labels -->
</div>