<?php //echo asset_js('validation/sales_report_validation.js', true);?>
<div class="col-md-12">
    <!--Labels -->
    <div class="block">
        <div class="block-header">
            <ul class="block-options">
                <li>
                    <button type="button" btnClose><i class="si si-close"></i></button>
                </li>
            </ul>
            <h3 class="block-title"><?php echo @$name; ?></h3>
        </div>
        <div class="block-content block-content-narrow">
            <form class="form-horizontal push-10-t" action="<?php echo base_url(); ?>sales_reports/download" method="post" accept-charset="utf-8" id="sales_report_form" enctype="multipart/form-data">  
                 <div class="form-group">
                    <div class="col-sm-6">
                        <div class="form-material form-material-success ">
                            <input class="form-control" type="hidden" id="id" name="id" value="<?php echo @$id; ?>">
                            <input data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" class="js-datepicker form-control" type="text" id="id" name="date_from" value="">
                            <label for="date_from">Date From</label>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-sm-6">
                        <div class="form-material form-material-success ">
                            <input data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" class="js-datepicker form-control" type="text" id="date_to" name="date_to" value="">
                            <label for="date_to">Date To</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9">
                        <button class="btn btn-sm btn-primary" type="submit">Download</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END  Labels -->
</div>


