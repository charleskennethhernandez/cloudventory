<?php if (!empty($sales_reports)) { ?>
    <?php echo form_open( 'sales_reports/delete_selected', array( 'id' => 'delete_selected', 'data-module' => 'sales_reports' ) ); ?>
    <?php   
        if( $sort_order == 'ASC' ) {        
            $sort_order = 'DESC';           
        } else {    
            $sort_order = 'ASC';    
        }
    ?>
    <table class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <td class="text-center col-md-"> <input type="checkbox" class="checkall" /> </td>
                <th class="text-center col-md-">#</th>
                <th class="hidden-xs col-md-3"><a href="#" onclick="event.preventDefault(); $('#list_table_container').fadeOut(); $('#list_table_container').load('<?php echo base_url('sales_reports/form_ajax/'.$code.'/sales_reports.name/'.$sort_order); ?>').fadeIn();">Name</a></th>
                <th class="hidden-xs col-md-3"><a href="#" onclick="event.preventDefault(); $('#list_table_container').fadeOut(); $('#list_table_container').load('<?php echo base_url('sales_reports/form_ajax/'.$code.'/sales_reports.description/'.$sort_order); ?>').fadeIn();">Description</a></th>
                <!-- <th class="hidden-xs col-md-2"><a href="#" onclick="event.preventDefault(); $('#list_table_container').fadeOut(); $('#list_table_container').load('<?php echo base_url('sales_reports/form_ajax/'.$code.'/sales_reports.date_created/'.$sort_order); ?>').fadeIn();">Date Created</a></th> -->
                <!-- <th class="hidden-xs col-md-2"><a href="#" onclick="event.preventDefault(); $('#list_table_container').fadeOut(); $('#list_table_container').load('<?php echo base_url('sales_reports/form_ajax/'.$code.'/sales_reports.date_updated/'.$sort_order); ?>').fadeIn();">Date Updated</a></th> -->
                <th class="text-center">Actions</th>
            </tr>
        </thead>
        <tbody>
        	
                <?php foreach($sales_reports as $_row):?>
                    <tr>
                        <td  class="text-center"> <input type="checkbox" name="sales_reports[]"  value="<?php echo $_row['id']; ?>" /> </td>
                        <td class="text-center"><?php echo $_row['id']?></td>
                        <td class="hidden-xs"><?php echo $_row['name']; ?></td>
                        <td class="hidden-xs"><?php echo $_row['description']; ?></td>
                        <!-- <td class="hidden-xs"><?php //echo date_time_format($_row['date_created']) ?></td> -->
                        <!-- <td class="hidden-xs"><?php //echo date_time_format($_row['date_updated']) ?></td> -->
                        <td class="text-center">
                            <div class="btn-group">
                                <?php if (user_permission($account['user_group_id'],'sales_reports','edit')): ?>
                                    <button data-id="<?php echo $_row['id']?>" data-module="sales_reports" btnedit class="btn btn-primary" type="button" data-toggle="tooltip" title="Edit Item" >
                                        <i class="fa fa-download"></i>
                                    </button>
                                <?php endif ?>
                                
                            </div>
                        </td>
                    </tr>
                <?php endforeach;?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="8">
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <?php if (user_permission($account['user_group_id'],'sales_reports','delete')): ?>
                            <button type="submit" class="btn btn-danger btn-sm delete-selected-btn"> <i class="glyphicon glyphicon-trash"></i>Delete Selected</button>
                        <?php endif ?>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 text-right">
                        <?php echo ( isset( $pagination ) ? $pagination : '' ); ?>
                    </div>
                </td>
            </tr>
        </tfoot>
    </table>
    <?php echo form_close(); ?>
<?php } else { ?>
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h3 class="font-w300 push-15">Warning</h3>
        <p>There are no sales reports right now!</p>
    </div>
<?php } ?>