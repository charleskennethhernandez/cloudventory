<?php echo asset_js('validation/user_group_permission_validation.js', true);?>

<?php if( isset($user_group_permissions) ): ?>
	<?php //echo "<pre>";print_r($user_group_permissions);echo "</pre>"; ?>
<?php echo form_open( 'user_group_permissions/update_selected', array( 'id' => 'user_group_permission_update_selected' ) ); ?>
	<table id="personnel_user_group_permission_list_table" class="table my_prop">
	<thead>
		<tr>
			<td class="col-md-8"> <span class="fld-name"> Module Pages </span> </td>
			<td class="col-md-1"> <span class="fld-name"> &nbsp; </span> </td>
			<td class="col-md-1"> <span class="fld-name"> View </span> </td>
			<td class="col-md-1"> <span class="fld-name"> Add </span> </td>
			<td class="col-md-1"> <span class="fld-name"> Edit </span> </td>
			<td class="col-md-1"> <span class="fld-name"> Delete </span> </td>
		</tr>
	</thead>
	<?php //$modules = get_all_modules(); ?>
	<input name="name" type="hidden"> 
	<tbody>
		<?php $a = 0; ?>
		<?php foreach( $user_group_permissions as $user_group_permission ): ?>
			<tr class="<?php if($a % 2 == 1): echo "unread"; endif; ?> " data-id="<?php echo $user_group_permission['id']; ?>" >
				<td> <?php echo $user_group_permission['title']; ?> </td>
				<td><input class="check_all" type="checkbox" value="1" <?php if ($user_group_permission['user_group_id'] == 1) { echo "disabled";} ?> <?php if ($user_group_permission['view']) { echo "checked";} ?>> </td>
				<td><input name="user_group_permissions[<?php echo $user_group_permission['id']; ?>][view]" type="checkbox" value="1" <?php if ($user_group_permission['user_group_id'] == 1) { echo "disabled";} ?> <?php if ($user_group_permission['view']) { echo "checked";} ?>></td>
				<td><input name="user_group_permissions[<?php echo $user_group_permission['id']; ?>][add]" type="checkbox" value="1" <?php if ($user_group_permission['user_group_id'] == 1) { echo "disabled";} ?> <?php if ($user_group_permission['add']) { echo "checked";} ?>></td>
				<td><input name="user_group_permissions[<?php echo $user_group_permission['id']; ?>][edit]" type="checkbox" value="1" <?php if ($user_group_permission['user_group_id'] == 1) { echo "disabled";} ?> <?php if ($user_group_permission['edit']) { echo "checked";} ?>></td>
				<td><input name="user_group_permissions[<?php echo $user_group_permission['id']; ?>][delete]" type="checkbox" value="1" <?php if ($user_group_permission['user_group_id'] == 1) { echo "disabled";} ?> <?php if ($user_group_permission['delete']) { echo "checked";} ?>></td>
			</tr>
			<?php $a++; ?>
		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="5"> 
				<div class="row">
					<div class="col-sm-12 col-md-6 col-lg-6">
						<button class="btn btn-primary" type="submit"> Update </button>
					</div>
				</div>
			</td>
		</tr>
	</tfoot>
	</table>
<?php echo form_close(); ?>
<?php else: ?>
	<div class="alert alert-warning" role="alert">
		<h6 class="text-center">Please select a user group</h6>
	</div>
<?php endif; ?>