<?php echo asset_js('validation/user_group_validation.js', true);?>
<div class="col-md-12">
    <!--Labels -->
    <div class="block">
        <div class="block-header">
            <ul class="block-options">
                <li>
                    <button type="button" btnClose><i class="si si-close"></i></button>
                </li>
            </ul>
            <h3 class="block-title">Add New Record</h3>
        </div>
        <div class="block-content block-content-narrow">
            <form id="user_group_form" class="form-horizontal push-10-t" name="frmuser_group" method="post" onsubmit="return false;">
                
                         <div class="form-group">
                            <div class="col-sm-6">
                                <div class="form-material form-material-success ">
                                    <input class="form-control" type="text" id="name" name="name" value="<?php echo @$name; ?>">
                                    <input class="form-control" type="hidden" id="id" name="id" value="<?php echo @$id; ?>">
                                    <label for="name">Name</label>
                                </div>
                            </div>

                        </div>
                        
                        <div class="form-group">

                            <div class="col-sm-6">
                                <div class="form-material form-material-success ">
                                    <textarea class="form-control" id="description" name="description"><?php echo @$description; ?></textarea>
                                    <label for="description">Optional Description</label>
                                </div>
                            </div>

                        </div>

                        <div class="form-group">
                            <div class="col-sm-9">
                                <button class="btn btn-sm btn-primary" type="submit">Submit</button>
                            </div>
                        </div>
            </form>
        </div>
    </div>
    <!-- END  Labels -->
</div>