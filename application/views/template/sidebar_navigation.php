<?php
// echo "<pre>";
// print_r($data['modules']);
// die();
?>
<!-- Sidebar -->
<nav id="sidebar">
    <!-- Sidebar Scroll Container -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
        <div class="sidebar-content">
            <!-- Side Header -->
            <div class="side-header side-content bg-white-op">
                <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                    <i class="fa fa-times"></i>
                </button>
                <!-- Themes functionality initialized in App() -> uiHandleTheme() -->
                <div class="btn-group pull-right hide">
                    <button class="btn btn-link text-gray dropdown-toggle" data-toggle="dropdown" type="button">
                        <i class="si si-drop"></i>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right font-s13 sidebar-mini-hide">
                        <li>
                            <a data-toggle="theme" data-theme="default" tabindex="-1" href="javascript:void(0)">
                                <i class="fa fa-circle text-default pull-right"></i> <span class="font-w600">Default</span>
                            </a>
                        </li>
                        <li>
                            <a data-toggle="theme" data-theme="assets/css/themes/amethyst.min.css" tabindex="-1" href="javascript:void(0)">
                                <i class="fa fa-circle text-amethyst pull-right"></i> <span class="font-w600">Amethyst</span>
                            </a>
                        </li>
                        <li>
                            <a data-toggle="theme" data-theme="assets/css/themes/city.min.css" tabindex="-1" href="javascript:void(0)">
                                <i class="fa fa-circle text-city pull-right"></i> <span class="font-w600">City</span>
                            </a>
                        </li>
                        <li>
                            <a data-toggle="theme" data-theme="assets/css/themes/flat.min.css" tabindex="-1" href="javascript:void(0)">
                                <i class="fa fa-circle text-flat pull-right"></i> <span class="font-w600">Flat</span>
                            </a>
                        </li>
                        <li>
                            <a data-toggle="theme" data-theme="assets/css/themes/modern.min.css" tabindex="-1" href="javascript:void(0)">
                                <i class="fa fa-circle text-modern pull-right"></i> <span class="font-w600">Modern</span>
                            </a>
                        </li>
                        <li>
                            <a data-toggle="theme" data-theme="assets/css/themes/smooth.min.css" tabindex="-1" href="javascript:void(0)">
                                <i class="fa fa-circle text-smooth pull-right"></i> <span class="font-w600">Smooth</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <a class="h5 text-white" href="index.html">
                    <i class="fa fa-circle-o-notch text-primary"></i> <span class="h4 font-w600 sidebar-mini-hide">ne</span>
                </a>
            </div>
            <!-- END Side Header -->

            <!-- Side Content -->
            <div class="side-content">
                <ul class="nav-main">
                    <li>
                        <a href="<?php echo base_url(); ?>dashboard">
                            <i class="si si-speedometer"></i>
                            <span class="sidebar-mini-hide">Dashboard</span>
                        </a>
                    </li>
                    <?php if (( isset($nav_infos) )) { ?>
                    <?php foreach ($nav_infos as $key => $module): ?>
                        <?php if ($module['parent']): ?>
                            <li class="open">
                                <a class="nav-submenu" data-toggle="nav-submenu" href="#">
                                    <i class="<?php echo $module['class-icon']; ?>"></i>
                                    <span class="sidebar-mini-hide"><?php echo $module['title']; ?>
                                    </span>
                                </a>
                                <ul>
                                    <?php foreach ($nav_infos as $key => $sub_mod): ?>
                                        <?php if ($sub_mod['parent_id'] == $module['id']) { ?>
                                            <li>
                                                <a href="<?php echo base_url(); ?><?php echo $sub_mod['module']; ?>">
                                                    <?php echo $sub_mod['title']; ?>
                                                </a>
                                            </li>
                                           
                                        <?php } ?>
                                    <?php endforeach ?>
                                </ul>
                            </li>
                        <?php endif ?>
                    <?php endforeach ?>
                    <?php } ?>
                    <li>
                        <a tabindex="-1" href="<?php echo site_url('security/logout');?>">
                            <i class="si si-logout pull-right"></i>Log out
                        </a>
                    </li>
                    <!-- <li class="open">
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#">
                            <i class="si si-grid"></i><span class="sidebar-mini-hide">Inventory</span>
                        </a>
                        <ul>
                            <li>
                                <a href="<?php echo base_url(); ?>inventories">Inventory List</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>inventory_categories">Manage Categories</a>
                            </li>
                            <li class="hide">
                                <a href="<?php echo base_url(); ?>inventory_attributes">Manage Attributes</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>brands">Manage Brands</a>
                            </li>
                        </ul>
                    </li>
                    <li class="open">
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#">
                            <i class="si si-grid"></i><span class="sidebar-mini-hide">Orders</span>
                        </a>
                        <ul>
                            <li>
                                <a href="<?php echo site_url('sales_orders');?>">Purchase Orders</a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('invoice');?>">Invoices</a>
                            </li>
                        </ul>
                    </li>
                    <li class="open">
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#">
                            <i class="si si-grid"></i><span class="sidebar-mini-hide">Reports</span>
                        </a>
                        <ul>
                            <li>
                                <a href="<?php echo site_url('sales_orders');?>">Purchase Orders</a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('invoice');?>">Invoices</a>
                            </li>
                        </ul>
                    </li>
                    <li class="open">
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#">
                            <i class="si si-grid"></i><span class="sidebar-mini-hide">Settings</span>
                        </a>
                        <ul>
                            <li>
                                <a href="<?php echo site_url('users');?>">Users</a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('user_groups');?>">Roles</a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('user_group_permissions');?>">Permissions</a>
                            </li>
                             
                        </ul>
                    </li> -->
                </ul>
            </div>
            <!-- END Side Content -->
        </div>
        <!-- Sidebar Content -->
    </div>
    <!-- END Sidebar Scroll Container -->
</nav>
<!-- END Sidebar -->