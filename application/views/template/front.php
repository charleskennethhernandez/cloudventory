<span id="current_week" class="hide"><?php echo $current_week_sales;?></span>
<span id="last_week" class="hide"><?php echo $last_week_sales;?> </span>
<span id="days" class="hide"><?php echo $days;?> </span>

<!-- Page Header -->
<div class="content bg-image overflow-hidden" style="background-image: url('assets/img/photos/photo3@2x.jpg');">
    <div class="push-50-t push-15">
        <h1 class="h2 text-white animated zoomIn">Dashboard</h1>
        <h2 class="h5 text-white-op animated zoomIn">Welcome Administrator</h2>
    </div>
</div>
<!-- END Page Header -->

<!-- Stats -->
<div class="content bg-white border-b">
    <div class="row items-push text-uppercase">
        <div class="col-xs-6 col-sm-3">
            <div class="font-w700 text-gray-darker animated fadeIn">Product Sales</div>
            <div class="text-muted animated fadeIn"><small><i class="si si-calendar"></i> Today</small></div>
            <?php 
                if ($today_sales['grand_total']) {
                    $today_sale = $today_sales['grand_total'];
                } else {
                    $today_sale = 0;
                }
            ?>
            <a class="h2 font-w300 text-primary animated flipInX" href="<?php echo base_url();?>sales_orders">Php <?php echo format_currency($today_sale); ?></a>
        </div>
        <div class="col-xs-6 col-sm-3">
            <div class="font-w700 text-gray-darker animated fadeIn">Product Sales</div>
            <div class="text-muted animated fadeIn"><small><i class="si si-calendar"></i> This Month</small></div>
            <a class="h2 font-w300 text-primary animated flipInX" href="<?php echo base_url();?>sales_orders">Php <?php echo format_currency($monthly_sales['grand_total']) ?></a>
        </div>
        <div class="col-xs-6 col-sm-3">
            <div class="font-w700 text-gray-darker animated fadeIn">Total Sales</div>
            <div class="text-muted animated fadeIn"><small><i class="si si-calendar"></i> All Time</small></div>
            <a class="h2 font-w300 text-primary animated flipInX" href="<?php echo base_url();?>sales_orders">Php <?php echo format_currency($total_sales['grand_total']) ?></a>
        </div>
        <div class="col-xs-6 col-sm-3">
            <div class="font-w700 text-gray-darker animated fadeIn">Average Sale</div>
            <div class="text-muted animated fadeIn"><small><i class="si si-calendar"></i> All Time</small></div>
            <a class="h2 font-w300 text-primary animated flipInX" href="<?php echo base_url();?>sales_orders">Php <?php echo format_currency($average_sale) ?></a>
        </div>
    </div>
</div>
<!-- END Stats -->

<!-- Page Content -->
<div class="content">
    <div class="row">
        <div class="col-lg-8">
            <!-- Main Dashboard Chart -->
            <div class="block">
                <div class="block-header">
                    <ul class="block-options">
                        <li>
                            <button type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">Weekly Overview</h3>
                </div>
                <div class="block-content block-content-full bg-gray-lighter text-center">
                    <!-- Chart.js Charts (initialized in js/pages/base_pages_dashboard.js), for more examples you can check out http://www.chartjs.org/docs/ -->
                    <div style="height: 374px;"><canvas class="js-dash-chartjs-lines"></canvas></div>
                </div>
                <div class="block-content text-center">
                    <div class="row items-push text-center">
                        <div class="col-xs-6 col-lg-3">
                            <div class="push-10"><i class="si si-graph fa-2x"></i></div>
                            <div class="h5 font-w300 text-muted">+ 205 Sales</div>
                        </div>
                        <div class="col-xs-6 col-lg-3">
                            <div class="push-10"><i class="si si-users fa-2x"></i></div>
                            <div class="h5 font-w300 text-muted">+ 25% Clients</div>
                        </div>
                        <div class="col-xs-6 col-lg-3 visible-lg">
                            <div class="push-10"><i class="si si-star fa-2x"></i></div>
                            <div class="h5 font-w300 text-muted">+ 10 Ratings</div>
                        </div>
                        <div class="col-xs-6 col-lg-3 visible-lg">
                            <div class="push-10"><i class="si si-share fa-2x"></i></div>
                            <div class="h5 font-w300 text-muted">+ 35 Followers</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Main Dashboard Chart -->
        </div>
        <div class="col-lg-4">
            <!-- Latest Sales Widget -->
            <div class="block">
                <div class="block-header">
                    <ul class="block-options">
                        <li>
                            <button type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">Latest Sales</h3>
                </div>
                <div class="block-content bg-gray-lighter">
                    <div class="row items-push">
                        <div class="col-xs-3">
                            <div class="text-muted"><small><i class="si si-calendar"></i> 24 hrs</small></div>
                            <div class="font-w600"><?php echo $sales_orders_count; ?> Sales</div>
                        </div>
                        <div class="col-xs-3">
                            <div class="text-muted"><small><i class="si si-calendar"></i> 7 days</small></div>
                            <div class="font-w600"><?php echo $sales_orders_count_week; ?> Sales</div>
                        </div>
                        <div class="col-xs-6 h1 font-100 text-right"> <?php echo format_currency($today_sale); ?></div>
                    </div>
                </div>
                <div class="block-content">
                    <div class="pull-t pull-r-l">
                        <!-- Slick slider (.js-slider class is initialized in App() -> uiHelperSlick()) -->
                        <!-- For more info and examples you can check out http://kenwheeler.github.io/slick/ -->
                        <div class="js-slider remove-margin-b" data-slider-autoplay="true" data-slider-autoplay-speed="2500">
                            <!-- <div> -->
                                <?php
                                    // echo "<pre>";
                                    // print_r($chunk_sales_orders);
                                    // echo "</pre>";
                                ?>
                                <?php if ($chunk_sales_orders): ?>
                                    <?php foreach ($chunk_sales_orders as $key => $ar_sales_orders) { ?>
                                    <div>
                                        <table class="table remove-margin-b font-s13">
                                            <tbody>
                                                <?php foreach ($ar_sales_orders as $key => $order): ?>
                                                    <tr>
                                                        <td class="font-w600 col-xs-5">
                                                            <a target="_BLANK" href="<?php echo base_url();?>invoices/index/<?php echo $order->id; ?>">
                                                                <?php echo get_value_field($order->customer_id,'customers','firstname').' '.get_value_field($order->customer_id,'customers','lastname'); ?>
                                                            </a>
                                                        </td>
                                                        <td class="hidden-xs text-muted text-right" style="width: 70px;">
                                                            <?php echo date( 'H:i' , strtotime($order->date_created)); ?>
                                                        </td>
                                                        <td class="font-w600 text-success text-right" style="width: 70px;">
                                                            + Php <?php echo format_currency($order->grand_total); ?>
                                                        </td>
                                                    </tr>
                                                <?php endforeach ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <?php } ?>
                                <?php endif ?>
                            <!-- </div> -->
                        </div>
                        <!-- END Slick slider -->
                    </div>
                </div>
            </div>
            <!-- END Latest Sales Widget -->
        </div>
    </div>
   
</div>
<!-- END Page Content -->



