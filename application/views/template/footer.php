		<!-- Footer -->
            <footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
              <!-- Copyright Info -->
                <div class="pull-right">
                    Crafted with <i class="fa fa-heart text-city"></i> by <a class="font-w600" href="#" target="_blank">Yggdrasil Solutions</a>
                </div>
                <div class="pull-left">
                    <a class="font-w600" href="#" target="_blank">Cloudventory</a> &copy; <span class="js-year-copy"></span>
                </div>
              <!-- END Copyright Info -->
            </footer>
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->

        <!-- END Login Content -->
        
        <!-- Login Footer -->
        <div class="pulldown push-30-t text-center animated fadeInUp">
            <small class="text-muted"><span class="js-year-copy"></span> &copy; Cloudventory v1</small>
        </div>
        <!-- END Login Footer -->

        <script type="text/javascript">
          var BASE_URL = '<?php echo site_url()?>';
        </script>

        <script type="text/javascript">
        function clear_all_fields(){
            $('input[type="text"]').val('');
            $('input[type="hidden"]').val('');
            $('textarea').html('');
            $('textarea').val('');
            $('input[type="checkbox"]').attr('checked', false);
            $('input[type="radio"]').attr('checked', false);
            $('input[type="file"]').val('');
            $('select').val('');
            $('.amt_lbl').html('');
            $("label.error").hide();
            $(".error").removeClass("error");
        }
        </script>

     
         <script type="text/javascript" src="<?php echo site_url('assets/js/core/jquery.min.js')?>"></script>
         <script type="text/javascript" src="<?php echo site_url('assets/js/plugins/select2/select2.full.min.js')?>"></script>
         <script type="text/javascript" src="<?php echo site_url('assets/js/plugins/jquery-validation/jquery.validate.min.js')?>"></script>
         <script type="text/javascript" src="<?php echo site_url('assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')?>"></script>
         <script type="text/javascript" src="<?php echo site_url('assets/js/plugins/masked-inputs/jquery.maskedinput.min.js')?>"></script>
         <script type="text/javascript" src="<?php echo site_url('assets/js/plugins/slick/slick.min.js')?>"></script>
         <script type="text/javascript" src="<?php echo site_url('assets/js/plugins/chartjs/Chart.min.js')?>"></script>
         <script type="text/javascript" src="<?php echo site_url('assets/js/main.js')?>"></script>
         <script type="text/javascript" src="<?php echo site_url('assets/js/core/bootstrap.min.js')?>"></script>
         <script type="text/javascript" src="<?php echo site_url('assets/js/core/jquery.slimscroll.min.js')?>"></script>
         <script type="text/javascript" src="<?php echo site_url('assets/js/core/jquery.scrollLock.min.js')?>"></script>
         <script type="text/javascript" src="<?php echo site_url('assets/js/core/jquery.appear.min.js')?>"></script>
         <script type="text/javascript" src="<?php echo site_url('assets/js/core/jquery.countTo.min.js')?>"></script>
         <script type="text/javascript" src="<?php echo site_url('assets/js/core/jquery.placeholder.min.js')?>"></script>
         <script type="text/javascript" src="<?php echo site_url('assets/js/core/js.cookie.min.js')?>"></script>
         <script type="text/javascript" src="<?php echo site_url('assets/js/pages/base_forms_validation.js')?>"></script>
         <script type="text/javascript" src="<?php echo site_url('assets/js/plugins/datatables/jquery.dataTables.min.js')?>"></script>
         <script type="text/javascript" src="<?php echo site_url('assets/js/pages/base_tables_datatables.js')?>"></script>
         <script type="text/javascript" src="<?php echo site_url('assets/js/app.js')?>"></script>
         <script type="text/javascript" src="<?php echo site_url('assets/js/api-service.js')?>"></script>
         <script type="text/javascript" src="<?php echo site_url('assets/js/messages.js')?>"></script>
         <script type="text/javascript" src="<?php echo site_url('assets/js/plugins/sweetalert/sweetalert.min.js')?>"></script>
         <script type="text/javascript" src="<?php echo site_url('assets/js/validation.js')?>"></script>
         <script type="text/javascript" src="<?php echo site_url('assets/js/plugins/datatables/jquery.dataTables.min.js')?>"></script>
         <script type="text/javascript" src="<?php echo site_url('assets/js/pages/base_tables_datatables.js')?>"></script>
         <script type="text/javascript" src="<?php echo site_url('assets/js/plugins/dropzonejs/dropzone.js')?>"></script>
         <script type="text/javascript" src="<?php echo site_url('assets/js/general.js')?>"></script>
        <?php echo asset_js('waitMe.min.js', true);?>

        <!-- Page JS Code -->
        <script>
            jQuery(function() {
                // Init page helpers (Appear plugin)
                App.initHelpers(['appear','select2']);
            });
        </script>

    </body>
</html>