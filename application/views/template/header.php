<!DOCTYPE html>
<!--[if IE 9]>         <html class="ie9 no-focus"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-focus"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>Cloudventory - Point of Sale and Inventory System</title>

        <meta name="description" content="Dynamic Inventory System used to help SME Business">
        <meta name="author" content="yggdrasil solutions">
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="<?php echo site_url('assets/img/favicons/favicon.png')?>">

        <link rel="icon" type="image/png" href="<?php echo site_url('assets/img/favicons/favicon-16x16.png')?>" sizes="16x16">
        <link rel="icon" type="image/png" href="<?php echo site_url('assets/img/favicons/favicon-32x32.png')?>" sizes="32x32">
        <link rel="icon" type="image/png" href="<?php echo site_url('assets/img/favicons/favicon-96x96.png')?>" sizes="96x96">
        <link rel="icon" type="image/png" href="<?php echo site_url('assets/img/favicons/favicon-160x160.png')?>" sizes="160x160">
        <link rel="icon" type="image/png" href="<?php echo site_url('assets/img/favicons/favicon-192x192.png')?>" sizes="192x192">

        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo site_url('assets/img/favicons/apple-touch-icon-57x57.png')?>">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo site_url('assets/img/favicons/apple-touch-icon-60x60.png')?>">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo site_url('assets/img/favicons/apple-touch-icon-72x72.png')?>">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo site_url('assets/img/favicons/apple-touch-icon-76x76.png')?>">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo site_url('assets/img/favicons/apple-touch-icon-114x114.png')?>">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo site_url('assets/img/favicons/apple-touch-icon-120x120.png')?>">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo site_url('assets/img/favicons/apple-touch-icon-144x144.png')?>">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo site_url('assets/img/favicons/apple-touch-icon-152x152.png')?>">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo site_url('assets/img/favicons/apple-touch-icon-180x180.png')?>">
        <!-- END Icons -->

        <!-- Stylesheets -->
        
        <!-- Web fonts -->
        <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/googlefont.css')?>">
        <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/bootstrap.min.css')?>">
        <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/oneui.css')?>">
        <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/js/plugins/select2/select2.min.css')?>">
        <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/js/plugins/select2/select2-bootstrap.min.css')?>">
        <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/js/plugins/datatables/jquery.dataTables.min.css')?>">
        <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/js/plugins/sweetalert/sweetalert.min.css')?>">
        <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/js/plugins/dropzonejs/dropzone.min.css')?>">
        <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/js/plugins/datatables/jquery.dataTables.min.css')?>">
        <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/js/plugins/slick/slick.min.css')?>">
        <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/js/plugins/slick/slick-theme.min.css')?>">
		<?php echo asset_css('waitMe.min.css', true);?>
		

		<?php echo asset_js('jquery.js', true);?>
		<?php echo asset_js('bootstrap.min.js', true);?>
		<?php echo asset_js('jquery_validation/jquery.validate.min.js', true);?>
		<?php echo asset_js('jquery_validation/additional-methods.min.js', true);?>
		<?php echo asset_js('jquery_ui.js', true);?>
		<?php //echo asset_js('spin.min.js', true);?>
		<?php echo asset_js('custom/cloudventory.js', true);?>
		<?php //echo asset_js('custom/myscript.js', true);?>
		<?php echo asset_js('custom/global_script.js', true);?>
		<?php echo asset_js('jquery.classyloader.js', true);?>
		<?php echo asset_js('jquery.classyloader.min.js', true);?>
		
		<?php //echo asset_js('pages/base_pages_dashboard.js', true);?>


		<?php $company_item = $this->config->item('company'); ?>
		<script type="text/javascript" src="<?php echo base_url('assets/tinymce/js/tinymce/tinymce.min.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo asset_url('assets/plugins/select2/js/select2.full.min.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url('assets/plugins/chosen/chosen.jquery.js	') ?>"></script>
    </head>

	<!-- Use Html5Shiv in order to allow IE render HTML5 -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<input type="hidden" id="dev_url" value="<?php echo str_replace('client/', '', base_url()).'developers/'; ?>">

	<script type="text/javascript">
		var base_url = '<?php echo base_url(); ?>';
		var site_url = '<?php echo site_url(); ?>';
	    var server_url = '<?php echo $_SERVER['SERVER_NAME']; ?>';
		
		function modal_trigger(alert_type, modal_body, custom_class){
			// old 
			// $('.modal-title').html(alert_type);
			// $('.alert_msg').html(modal_body);
			
			// $('#alertbox').removeClass('hide');
			// $('#confirmbox').addClass('hide');
			// $('#popup_container').addClass('hide');
			
			// $('#cornerstone_modal').modal('show');

			if (typeof custom_class === 'undefined') { custom_class = ''; }
			if (typeof alert_type === 'undefined') { alert_type = 'Alert'; }
			var $modal = $('#cornerstone_modal');
			$modal.find('.modal-title').html(alert_type);
			$modal.find('.alert_msg').html(modal_body);
			$modal.find('.additional_msg').html('').addClass('hide');
			$modal.find('#alertbox').removeClass('hide');
			$modal.find('#confirmbox').addClass('hide')
			$modal.find('#popup_container').addClass('hide');
			$modal.find('.alert-confirm').addClass(custom_class);
			$modal.modal('show');
		}
		
		function confirm_trigger(message, additional, proceedFunction, autohide, custom_class){
			// old
			// $('.confirm_msg').html(message);
			// $('#popup_container').addClass('hide');
			
			// $('#alertbox').addClass('hide');
			// $('#confirmbox').removeClass('hide');
			// $('#cornerstone_modal').modal('show');
			// 
			if (typeof additional === 'undefined') { additional = ''; }
			if (typeof message === 'undefined') { message = 'Confirm'; }
			if (typeof autohide === 'undefined') { autohide = true; }
			var $modal = $('#cornerstone_modal');
			$modal.find('.confirm_msg').html(message);
			if (additional === '') {
				$modal.find('.additional_msg').addClass('hide');
			} else {
				$modal.find('.additional_msg').html(additional).removeClass('hide');
			}
			$modal.find('#popup_container').addClass('hide');
			$modal.find('#alertbox').addClass('hide');
			$modal.find('#confirmbox').removeClass('hide');
			$modal.find('.alert-confirm').addClass(custom_class);
			$modal.modal('show');
			if (typeof(proceedFunction) === 'function') {
				function updatedproceedFunction() {
					proceedFunction();
					if (autohide) {
						$modal.modal('hide');
					}
				}
				$modal.find('#custom_confirm_button').removeClass('hide');
				$modal.find('#cofirm_button').addClass('hide');
				$modal.on('click', '#custom_confirm_button', updatedproceedFunction);
			} else {
				$modal.find('#custom_confirm_button').addClass('hide').off('click');
				$modal.find('#cofirm_button').removeClass('hide');
			}
		}
		function clear_fields(id){
			$('#'+id+" input[type=text]").val('');
			$('#'+id+"input[type=email]").val('');
			$('#'+id+"input[type=hidden]").val('');
			$('#'+id+'select').val('');
		}
		function custom_modal_trigger(modal_title, body){
		

			// old
			// $('.body_container').html(body);
			// $('.modal-title').html(modal_title);
			
			// $('#confirmbox').addClass('hide');
			// $('#alertbox').addClass('hide');
			// $('#popup_container').removeClass('hide');
			
			// $('#cornerstone_modal').modal('show');
			// 
			if (typeof modal_title === 'undefined') { modal_title = ''; }
			if (typeof body === 'undefined') { body = ''; }
			var $modal = $('#cornerstone_modal');
			$modal.find('.body_container').html(body);
			$modal.find('.additional_msg').html('');
			$modal.find('.modal-title').html(modal_title);
			$modal.find('#confirmbox').addClass('hide');
			$modal.find('#alertbox').addClass('hide');
			$modal.find('#popup_container').removeClass('hide');
			$modal.modal('show');
		}
	</script>
	</head>
	<body>
		<!--class="body-bg"-->
		<?php if($this->session->flashdata('message') || $this->session->flashdata('error')): ?>
	        <?php
	            //lets have the flashdata overright "$message" if it exists
	            if($this->session->flashdata('message')){
	                $message	= $this->session->flashdata('message');
	            }
	            
	            if($this->session->flashdata('error')){
	                $error	= $this->session->flashdata('error');
	            }
	            
	            if(function_exists('validation_errors') && validation_errors() != ''){
	                $error	= validation_errors();
	            }
	        ?>        
	        <?php if (!empty($message)): ?>
	            <div class="alert alert-success a-float">
	                <a class="close" data-dismiss="alert">×</a>
	                <?php echo $message; ?>
	            </div>
	        <?php endif; ?>
	    
	        <?php if (!empty($error)): ?>
	            <div class="alert alert-danger a-float">
	                <a class="close" data-dismiss="alert">×</a>
	                <?php echo $error; ?>
	            </div>
	        <?php endif; ?>
	    <?php endif; ?>