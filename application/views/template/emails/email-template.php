<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <title><?php //echo ($subject) ? $subject : "Emial Template"; ?></title>
    <style type="text/css">
        <!--
        a.link {
            color: #0075b6;
            text-decoration:none;
            font-family: 'Open Sans', sans-serif;
            font-size: 12px;
        }
        p {
            font-family: 'Open Sans', sans-serif;
            font-size: 12px;
            color: #333333;
            padding: 0px 20px 0px;
            text-align: justify;
            line-height: 18px;
        }
        h2 {
            font-family: 'Arial';
            font-size: 19px;
            color: #038bd7;
            padding: 0px 20px 0px;
            text-align: justify;
            font-weight: bold;
        }
        .intro{
            font-size: 17px;
            padding: 0px;
            color: #656565;
            font-weight: bold;

        }
        .body-title{
            color: #c32228;
            font-size: 17px;
            padding: 0px;
            font-weight: bold;
            border-bottom: 1px solid #ececec;
            padding: 10px 0px;
        }
        .fld-name {
            color: #36abbd;font-size: 13px;
        }
        -->
    </style>
</head>
<body bgcolor="white" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
    <div align="center">
        <!-- ImageReady Slices (newsletterV.jpg) -->
        <table id="Table_01" width="600" height="700" border="0" cellpadding="0" cellspacing="0" style="background: #EAEAEA">
            <tr>
                <td height="35" colspan="8" valign="top">
                    <div style="background-color: white; color: #bfc2c3; font-family: 'Open Sans', sans-serif; font-size: 13px; padding: 9px 20px; display: flex; line-height: 35px; height: 60px;border-bottom:3px solid #ececec">
                        <img alt="" src="http://creation-site-internet-moulins.com/projects/cornerstone/assets/img/cornerstone/logo1.png">
                        <?php //echo img(array('src'=> base_url('assets/images/logo-lhoopa.png'), 'style' => 'height: 38px; margin-right: 10px; float: left', 'height'=>'38'));?>
                    </div>
                    <?php //#echo img(array("src"=>theme_img("newsletter_01.jpg"), "width"=>"600", "height"=>"322"));?>
                </td>
            </tr>
            <tr>
                <td colspan="8" valign="top" style="height:50px;background-color:white;padding: 0px 20px; font-family: 'Open Sans', sans-serif; font-size: 11px; text-align: justify; line-height: 20px;">    
                    <p style="font-size: 17px;padding: 0px;color: #656565;font-weight: bold;"> Hello <?php echo $information['firstname']; ?>! </p>
                    <?php echo $information['message'] ?>
                </td>
            </tr>
            <tr>
                <td colspan="8" valign="top" style="background-color:white;padding: 0px 20px; font-family: 'Open Sans', sans-serif; font-size: 11px; text-align: justify; line-height: 20px;">
                    <p class="body-title" style=" color: #c32228;font-size: 17px;padding: 0px; font-weight: bold;border-bottom: 1px solid #ececec;padding: 10px 0px;font-family: 'Open Sans', sans-serif;"> INFORMATION </p>
                    <div class="fld-name" style="color: #36abbd;font-size: 13px;font-family: 'Open Sans', sans-serif;">Name : <?php echo $information['firstname'].' '.$information['lastname'];?></div>
                    <div class="fld-name" style="color: #36abbd;font-size: 13px;font-family: 'Open Sans', sans-serif;">Email : <span style="text-decoration:none"><?php echo $information['email'];?></span></div>
                    <div class="fld-name" style="color: #36abbd;font-size: 13px;font-family: 'Open Sans', sans-serif;">Username : <?php echo $information['username'];?></div>
                    <div class="fld-name" style="color: #36abbd;font-size: 13px;font-family: 'Open Sans', sans-serif;">Password : <?php echo $information['confirm_password'];?></div>
                </td>
            </tr>
            <tr>
                <td style="height: 62px;">
                    <img alt="" src="http://creation-site-internet-moulins.com/projects/cornerstone/assets/img/cornerstone/footer.jpg">
                </td>
            </tr>
            <tr>
                <td colspan="8" height="35" valign="top" style="height: 35px; overflow: hidden; width: 50%">
                    <div style="padding-left:10px;color: #777; font-family: 'Open Sans', sans-serif; font-size: 11px; padding: 9px 10px 9px 10px; display: flex; line-height: 35px; height: 35px; background: white;position: relative">
                        <div style="float: right; text-align: right; width: auto;">
                            <b style="float: left;">2014 by <a href="lhoopa.com" style="color:#24447C;text-decoration:none">Lhoopa.com</a> . All rights reserved </b>
<!--                             <a href="https://www.facebook.com/lhoopa" style="float: left; margin-left: 10px; line-height: normal !important;">
                                <?php //echo img(array('src'=>theme_img('fb.png'),'style'=>'height: 35px; width: 35px;', 'height'=>'35')); ?>
                            </a>
                            <a href="https://twitter.com/lhoopadotcom" style="float: left; margin-left: 10px; line-height: normal !important;">
                                <?php //echo img(array('src'=>theme_img('tweet.png'),'style'=>'height: 35px; width: 35px;', 'height'=>'35')); ?>
                            </a>
                            <a href="mailto:support@lhoopa.com" style="float: left; margin-left: 10px; line-height: normal !important;">
                                <?php //echo img(array('src'=> theme_img('email.png'),'style'=> 'height: 35px; width: 35px;', 'height'=>'35')); ?>
                            </a> -->
                        </div>
                    </div>
                </td>
            </tr>
        </table>
        <!-- End ImageReady Slices -->
    </div>
</body>
</html>