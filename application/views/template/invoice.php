<html>
<head>
  	<style>
		html, body { font-family:Helvetica; font-size: 14px;}
	    @page { margin: 180px 10px; }
	    #header { position: fixed; left: 0px; top: -180px; right: 0px; height: 150px; }
	    #footer { position: fixed; left: 0px; bottom: -180px; right: 0px; height: 150px;}
	    #footer .page:after { content: counter(page, upper-roman); }
	    .half {width: 50%;}
	    .whole {width: 100%}
		.text-right{text-align: right}
		.text-left{text-align: left}
		.text-center{text-align: center}
		.items tr td{ border: 1px solid black; }
		/*#content{margin-top: 200px;}*/
		table { width: 100%;}
  	</style>

<body>
  	<div id="header">
  		<table>
  			<tr>
  				<td class="half" style="width:49.5% !important">
  					<div id="original" class="whole" style="float:left">
			            <h3 style="color:red; text-align:right">CUSTOMER'S COPY</h3>

			            <div class="page_break">
			                <div class="col-md-6 block-content block-content-narrow" style="padding-left:20px;padding-right:20px;">
			                      <!-- Invoice Info -->
			                      <center>
			                          <b style="font-size:18px">NATION FIREWORKS</b>
			                          <br>
			                          <address style="font-size:14px">
			                              STO NINO, Baliuag Bulacan<br>
			                              ARMANDO S. RAMOS - Prop<br>
			                              <i class="si si-call-end"></i> 0922 843 2681; 0910 298 3724
			                          </address>
			                      </center>
			                      	<br>
			                    <div class="row items-push-2x">
			                        <table>
			                          <tr>
			                            <td><b>Customer Name:</b><?php echo $sales_order['firstname'].' '.$sales_order['lastname']; ?></td>
			                            <td><b>Date:</b></td>
			                          </tr>

			                          <tr>
			                            <td><b>Address:</b></td>
			                            <td><b>Terms:</b></td>
			                          </tr>
			                        </table>
			                    </div>
			                </div>
			            </div>
			    	</div>
  				</td>
  				<td class="half" style="width:1% !important;border:1px dashed #e7e7e7">
  					&nbsp;
  				</td>
  				<td class="half" style="width:49.5% !important">
  					<div id="original" class="whole" style="float:left">
			            <h3 style="color:red; text-align:right">OWNER'S COPY</h3>

			            <div class="page_break">
			                <div class="col-md-6 block-content block-content-narrow" style="padding-left:20px;padding-right:20px;">
			                      <!-- Invoice Info -->
			                      <center>
			                          <b style="font-size:18px">NATION FIREWORKS</b>
			                          <br>
			                          <address style="font-size:14px">
			                              STO NINO, Baliuag Bulacan<br>
			                              ARMANDO S. RAMOS - Prop<br>
			                              <i class="si si-call-end"></i> 0922 843 2681; 0910 298 3724
			                          </address>
			                      </center>
			                      	<br>
			                    <div class="row items-push-2x">
			                        <table>
			                          <tr>
			                            <td><b>Customer Name:</b><?php echo $sales_order['firstname'].' '.$sales_order['lastname']; ?></td>
			                            <td><b>Date:</b></td>
			                          </tr>

			                          <tr>
			                            <td><b>Address:</b></td>
			                            <td><b>Terms:</b></td>
			                          </tr>
			                        </table>
			                    </div>
			                </div>
			            </div>
			    	</div>
  				</td>
  			</tr>
  		</table>
  	</div>

  	<div id="footer">
  		<table>
  			<tr>
  				<td class="half" style="width:49.5% !important">
  					<div id="original" class="whole" style="float:left">
			    		<div class="row">
					        <table>
					          <tr>
					            <td><b>Prepared <br><br>By______________________________</b></td>
					            <td><b>Received the above goods order<br><br>By_____________________________</b></td>
					          </tr>
					        </table>
			      		</div>
			      		<!-- Footer -->
					    
			      		<p class="hidden-print text-muted text-center"><small>Thank you very much for doing business with us. We look forward to working with you again!</small></p>
			      		<p style="font-style:italic; color:#7e7e7e; font-size:12px">NOTE: This is a computer-generated document and serves as your acknowledgement receipt.</p>
			  		</div>
  				</td>
  				<td class="half" style="width:1% !important;border:1px dashed #e7e7e7">
  					&nbsp;
  				</td>
  				<td class="half" style="width:49.5% !important">
  					<div id="original" class="whole" style="float:left">
			    		<div class="row">
					        <table>
					          <tr>
					            <td><b>Prepared <br><br>By______________________________</b></td>
					            <td><b>Received the above goods order<br><br>By_____________________________</b></td>
					          </tr>
					        </table>
			      		</div>
			      		<!-- Footer -->
					    
			      		<p class="hidden-print text-muted text-center"><small>Thank you very much for doing business with us. We look forward to working with you again!</small></p>
			      		<p style="font-style:italic; color:#7e7e7e; font-size:12px">NOTE: This is a computer-generated document and serves as your acknowledgement receipt.</p>
			  		</div>
  				</td>
  			</tr>
  		</table>
  	</div>
  	
  	<div id="content">
    	<table>
	    	<tr>
	    		<td class="half" style="width:49.5% !important">
	    			<!-- Table -->
                  	<div class="whole">
                      <table class="table table-bordered table-hover items">
                          <thead>
                              <tr>
                                  <th class="text-center">QTY</th>
                                  <th class="text-center">DESCRIPTION</th>
                                  <th class="text-right">UNIT PRICE</th>
                                  <th class="text-right">AMOUNT</th>
                              </tr>
                          </thead>
                          <tbody>
                              <?php if ($sales_order_items) { $a = 1; ?>
                                  <?php foreach ($sales_order_items as $key => $sales_order_item) { ?>
                                  <tr>
                                      <td class="text-center col-md-1">
                                          <span class="badge badge-primary"><?php echo $sales_order_item['quantity']; ?></span>
                                      </td>

                                      <td class="text-left col-md-9">
                                          <!-- <p class="font-w600 push-10"> -->
										<?php echo substr(get_value_field($sales_order_item['inventory_id'],'inventories','name'), 0, 250); ?>-<?php echo substr(get_value_field($sales_order_item['inventory_id'],'inventories','description'), 0, 250); ?>
                                          <!-- </p> -->
                                          
                                      </td>
                                      
                                      <td class="text-right col-md-1"><?php echo $sales_order_item['unit_cost']; ?></td>
                                      <td class="text-right col-md-1"><?php echo $sales_order_item['total_cost']; ?></td>
                                  </tr>
                              <?php $a++; } } ?>

                              <tr>
                                  <td colspan="3" class="font-w700 text-uppercase text-right">Grand Total</td>
                                  <td class="font-w700 text-right">Php <?php echo $sales_order['grand_total']; ?></td>
                              </tr>
                              <?php if ($sales_order['total_discount'] != "0.00") { ?>
                                  <tr>
                                      <td colspan="3" class="font-w700 text-uppercase text-right">Total Discount</td>
                                      <td class="font-w700 text-right">Php <?php echo $sales_order['total_discount']; ?></td>
                                  </tr>
                              <?php } ?>
                              <tr class="active">
                                  <td colspan="3" class="font-w600 text-right">Amount Paid</td>
                                  <td class="text-right">Php <?php echo $sales_order['amount_paid']; ?></td>
                              </tr>
                          </tbody>
                      </table>
                  	</div>
	    		</td>
  				<td class="half" style="width:1% !important;border:1px dashed #e7e7e7">&nbsp;</td>
	    		<td class="half" style="width:49.5% !important">
	    			<!-- Table -->
                  	<div class="whole">
                      <table class="table table-bordered table-hover items">
                          <thead>
                              <tr>
                                  <th class="text-center">QTY</th>
                                  <th class="text-center">DESCRIPTION</th>
                                  <th class="text-right">UNIT PRICE</th>
                                  <th class="text-right">AMOUNT</th>
                              </tr>
                          </thead>
                          <tbody>
                              <?php if ($sales_order_items) { $a = 1; ?>
                                  <?php foreach ($sales_order_items as $key => $sales_order_item) { ?>
                                  <tr>
                                      <td class="text-center col-md-1">
                                          <span class="badge badge-primary"><?php echo $sales_order_item['quantity']; ?></span>
                                      </td>

                                      <td class="text-left col-md-9">
                                          <!-- <p class="font-w600 push-10"> -->
										<?php echo substr(get_value_field($sales_order_item['inventory_id'],'inventories','name'), 0, 250); ?>-<?php echo substr(get_value_field($sales_order_item['inventory_id'],'inventories','description'), 0, 250); ?>
                                          <!-- </p> -->
                                          
                                      </td>
                                      
                                      <td class="text-right col-md-1"><?php echo $sales_order_item['unit_cost']; ?></td>
                                      <td class="text-right col-md-1"><?php echo $sales_order_item['total_cost']; ?></td>
                                  </tr>
                              <?php $a++; } } ?>

                              <tr>
                                  <td colspan="3" class="font-w700 text-uppercase text-right">Grand Total</td>
                                  <td class="font-w700 text-right">Php <?php echo $sales_order['grand_total']; ?></td>
                              </tr>
                              <?php if ($sales_order['total_discount'] != "0.00") { ?>
                                  <tr>
                                      <td colspan="3" class="font-w700 text-uppercase text-right">Total Discount</td>
                                      <td class="font-w700 text-right">Php <?php echo $sales_order['total_discount']; ?></td>
                                  </tr>
                              <?php } ?>
                              <tr class="active">
                                  <td colspan="3" class="font-w600 text-right">Amount Paid</td>
                                  <td class="text-right">Php <?php echo $sales_order['amount_paid']; ?></td>
                              </tr>
                          </tbody>
                      </table>
                  	</div>
	    		</td>
	    	</tr>
	    </table>

	    <?php if ($sales_order_items_2): ?>
	    	<table style="page-break-before: always";>
		    	<tr>
	  				<td class="half" style="width:49.5% !important">
		    			<!-- Table -->
	                  	<div class="whole">
	                      <table class="table table-bordered table-hover items">
	                          <thead>
	                              <tr>
	                                  <th class="text-center">QTY</th>
	                                  <th class="text-center">DESCRIPTION</th>
	                                  <th class="text-right">UNIT PRICE</th>
	                                  <th class="text-right">AMOUNT</th>
	                              </tr>
	                          </thead>
	                          <tbody>
	                              <?php if ($sales_order_items_2) { $a = 1; ?>
	                                  <?php foreach ($sales_order_items_2 as $key => $sales_order_item) { ?>
	                                  <tr>
	                                      <td class="text-center col-md-1">
	                                          <span class="badge badge-primary"><?php echo $sales_order_item['quantity']; ?></span>
	                                      </td>

	                                      <td class="text-left col-md-9">
	                                          <!-- <p class="font-w600 push-10"> -->
											<?php echo substr(get_value_field($sales_order_item['inventory_id'],'inventories','name'), 0, 250); ?>-<?php echo substr(get_value_field($sales_order_item['inventory_id'],'inventories','description'), 0, 250); ?>
	                                          <!-- </p> -->
	                                          
	                                      </td>
	                                      
	                                      <td class="text-right col-md-1"><?php echo $sales_order_item['unit_cost']; ?></td>
	                                      <td class="text-right col-md-1"><?php echo $sales_order_item['total_cost']; ?></td>
	                                  </tr>
	                              <?php $a++; } } ?>

	                              <tr>
	                                  <td colspan="3" class="font-w700 text-uppercase text-right">Grand Total</td>
	                                  <td class="font-w700 text-right">Php <?php echo $sales_order['grand_total']; ?></td>
	                              </tr>
	                              <?php if ($sales_order['total_discount'] != "0.00") { ?>
	                                  <tr>
	                                      <td colspan="3" class="font-w700 text-uppercase text-right">Total Discount</td>
	                                      <td class="font-w700 text-right">Php <?php echo $sales_order['total_discount']; ?></td>
	                                  </tr>
	                              <?php } ?>
	                              <tr class="active">
	                                  <td colspan="3" class="font-w600 text-right">Amount Paid</td>
	                                  <td class="text-right">Php <?php echo $sales_order['amount_paid']; ?></td>
	                              </tr>
	                          </tbody>
	                      </table>
	                  	</div>
		    		</td>
		    		<td class="half" style="width:1% !important;border:1px dashed #e7e7e7">
	  					&nbsp;
	  				</td>
	  				<td class="half" style="width:49.5% !important">
		    			<!-- Table -->
	                  	<div class="whole">
	                      <table class="table table-bordered table-hover items">
	                          <thead>
	                              <tr>
	                                  <th class="text-center">QTY</th>
	                                  <th class="text-center">DESCRIPTION</th>
	                                  <th class="text-right">UNIT PRICE</th>
	                                  <th class="text-right">AMOUNT</th>
	                              </tr>
	                          </thead>
	                          <tbody>
	                              <?php if ($sales_order_items_2) { $a = 1; ?>
	                                  <?php foreach ($sales_order_items_2 as $key => $sales_order_item) { ?>
	                                  <tr>
	                                      <td class="text-center col-md-1">
	                                          <span class="badge badge-primary"><?php echo $sales_order_item['quantity']; ?></span>
	                                      </td>

	                                      <td class="text-left col-md-9">
	                                          <!-- <p class="font-w600 push-10"> -->
												<?php echo substr(get_value_field($sales_order_item['inventory_id'],'inventories','name'), 0, 250); ?>-<?php echo substr(get_value_field($sales_order_item['inventory_id'],'inventories','description'), 0, 250); ?>
	                                          <!-- </p> -->
	                                          
	                                      </td>
	                                      
	                                      <td class="text-right col-md-1"><?php echo $sales_order_item['unit_cost']; ?></td>
	                                      <td class="text-right col-md-1"><?php echo $sales_order_item['total_cost']; ?></td>
	                                  </tr>
	                              <?php $a++; } } ?>

	                              <tr>
	                                  <td colspan="3" class="font-w700 text-uppercase text-right">Grand Total</td>
	                                  <td class="font-w700 text-right">Php <?php echo $sales_order['grand_total']; ?></td>
	                              </tr>
	                              <?php if ($sales_order['total_discount'] != "0.00") { ?>
	                                  <tr>
	                                      <td colspan="3" class="font-w700 text-uppercase text-right">Total Discount</td>
	                                      <td class="font-w700 text-right">Php <?php echo $sales_order['total_discount']; ?></td>
	                                  </tr>
	                              <?php } ?>
	                              <tr class="active">
	                                  <td colspan="3" class="font-w600 text-right">Amount Paid</td>
	                                  <td class="text-right">Php <?php echo $sales_order['amount_paid']; ?></td>
	                              </tr>
	                          </tbody>
	                      </table>
	                  	</div>
		    		</td>
		    	</tr>
		    </table>
	    <?php endif ?>

	    <?php if ($sales_order_items_3): ?>
	    	<table style="page-break-before: always";>
		    	<tr>
	  				<td class="half" style="width:49.5% !important">
		    			<!-- Table -->
	                  	<div class="whole">
	                      <table class="table table-bordered table-hover items">
	                          <thead>
	                              <tr>
	                                  <th class="text-center">QTY</th>
	                                  <th class="text-center">DESCRIPTION</th>
	                                  <th class="text-right">UNIT PRICE</th>
	                                  <th class="text-right">AMOUNT</th>
	                              </tr>
	                          </thead>
	                          <tbody>
	                              <?php if ($sales_order_items_3) { $a = 1; ?>
	                                  <?php foreach ($sales_order_items_3 as $key => $sales_order_item) { ?>
	                                  <tr>
	                                      <td class="text-center col-md-1">
	                                          <span class="badge badge-primary"><?php echo $sales_order_item['quantity']; ?></span>
	                                      </td>

	                                      <td class="text-left col-md-9">
	                                          <!-- <p class="font-w600 push-10"> -->
											<?php echo substr(get_value_field($sales_order_item['inventory_id'],'inventories','name'), 0, 250); ?>-<?php echo substr(get_value_field($sales_order_item['inventory_id'],'inventories','description'), 0, 250); ?>
	                                          <!-- </p> -->
	                                          
	                                      </td>
	                                      
	                                      <td class="text-right col-md-1"><?php echo $sales_order_item['unit_cost']; ?></td>
	                                      <td class="text-right col-md-1"><?php echo $sales_order_item['total_cost']; ?></td>
	                                  </tr>
	                              <?php $a++; } } ?>

	                              <tr>
	                                  <td colspan="3" class="font-w700 text-uppercase text-right">Grand Total</td>
	                                  <td class="font-w700 text-right">Php <?php echo $sales_order['grand_total']; ?></td>
	                              </tr>
	                              <?php if ($sales_order['total_discount'] != "0.00") { ?>
	                                  <tr>
	                                      <td colspan="3" class="font-w700 text-uppercase text-right">Total Discount</td>
	                                      <td class="font-w700 text-right">Php <?php echo $sales_order['total_discount']; ?></td>
	                                  </tr>
	                              <?php } ?>
	                              <tr class="active">
	                                  <td colspan="3" class="font-w600 text-right">Amount Paid</td>
	                                  <td class="text-right">Php <?php echo $sales_order['amount_paid']; ?></td>
	                              </tr>
	                          </tbody>
	                      </table>
	                  	</div>
		    		</td>
		    		<td class="half" style="width:1% !important;border:1px dashed #e7e7e7">
	  					&nbsp;
	  				</td>
	  				<td class="half" style="width:49.5% !important">
		    			<!-- Table -->
	                  	<div class="whole">
	                      <table class="table table-bordered table-hover items">
	                          <thead>
	                              <tr>
	                                  <th class="text-center">QTY</th>
	                                  <th class="text-center">DESCRIPTION</th>
	                                  <th class="text-right">UNIT PRICE</th>
	                                  <th class="text-right">AMOUNT</th>
	                              </tr>
	                          </thead>
	                          <tbody>
	                              <?php if ($sales_order_items_3) { $a = 1; ?>
	                                  <?php foreach ($sales_order_items_3 as $key => $sales_order_item) { ?>
	                                  <tr>
	                                      <td class="text-center col-md-1">
	                                          <span class="badge badge-primary"><?php echo $sales_order_item['quantity']; ?></span>
	                                      </td>

	                                      <td class="text-left col-md-9">
	                                          <!-- <p class="font-w600 push-10"> -->
												<?php echo substr(get_value_field($sales_order_item['inventory_id'],'inventories','name'), 0, 250); ?>-<?php echo substr(get_value_field($sales_order_item['inventory_id'],'inventories','description'), 0, 250); ?>
	                                          <!-- </p> -->
	                                          
	                                      </td>
	                                      
	                                      <td class="text-right col-md-1"><?php echo $sales_order_item['unit_cost']; ?></td>
	                                      <td class="text-right col-md-1"><?php echo $sales_order_item['total_cost']; ?></td>
	                                  </tr>
	                              <?php $a++; } } ?>

	                              <tr>
	                                  <td colspan="3" class="font-w700 text-uppercase text-right">Grand Total</td>
	                                  <td class="font-w700 text-right">Php <?php echo $sales_order['grand_total']; ?></td>
	                              </tr>
	                              <?php if ($sales_order['total_discount'] != "0.00") { ?>
	                                  <tr>
	                                      <td colspan="3" class="font-w700 text-uppercase text-right">Total Discount</td>
	                                      <td class="font-w700 text-right">Php <?php echo $sales_order['total_discount']; ?></td>
	                                  </tr>
	                              <?php } ?>
	                              <tr class="active">
	                                  <td colspan="3" class="font-w600 text-right">Amount Paid</td>
	                                  <td class="text-right">Php <?php echo $sales_order['amount_paid']; ?></td>
	                              </tr>
	                          </tbody>
	                      </table>
	                  	</div>
		    		</td>
		    	</tr>
		    </table>
	    <?php endif ?>

	    <?php if ($sales_order_items_4): ?>
	    	<table style="page-break-before: always";>
		    	<tr>
	  				<td class="half" style="width:49.5% !important">
		    			<!-- Table -->
	                  	<div class="whole">
	                      <table class="table table-bordered table-hover items">
	                          <thead>
	                              <tr>
	                                  <th class="text-center">QTY</th>
	                                  <th class="text-center">DESCRIPTION</th>
	                                  <th class="text-right">UNIT PRICE</th>
	                                  <th class="text-right">AMOUNT</th>
	                              </tr>
	                          </thead>
	                          <tbody>
	                              <?php if ($sales_order_items_4) { $a = 1; ?>
	                                  <?php foreach ($sales_order_items_4 as $key => $sales_order_item) { ?>
	                                  <tr>
	                                      <td class="text-center col-md-1">
	                                          <span class="badge badge-primary"><?php echo $sales_order_item['quantity']; ?></span>
	                                      </td>

	                                      <td class="text-left col-md-9">
	                                          <!-- <p class="font-w600 push-10"> -->
											<?php echo substr(get_value_field($sales_order_item['inventory_id'],'inventories','name'), 0, 250); ?>-<?php echo substr(get_value_field($sales_order_item['inventory_id'],'inventories','description'), 0, 250); ?>
	                                          <!-- </p> -->
	                                          
	                                      </td>
	                                      
	                                      <td class="text-right col-md-1"><?php echo $sales_order_item['unit_cost']; ?></td>
	                                      <td class="text-right col-md-1"><?php echo $sales_order_item['total_cost']; ?></td>
	                                  </tr>
	                              <?php $a++; } } ?>

	                              <tr>
	                                  <td colspan="3" class="font-w700 text-uppercase text-right">Grand Total</td>
	                                  <td class="font-w700 text-right">Php <?php echo $sales_order['grand_total']; ?></td>
	                              </tr>
	                              <?php if ($sales_order['total_discount'] != "0.00") { ?>
	                                  <tr>
	                                      <td colspan="3" class="font-w700 text-uppercase text-right">Total Discount</td>
	                                      <td class="font-w700 text-right">Php <?php echo $sales_order['total_discount']; ?></td>
	                                  </tr>
	                              <?php } ?>
	                              <tr class="active">
	                                  <td colspan="3" class="font-w600 text-right">Amount Paid</td>
	                                  <td class="text-right">Php <?php echo $sales_order['amount_paid']; ?></td>
	                              </tr>
	                          </tbody>
	                      </table>
	                  	</div>
		    		</td>
		    		<td class="half" style="width:1% !important;border:1px dashed #e7e7e7">
	  					&nbsp;
	  				</td>
	  				<td class="half" style="width:49.5% !important">
		    			<!-- Table -->
	                  	<div class="whole">
	                      <table class="table table-bordered table-hover items">
	                          <thead>
	                              <tr>
	                                  <th class="text-center">QTY</th>
	                                  <th class="text-center">DESCRIPTION</th>
	                                  <th class="text-right">UNIT PRICE</th>
	                                  <th class="text-right">AMOUNT</th>
	                              </tr>
	                          </thead>
	                          <tbody>
	                              <?php if ($sales_order_items_4) { $a = 1; ?>
	                                  <?php foreach ($sales_order_items_4 as $key => $sales_order_item) { ?>
	                                  <tr>
	                                      <td class="text-center col-md-1">
	                                          <span class="badge badge-primary"><?php echo $sales_order_item['quantity']; ?></span>
	                                      </td>

	                                      <td class="text-left col-md-9">
	                                          <!-- <p class="font-w600 push-10"> -->
												<?php echo substr(get_value_field($sales_order_item['inventory_id'],'inventories','name'), 0, 250); ?>-<?php echo substr(get_value_field($sales_order_item['inventory_id'],'inventories','description'), 0, 250); ?>
	                                          <!-- </p> -->
	                                          
	                                      </td>
	                                      
	                                      <td class="text-right col-md-1"><?php echo $sales_order_item['unit_cost']; ?></td>
	                                      <td class="text-right col-md-1"><?php echo $sales_order_item['total_cost']; ?></td>
	                                  </tr>
	                              <?php $a++; } } ?>

	                              <tr>
	                                  <td colspan="3" class="font-w700 text-uppercase text-right">Grand Total</td>
	                                  <td class="font-w700 text-right">Php <?php echo $sales_order['grand_total']; ?></td>
	                              </tr>
	                              <?php if ($sales_order['total_discount'] != "0.00") { ?>
	                                  <tr>
	                                      <td colspan="3" class="font-w700 text-uppercase text-right">Total Discount</td>
	                                      <td class="font-w700 text-right">Php <?php echo $sales_order['total_discount']; ?></td>
	                                  </tr>
	                              <?php } ?>
	                              <tr class="active">
	                                  <td colspan="3" class="font-w600 text-right">Amount Paid</td>
	                                  <td class="text-right">Php <?php echo $sales_order['amount_paid']; ?></td>
	                              </tr>
	                          </tbody>
	                      </table>
	                  	</div>
		    		</td>
		    	</tr>
		    </table>
	    <?php endif ?>

	    <?php if ($sales_order_items_5): ?>
	    	<table style="page-break-before: always";>
		    	<tr>
	  				<td class="half" style="width:49.5% !important">
		    			<!-- Table -->
	                  	<div class="whole">
	                      <table class="table table-bordered table-hover items">
	                          <thead>
	                              <tr>
	                                  <th class="text-center">QTY</th>
	                                  <th class="text-center">DESCRIPTION</th>
	                                  <th class="text-right">UNIT PRICE</th>
	                                  <th class="text-right">AMOUNT</th>
	                              </tr>
	                          </thead>
	                          <tbody>
	                              <?php if ($sales_order_items_5) { $a = 1; ?>
	                                  <?php foreach ($sales_order_items_5 as $key => $sales_order_item) { ?>
	                                  <tr>
	                                      <td class="text-center col-md-1">
	                                          <span class="badge badge-primary"><?php echo $sales_order_item['quantity']; ?></span>
	                                      </td>

	                                      <td class="text-left col-md-9">
	                                          <!-- <p class="font-w600 push-10"> -->
											<?php echo substr(get_value_field($sales_order_item['inventory_id'],'inventories','name'), 0, 250); ?>-<?php echo substr(get_value_field($sales_order_item['inventory_id'],'inventories','description'), 0, 250); ?>
	                                          <!-- </p> -->
	                                          
	                                      </td>
	                                      
	                                      <td class="text-right col-md-1"><?php echo $sales_order_item['unit_cost']; ?></td>
	                                      <td class="text-right col-md-1"><?php echo $sales_order_item['total_cost']; ?></td>
	                                  </tr>
	                              <?php $a++; } } ?>

	                              <tr>
	                                  <td colspan="3" class="font-w700 text-uppercase text-right">Grand Total</td>
	                                  <td class="font-w700 text-right">Php <?php echo $sales_order['grand_total']; ?></td>
	                              </tr>
	                              <?php if ($sales_order['total_discount'] != "0.00") { ?>
	                                  <tr>
	                                      <td colspan="3" class="font-w700 text-uppercase text-right">Total Discount</td>
	                                      <td class="font-w700 text-right">Php <?php echo $sales_order['total_discount']; ?></td>
	                                  </tr>
	                              <?php } ?>
	                              <tr class="active">
	                                  <td colspan="3" class="font-w600 text-right">Amount Paid</td>
	                                  <td class="text-right">Php <?php echo $sales_order['amount_paid']; ?></td>
	                              </tr>
	                          </tbody>
	                      </table>
	                  	</div>
		    		</td>
		    		<td class="half" style="width:1% !important;border:1px dashed #e7e7e7">
	  					&nbsp;
	  				</td>
	  				<td class="half" style="width:49.5% !important">
		    			<!-- Table -->
	                  	<div class="whole">
	                      <table class="table table-bordered table-hover items">
	                          <thead>
	                              <tr>
	                                  <th class="text-center">QTY</th>
	                                  <th class="text-center">DESCRIPTION</th>
	                                  <th class="text-right">UNIT PRICE</th>
	                                  <th class="text-right">AMOUNT</th>
	                              </tr>
	                          </thead>
	                          <tbody>
	                              <?php if ($sales_order_items_5) { $a = 1; ?>
	                                  <?php foreach ($sales_order_items_5 as $key => $sales_order_item) { ?>
	                                  <tr>
	                                      <td class="text-center col-md-1">
	                                          <span class="badge badge-primary"><?php echo $sales_order_item['quantity']; ?></span>
	                                      </td>

	                                      <td class="text-left col-md-9">
	                                          <!-- <p class="font-w600 push-10"> -->
												<?php echo substr(get_value_field($sales_order_item['inventory_id'],'inventories','name'), 0, 250); ?>-<?php echo substr(get_value_field($sales_order_item['inventory_id'],'inventories','description'), 0, 250); ?>
	                                          <!-- </p> -->
	                                          
	                                      </td>
	                                      
	                                      <td class="text-right col-md-1"><?php echo $sales_order_item['unit_cost']; ?></td>
	                                      <td class="text-right col-md-1"><?php echo $sales_order_item['total_cost']; ?></td>
	                                  </tr>
	                              <?php $a++; } } ?>

	                              <tr>
	                                  <td colspan="3" class="font-w700 text-uppercase text-right">Grand Total</td>
	                                  <td class="font-w700 text-right">Php <?php echo $sales_order['grand_total']; ?></td>
	                              </tr>
	                              <?php if ($sales_order['total_discount'] != "0.00") { ?>
	                                  <tr>
	                                      <td colspan="3" class="font-w700 text-uppercase text-right">Total Discount</td>
	                                      <td class="font-w700 text-right">Php <?php echo $sales_order['total_discount']; ?></td>
	                                  </tr>
	                              <?php } ?>
	                              <tr class="active">
	                                  <td colspan="3" class="font-w600 text-right">Amount Paid</td>
	                                  <td class="text-right">Php <?php echo $sales_order['amount_paid']; ?></td>
	                              </tr>
	                          </tbody>
	                      </table>
	                  	</div>
		    		</td>
		    	</tr>
		    </table>
	    <?php endif ?>

	    <?php if ($sales_order_items_6): ?>
	    	<table style="page-break-before: always";>
		    	<tr>
	  				<td class="half" style="width:49.5% !important">
		    			<!-- Table -->
	                  	<div class="whole">
	                      <table class="table table-bordered table-hover items">
	                          <thead>
	                              <tr>
	                                  <th class="text-center">QTY</th>
	                                  <th class="text-center">DESCRIPTION</th>
	                                  <th class="text-right">UNIT PRICE</th>
	                                  <th class="text-right">AMOUNT</th>
	                              </tr>
	                          </thead>
	                          <tbody>
	                              <?php if ($sales_order_items_6) { $a = 1; ?>
	                                  <?php foreach ($sales_order_items_6 as $key => $sales_order_item) { ?>
	                                  <tr>
	                                      <td class="text-center col-md-1">
	                                          <span class="badge badge-primary"><?php echo $sales_order_item['quantity']; ?></span>
	                                      </td>

	                                      <td class="text-left col-md-9">
	                                          <!-- <p class="font-w600 push-10"> -->
											<?php echo substr(get_value_field($sales_order_item['inventory_id'],'inventories','name'), 0, 250); ?>-<?php echo substr(get_value_field($sales_order_item['inventory_id'],'inventories','description'), 0, 250); ?>
	                                          <!-- </p> -->
	                                          
	                                      </td>
	                                      
	                                      <td class="text-right col-md-1"><?php echo $sales_order_item['unit_cost']; ?></td>
	                                      <td class="text-right col-md-1"><?php echo $sales_order_item['total_cost']; ?></td>
	                                  </tr>
	                              <?php $a++; } } ?>

	                              <tr>
	                                  <td colspan="3" class="font-w700 text-uppercase text-right">Grand Total</td>
	                                  <td class="font-w700 text-right">Php <?php echo $sales_order['grand_total']; ?></td>
	                              </tr>
	                              <?php if ($sales_order['total_discount'] != "0.00") { ?>
	                                  <tr>
	                                      <td colspan="3" class="font-w700 text-uppercase text-right">Total Discount</td>
	                                      <td class="font-w700 text-right">Php <?php echo $sales_order['total_discount']; ?></td>
	                                  </tr>
	                              <?php } ?>
	                              <tr class="active">
	                                  <td colspan="3" class="font-w600 text-right">Amount Paid</td>
	                                  <td class="text-right">Php <?php echo $sales_order['amount_paid']; ?></td>
	                              </tr>
	                          </tbody>
	                      </table>
	                  	</div>
		    		</td>
		    		<td class="half" style="width:1% !important;border:1px dashed #e7e7e7">
	  					&nbsp;
	  				</td>
	  				<td class="half" style="width:49.5% !important">
		    			<!-- Table -->
	                  	<div class="whole">
	                      <table class="table table-bordered table-hover items">
	                          <thead>
	                              <tr>
	                                  <th class="text-center">QTY</th>
	                                  <th class="text-center">DESCRIPTION</th>
	                                  <th class="text-right">UNIT PRICE</th>
	                                  <th class="text-right">AMOUNT</th>
	                              </tr>
	                          </thead>
	                          <tbody>
	                              <?php if ($sales_order_items_6) { $a = 1; ?>
	                                  <?php foreach ($sales_order_items_6 as $key => $sales_order_item) { ?>
	                                  <tr>
	                                      <td class="text-center col-md-1">
	                                          <span class="badge badge-primary"><?php echo $sales_order_item['quantity']; ?></span>
	                                      </td>

	                                      <td class="text-left col-md-9">
	                                          <!-- <p class="font-w600 push-10"> -->
												<?php echo substr(get_value_field($sales_order_item['inventory_id'],'inventories','name'), 0, 250); ?>-<?php echo substr(get_value_field($sales_order_item['inventory_id'],'inventories','description'), 0, 250); ?>
	                                          <!-- </p> -->
	                                          
	                                      </td>
	                                      
	                                      <td class="text-right col-md-1"><?php echo $sales_order_item['unit_cost']; ?></td>
	                                      <td class="text-right col-md-1"><?php echo $sales_order_item['total_cost']; ?></td>
	                                  </tr>
	                              <?php $a++; } } ?>

	                              <tr>
	                                  <td colspan="3" class="font-w700 text-uppercase text-right">Grand Total</td>
	                                  <td class="font-w700 text-right">Php <?php echo $sales_order['grand_total']; ?></td>
	                              </tr>
	                              <?php if ($sales_order['total_discount'] != "0.00") { ?>
	                                  <tr>
	                                      <td colspan="3" class="font-w700 text-uppercase text-right">Total Discount</td>
	                                      <td class="font-w700 text-right">Php <?php echo $sales_order['total_discount']; ?></td>
	                                  </tr>
	                              <?php } ?>
	                              <tr class="active">
	                                  <td colspan="3" class="font-w600 text-right">Amount Paid</td>
	                                  <td class="text-right">Php <?php echo $sales_order['amount_paid']; ?></td>
	                              </tr>
	                          </tbody>
	                      </table>
	                  	</div>
		    		</td>
		    	</tr>
		    </table>
	    <?php endif ?>
  	</div>
</body>
</html>