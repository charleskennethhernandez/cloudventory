<?php if (!empty($sales_orders)) { ?>
    <?php echo form_open( 'sales_orders/delete_selected', array( 'id' => 'delete_selected', 'data-module' => 'sales_orders' ) ); ?>
    <?php   
        if( $sort_order == 'ASC' ) {        
            $sort_order = 'DESC';           
        } else {    
            $sort_order = 'ASC';    
        }
    ?>
  
    <table class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <td class="text-center col-md-"> <input type="checkbox" class="checkall" /> </td>
                <th class="text-center col-md-">#</th>
                <th class="hidden-xs col-md-2"><a href="#" onclick="event.preventDefault(); $('#sales_order_list_table').fadeOut(); $('#sales_order_list_table').load('<?php echo base_url('sales_orders/form_ajax/'.$code.'/customers.firstname/'.$sort_order); ?>').fadeIn();">Customer Name</a></th>
                <th class="hidden-xs col-md-1"><a href="#" onclick="event.preventDefault(); $('#sales_order_list_table').fadeOut(); $('#sales_order_list_table').load('<?php echo base_url('sales_orders/form_ajax/'.$code.'/sales_orders.grand_total/'.$sort_order); ?>').fadeIn();">Grand Total</a></th>
                <th class="hidden-xs col-md-1"><a href="#" onclick="event.preventDefault(); $('#sales_order_list_table').fadeOut(); $('#sales_order_list_table').load('<?php echo base_url('sales_orders/form_ajax/'.$code.'/sales_orders.amount_paid/'.$sort_order); ?>').fadeIn();">Amount Paid</a></th>
                <th class="hidden-xs col-md-1"><a href="#" onclick="event.preventDefault(); $('#sales_order_list_table').fadeOut(); $('#sales_order_list_table').load('<?php echo base_url('sales_orders/form_ajax/'.$code.'/sales_orders.status/'.$sort_order); ?>').fadeIn();">Status</a></th>
                <th class="hidden-xs col-md-2"><a href="#" onclick="event.preventDefault(); $('#sales_order_list_table').fadeOut(); $('#sales_order_list_table').load('<?php echo base_url('sales_orders/form_ajax/'.$code.'/sales_orders.date_created/'.$sort_order); ?>').fadeIn();">Date Created</a></th>
                <th class="hidden-xs col-md-1"><a href="#" onclick="event.preventDefault(); $('#sales_order_list_table').fadeOut(); $('#sales_order_list_table').load('<?php echo base_url('sales_orders/form_ajax/'.$code.'/sales_orders.date_updated/'.$sort_order); ?>').fadeIn();">Date Updated</a></th>
                <th class="hidden-xs col-md-2"><a href="#" onclick="event.preventDefault(); $('#sales_order_list_table').fadeOut(); $('#sales_order_list_table').load('<?php echo base_url('sales_orders/form_ajax/'.$code.'/users.firstname/'.$sort_order); ?>').fadeIn();">Created By</a></th>
                <th class="text-center col-md-2">Actions</th>
            </tr>
        </thead>
        <tbody>
        	
                <?php foreach($sales_orders as $_row):?>
                    <tr>
                        <td  class="text-center"> <input type="checkbox" name="sales_orders[]"  value="<?php echo $_row['id']; ?>" /> </td>
                        <td class="text-center"><?php echo $_row['id']?></td>
                        <td class="hidden-xs"><?php echo $_row['firstname'].' '.$_row['middlename'].' '.$_row['lastname'] ?></td>
                        <td class="hidden-xs"><?php echo $_row['grand_total'] ?></td>
                        <td class="hidden-xs"><?php echo $_row['amount_paid'] ?></td>
                        <td class="hidden-xs">
                            <?php echo $_row['status_name'] ?>
                        </td>
                        <td class="hidden-xs"><?php echo date_time_format($_row['date_created']) ?></td>
                        <td class="hidden-xs"><?php echo date_time_format($_row['date_updated']) ?></td>
                        <td class="hidden-xs"><?php echo $_row['user_firstname'].' '.$_row['user_middlename'].' '.$_row['user_lastname'] ?></td>
                        <td class="text-center">
                            <div class="btn-group">
                                <?php if (user_permission($account['user_group_id'],'sales_orders','edit')): ?>
                                    <a data-module="sales_orders" btneditsalesorder data-id="<?php echo $_row['id']?>" class="btn btn-primary" type="button" data-toggle="tooltip" title="Edit Item">
                                    <i class="fa fa-pencil"></i>
                                    </a>
                                 <?php endif; ?>
                                <?php if (user_permission($account['user_group_id'],'sales_orders','view')): ?>
                                    <a data-module="sales_orders" btnviewsalesorder data-id="<?php echo $_row['id']?>" class="btn btn-warning" type="button" data-toggle="tooltip" title="View Item">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                <?php endif; ?>

                                <a target="_BLANK" href="<?php echo base_url(); ?>invoices/index/<?php echo $_row['id'];?>" data-id="<?php echo $_row['id']?>" class="btn btn-success" type="button" data-toggle="tooltip" title="Print Item">
                                    <i class="fa fa-print"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                <?php endforeach;?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="10">
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <?php if (user_permission($account['user_group_id'],'sales_orders','delete')): ?> 
                            <button type="submit" class="btn btn-danger btn-sm delete-selected-btn"> <i class="glyphicon glyphicon-trash"></i>Delete Selected</button>
                        <?php endif; ?>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 text-right">
                        <?php echo ( isset( $sales_order_pagination ) ? $sales_order_pagination : '' ); ?>
                    </div>
                </td>
            </tr>
        </tfoot>
    </table>
    <?php echo form_close(); ?>
<?php } else { ?>
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h3 class="font-w300 push-15">Warning</h3>
        <p>There are no sales orders right now!</p>
    </div>
<?php } ?>