    <div class="block">
        <div class="block-header bg-gray-lighter">
            <h3 class="block-title">Products</h3>
        </div>
        
        <div class="block-content">
            <div class="table-responsive">
                <table class="table table-borderless table-striped table-vcenter" id="tbl_sales_cart">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 100px;">ID</th>
                            <th style="width: 25%;">Product Name</th>
                            <th class="text-center" style="width: 10%;">Stock</th>
                            <th class="text-center" style="width: 10%;">Qty</th>
                            <th class="text-center"  style="width: 15%;">Unit Cost</th>
                            <th class="text-center" style="width: 15%;">Discount</th>
                            <th class="text-center"  style="width: 15%;">Total Cost</th>
                            <th class="text-center"  style="width: 3%;">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $overall_total_cost = 0; ?>
                        <?php //foreach ($inventories as $key => $inventory) { ?>
                            <tr class="sales_item" id="sales_item_id_<?php echo $inventory['id']; ?>">
                                <td class="text-center">
                                    <input type="hidden" name="inventory_id[<?php echo $inventory['id']; ?>]" class="form-control" value="<?php echo $inventory['id']; ?>">
                                    <a href="">
                                        <strong>PID.<?php echo $inventory['id']; ?></strong>
                                    </a>
                                </td>
                                <td>
                                    <a href="">
                                        <?php echo $inventory['name']; ?> (<?php echo get_value_field($inventory['id'],'brands','name'); ?>)
                                        <br> <?php echo $inventory['description']; ?> 
                                    </a>
                                </td>
                                <td class="text-center">
                                    <?php echo $inventory['quantity']; ?>
                                    <input type="hidden" name="inventory_quantity[<?php echo $inventory['id']; ?>]" class="inventory_quantity form-control " value="<?php echo $inventory['quantity']; ?>">
                                </td>
                                <td class="text-center">
                                    <div class="form-material">
                                        <input type="text" name="quantity[<?php echo $inventory['id']; ?>]" class="to_reload sales_item_quantity form-control" value="<?php echo $quantity; ?>">
                                    </div>
                                </td>
                                <td class="text-right form-material">
                                    <div class="form-material">
                                        <?php if ($customer_type) {
                                            $price = $inventory['dealer_price'];
                                        } else {
                                            $price = $inventory['marked_price'];
                                        } ?>
                                        <input type="text" name="unit_cost[<?php echo $inventory['id']; ?>]" class="to_reload sales_item_unit_cost form-control" value="<?php echo $price; ?>">
                                    </div>
                                </td>
                                <td class="text-right form-material">
                                    <div class="form-material">
                                        <input type="text" name="discount[<?php echo $inventory['id']; ?>]" class="to_reload sales_item_discount form-control" value="0.00">
                                    </div>
                                </td>
                                <td class="text-right form-material">
                                    <?php $total_cost = $price * $quantity;  ?>
                                    <div class="form-material"> 
                                        <input type="text" readonly name="total_cost[<?php echo $inventory['id']; ?>]" class="sales_item_total_cost form-control" value="<?php echo $total_cost;?>">
                                    </div>
                                </td>
                                <td class="text-right form-material">
                                    <div class="btn-group">
                                        <button data-id="<?php echo $inventory['sales_order_item_id'] ?>" btnDeletesalesitem class="btn btn-sm btn-default" type="button" data-toggle="tooltip" title="Remove Item">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            <?php $overall_total_cost = $total_cost + $overall_total_cost; ?>
                        <?php //} ?>
                        <tr> 
                            <!-- <td colspan="4" class="text-right"><strong>Total Discount:</strong></td> -->
                            <td colspan="5">&nbsp;</td>
                            <td class="text-right" id="overall_total_discount">
                                <strong>Total Discount:</strong>
                                <div class="form-material"> 
                                    <input type="text" name="overall_total_discount" class="form-control" value="0.00">
                                </div>
                            </td>
                            <!-- <td class="text-right"><strong>Total Price:</strong></td> -->
                            <td class="text-right" id="grand_total">
                                <strong>Total Price:</strong>
                                <div class="form-material"> 
                                    <input type="text" readonly name="overall_total_cost" class="form-control overall_total_cost" value="<?php echo $overall_total_cost;?>">
                                </div>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            
                        </tr>
                        <tr class="success">
                            <td colspan="6" class="text-right"><strong>Total Amount to Pay:</strong></td>
                            <td class="text-right" id="amount_paid">
                                <div class="form-material"> 
                                    <input type="text" name="amount_paid" class="form-control amount_paid" value="<?php echo @$total_cost;?>">
                                </div>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr class="danger">
                            <td colspan="6" class="text-right text-uppercase"><strong>Total Due:</strong></td>
                            <td class="text-right">
                                  <div class="form-material"> 
                                    <input type="text" name="total_due" class="form-control total_due" value="0.00">
                                </div>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
            <button class="btn btn-sm btn-primary pull-right" type="submit">Submit</button>
        </div>
    </div>

