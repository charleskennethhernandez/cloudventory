<?php echo asset_js('validation/sales_order_validation.js', true);?>
<div class="col-md-12">
    <!--Labels -->
    <div class="block">
        <div class="block-header">
            <ul class="block-options">
                <li>
                    <button type="button" BtnCloseSalesOrder><i class="si si-close"></i></button>
                </li>
            </ul>
            <h3 class="block-title">Add New Record</h3>
        </div>
        <div class="block-content block-content-narrow">
            <form id="sales_order_form" class="form-horizontal push-10-t" name="frmsales_order" method="post" onsubmit="return false;">
                <div class="row">
                    <?php if (empty($view)) { ?>
                    <!-- <div class="col-md-6"> -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="block-content">
                                    <div class="form-material">
                                        <input class="form-control" type="hidden" id="id" name="id" value=<?php echo @$sales_order['id']; ?>>
                                        <div>
                                            <label for="material-select">Customer Type</label>
                                            <select class="form-control" name="customer_type">
                                                <option value="0">Walk-In</option>
                                                <option value="1">Dealer</option>
                                            </select>
                                        </div>

                                        
                                        <div>
                                            <label for="material-select">Is it an old customer?</label>
                                            <select class="form-control" name="is_customer_exists">
                                                <option value="1">Yes</option>
                                                <option value="0">No</option>
                                            </select>
                                        </div>

                                        <br>

                                        <div id="is_exists">
                                            <label for="material-select">Please Select Customer</label>
                                            <select class="form-control" name="customer_id" id="customer_id">
                                                <?php if ($customers): ?>
                                                     <?php foreach ($customers as $key => $customer) { ?>
                                                        <option <?php if ((isset($sales_order['customer_id'])) &&( $customer['id'] == $sales_order['customer_id'])) { echo "selected"; }; ?> value="<?php echo $customer['id']; ?>">
                                                            <?php echo $customer['firstname'].' '.$customer['lastname']; ?>
                                                        </option>
                                                    <?php } ?>
                                                <?php endif ?>
                                               
                                            </select>   
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="block-content hide" id="is_not_exists">
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <div class="form-material form-material-success">
                                                <input class="form-control" type="text" id="firstname" name="firstname">
                                                <label for="firstname">First Name</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-material form-material-success ">
                                                <input class="form-control" type="text" id="middlename" name="middlename">
                                                <label for="middlename">Middle Name</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-material form-material-success ">
                                                <input class="form-control" type="text" id="lastname" name="lastname">
                                                <label for="lastname">Last Name</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="block-content">
                        <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="dataTables_length" id="DataTables_Table_0_length">
                                        <label>
                                            <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" class="form-control">
                                                <option value="5">5</option>
                                                <option value="10" selected>10</option>
                                                <option value="15">15</option>
                                                <option value="20">20</option></select>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div id="DataTables_Table_0_filter" class="dataTables_filter">

                                        <label>Search:<input type="search" class="form-control search_field" placeholder=""  data-form="1" data-module="inventories"></label>
                                        <button style="margin-top: 11px;" href="#cart_table" class="btn btn-danger push-5-r push-10" type="button"><i class="fa fa-down"></i> View Cart</button>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="list_table_container">
                          <?php echo $this->load->view('inventories/inventory_list_table'); ?>
                        </div>
                        </div>
                    <!-- </div> -->
                    <?php } ?>
                    <!-- <div class="col-md-6"> -->
                        <div id="cart_table">
                            <?php if ((isset($sales_order_items)) && (!empty($sales_order_items))): ?>
                                <?php echo $this->load->view('sales_orders/sales_order_cart'); ?>
                            <?php endif ?>
                            <!-- Sales Order Items -->
                        </div>
                    <!-- </div> -->

                </div>
            </form>
        </div>
    </div>
    <!-- END  Labels -->
</div>