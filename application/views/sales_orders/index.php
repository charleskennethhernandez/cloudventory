<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="<?php echo site_url('assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css')?>">
<?php echo asset_js('custom/sales_orders.js', true);?>

<!-- Page Header -->
<div class="content bg-gray-lighter">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                <?php echo $title; ?> <small><?php echo $description; ?></small>
            </h1>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Home</li>
                <li><a class="link-effect" href=""><?php echo $title; ?></a></li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<!-- Page Content -->
<div class="content">
    <div class="row" id="sales_order_list_container">
		<div class="col-lg-12">
	        <!-- Hover Table -->
	        <div class="block">
	            <div class="block-header">
	                <div class="block-options">
                        <?php if (user_permission($account['user_group_id'],'sales_orders','add')): ?>
                            <button class="btn btn-lg btn-success push-5-r push-10" BtnAddSalesOrder type="button"><i class="fa fa-plus"></i> Add Item</button>
                        <?php endif; ?>
	                </div>
	                <h3 class="block-title">Sales Order List Table</h3>
	            </div>
                
	            <div class="block-content">
                    <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="dataTables_length" id="DataTables_Table_0_length">
                                    <label>
                                        <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" class="form-control">
                                            <option value="5">5</option>
                                            <option value="10" selected>10</option>
                                            <option value="15">15</option>
                                            <option value="20">20</option></select>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div id="DataTables_Table_0_filter" class="dataTables_filter">
                                    <label>Search:
                                        <input type="search" class="form-control search_field" placeholder="" data-module="sales_orders"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="sales_order_list_table_container">
	            	  <?php echo $this->load->view('sales_orders/sales_order_list_table'); ?>
                    </div>
	            </div>
	        </div>
	        <!-- END Hover Table -->
	    </div>
	</div>
	<div class="row hide" id="sales_order_form_container">
        <?php //echo $this->load->view('sales_orders/sales_order_form'); ?>
	</div>
</div>

