<?php $overall_total_cost = 0; ?>
<?php //foreach ($inventories as $key => $inventory) { ?>
    <tr class="sales_item" data-id="" id="sales_item_id_<?php echo $inventory['inventory_id']; ?>">
        <td class="text-center">
            <input type="hidden" name="inventory_id[<?php echo $inventory['inventory_id']; ?>]" class="form-control" value="<?php echo $inventory['inventory_id']; ?>">
            <a href="">
                <strong>PID.<?php echo $inventory['inventory_id']; ?></strong>
            </a>
        </td>
        <td>
            <a href="">
                <?php echo $inventory['name']; ?> (<?php echo get_value_field($inventory['inventory_id'],'brands','name'); ?>)
                <br> <?php echo $inventory['description']; ?> 
            </a>
        </td>
        <td class="text-center">
            <?php echo $inventory['quantity']; ?>
            <input type="hidden" name="inventory_quantity[<?php echo $inventory['inventory_id']; ?>]" class="form-control inventory_quantity" value="<?php echo $inventory['quantity']; ?>">
        </td>
        <td class="text-center">
            <div class="form-material">
                <input type="text" name="quantity[<?php echo $inventory['inventory_id']; ?>]" class="to_reload sales_item_quantity form-control" value="<?php echo $quantity; ?>">
            </div>
        </td>
        <td class="text-right form-material">
            <div class="form-material">
                <?php if ($customer_type) {
                    $price = $inventory['dealer_price'];
                } else {
                    $price = $inventory['marked_price'];
                } ?>
                
                <input type="text" name="unit_cost[<?php echo $inventory['inventory_id']; ?>]" class="to_reload sales_item_unit_cost form-control" value="<?php echo $price; ?>">
            </div>
        </td>
        <td class="text-right form-material">
            <div class="form-material">
                <input type="text" name="discount[<?php echo $inventory['inventory_id']; ?>]" class="to_reload sales_item_discount form-control" value="0.00">
            </div>
        </td>
        <td class="text-right form-material">
            <?php $total_cost = $price * $quantity;  ?>
            <div class="form-material"> 
                <input type="text" readonly name="total_cost[<?php echo $inventory['inventory_id']; ?>]" class="form-control sales_item_total_cost" value="<?php echo $total_cost;?>">
            </div>
        </td>
        <td class="text-right form-material">
            <div class="btn-group">
                <button data-id="<?php echo $inventory['inventory_id']?>" class="btn btn-sm btn-default" btnDeletesalesitem type="button" data-toggle="tooltip" title="Remove Item">
                    <i class="fa fa-times"></i>
                </button>
            </div>
        </td>
    </tr>
    <?php $overall_total_cost = $total_cost + $overall_total_cost; ?>
<?php //} ?>