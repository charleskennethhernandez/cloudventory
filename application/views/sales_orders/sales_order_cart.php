<?php 
    if (!empty($view)) {
        $readonly = "readonly";
    } else {
        $readonly = "";
    }
?>
<div class="block">
    <div class="block-header bg-gray-lighter">
        <h3 class="block-title">Products</h3>
    </div>
    
    <div class="block-content">
        <div class="table-responsive">
            <table class="table table-borderless table-striped table-vcenter" id="tbl_sales_cart">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 100px;">ID</th>
                        <th style="width: 25%;">Product Name</th>
                        <th class="text-center" style="width: 10%;">Stock</th>
                        <th class="text-center" style="width: 10%;">Qty</th>
                        <th class="text-center"  style="width: 15%;">Unit Cost</th>
                        <th class="text-center" style="width: 15%;">Discount</th>
                        <th class="text-center"  style="width: 15%;">Total Cost</th>
                        <th class="text-center"  style="width: 3%;">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($sales_order_items as $key => $inventory) { ?>
                        <tr class="sales_item" id="sales_item_id_<?php echo $inventory['inventory_id']; ?>">
                            <td class="text-center">
                                <input <?php echo $readonly; ?> type="hidden" name="sales_order_item_id[<?php echo $inventory['inventory_id']; ?>]" class="form-control" value="<?php echo @$inventory['sales_order_item_id']; ?>">
                                <input <?php echo $readonly; ?> type="hidden" name="inventory_id[<?php echo $inventory['inventory_id']; ?>]" class="form-control" value="<?php echo $inventory['inventory_id']; ?>">
                                <a href="">
                                    <strong>PID.<?php echo $inventory['inventory_id']; ?></strong>
                                </a>
                            </td>
                            <td>
                                <a href="">
                                    <?php echo $inventory['name']; ?> (<?php echo get_value_field($inventory['inventory_id'],'brands','name'); ?>)
                                    <br> <?php echo $inventory['description']; ?> 
                                </a>
                            </td>
                            <td class="text-center">
                                <?php echo $inventory['inventory_quantity']; ?>
                                <input <?php echo $readonly; ?> type="hidden" name="inventory_quantity[<?php echo $inventory['inventory_id']; ?>]" class="inventory_quantity form-control " value="<?php echo $inventory['inventory_quantity'] + @$inventory['quantity']; ?>">
                            </td>
                            <td class="text-center">
                                <div class="form-material">
                                    <input <?php echo $readonly; ?> type="text" name="quantity[<?php echo $inventory['inventory_id']; ?>]" class="to_reload sales_item_quantity form-control" value="<?php echo $inventory['quantity']; ?>">
                                </div>
                            </td>
                            <td class="text-right form-material">
                                <div class="form-material">
                                    <input <?php echo $readonly; ?> type="text" name="unit_cost[<?php echo $inventory['inventory_id']; ?>]" class="to_reload sales_item_unit_cost form-control" value="<?php echo $inventory['unit_cost']; ?>">
                                </div>
                            </td>
                            <td class="text-right form-material">
                                <div class="form-material">
                                    <input <?php echo $readonly; ?> type="text" name="discount[<?php echo $inventory['inventory_id']; ?>]" class="to_reload sales_item_discount form-control" value="0.00">
                                </div>
                            </td>
                            <td class="text-right form-material">
                                <div class="form-material"> 
                                    <input <?php echo $readonly; ?> type="text" readonly name="total_cost[<?php echo $inventory['inventory_id']; ?>]" class="sales_item_total_cost form-control" value="<?php echo $inventory['total_cost'];?>">
                                </div>
                            </td>
                            <td class="text-right form-material">
                                <?php if (empty($view)){ ?>

                                <div class="btn-group">
                                    <button data-id="<?php echo $inventory['inventory_id'] ?>" btnDeletesalesitem class="btn btn-warning" type="button" data-toggle="tooltip" title="Remove Item">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </div>

                                <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                    <tr> 
                        <!-- <td colspan="4" class="text-right"><strong>Total Discount:</strong></td> -->
                        <td colspan="5">&nbsp;</td>
                        <td class="text-right" id="overall_total_discount">
                            <strong>Total Discount:</strong>
                            <div class="form-material"> 
                                <input <?php echo $readonly; ?> type="text" name="overall_total_discount" class="form-control" value="<?php echo $sales_order['total_discount']; ?>">
                            </div>
                        </td>
                        <!-- <td class="text-right"><strong>Total Price:</strong></td> -->
                        <td class="text-right" id="grand_total">
                            <strong>Total Price:</strong>
                            <div class="form-material"> 
                                <input <?php echo $readonly; ?> type="text" readonly name="overall_total_cost" class="form-control overall_total_cost" value="<?php echo $sales_order['grand_total']; ?>">
                            </div>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        
                    </tr>
                    <tr class="success">
                        <td colspan="6" class="text-right"><strong>Total Amount to Pay:</strong></td>
                        <td class="text-right" id="amount_paid">
                            <div class="form-material"> 
                                <input <?php echo $readonly; ?> type="text" name="amount_paid" class="form-control amount_paid" value="<?php echo $sales_order['amount_paid']; ?>">
                            </div>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr class="danger">
                        <td colspan="6" class="text-right text-uppercase"><strong>Total Due:</strong></td>
                        <td class="text-right">
                              <div class="form-material"> 
                                <input <?php echo $readonly; ?> type="text" name="total_due" class="form-control total_due" value="<?php echo $sales_order['total_due']; ?>">
                            </div>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php if (empty($view)): ?>
    <div class="form-group">
        <div class="col-sm-12">
            <button class="btn btn-sm btn-primary pull-right" type="submit">Submit</button>
        </div>
    </div>
<?php endif ?>


