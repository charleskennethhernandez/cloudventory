<?php if (!empty($sales_order_items)) { ?>
    <?php   
        if( $sort_order == 'ASC' ) {        
            $sort_order = 'DESC';           
        } else {    
            $sort_order = 'ASC';    
        }
    ?>
    <table class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th class="text-center hidden-xs col-md-"><a href="#" onclick="event.preventDefault(); $('#list_table_container').fadeOut(); $('#list_table_container').load('<?php echo base_url('sales_order_items/form_ajax/'.$code.'/sales_order_items.sales_order_id/'.$sort_order); ?>').fadeIn();">Sales Order ID</a></th>
                <th class="text-center hidden-xs col-md-2"><a href="#" onclick="event.preventDefault(); $('#list_table_container').fadeOut(); $('#list_table_container').load('<?php echo base_url('sales_order_items/form_ajax/'.$code.'/sales_orders.customer_id/'.$sort_order); ?>').fadeIn();">Customer Name</a></th>
                <th class="text-center hidden-xs col-md-2"><a href="#" onclick="event.preventDefault(); $('#list_table_container').fadeOut(); $('#list_table_container').load('<?php echo base_url('sales_order_items/form_ajax/'.$code.'/sales_orders.customer_id/'.$sort_order); ?>').fadeIn();">Inventory Name</a></th>
                <th class="text-center hidden-xs col-md-1"><a href="#" onclick="event.preventDefault(); $('#list_table_container').fadeOut(); $('#list_table_container').load('<?php echo base_url('sales_order_items/form_ajax/'.$code.'/sales_order_items.description/'.$sort_order); ?>').fadeIn();">Unit Cost</a></th>
                <th class="text-center hidden-xs col-md-1"><a href="#" onclick="event.preventDefault(); $('#list_table_container').fadeOut(); $('#list_table_container').load('<?php echo base_url('sales_order_items/form_ajax/'.$code.'/sales_order_items.quantity/'.$sort_order); ?>').fadeIn();">Quantity</a></th>
                <th class="text-center hidden-xs col-md-1"><a href="#" onclick="event.preventDefault(); $('#list_table_container').fadeOut(); $('#list_table_container').load('<?php echo base_url('sales_order_items/form_ajax/'.$code.'/sales_order_items.discount/'.$sort_order); ?>').fadeIn();">Discount</a></th>
                <th class="text-center hidden-xs col-md-1"><a href="#" onclick="event.preventDefault(); $('#list_table_container').fadeOut(); $('#list_table_container').load('<?php echo base_url('sales_order_items/form_ajax/'.$code.'/sales_order_items.total_cost/'.$sort_order); ?>').fadeIn();">Total Cost</a></th>
                <th class="text-center hidden-xs col-md-2"><a href="#" onclick="event.preventDefault(); $('#list_table_container').fadeOut(); $('#list_table_container').load('<?php echo base_url('sales_order_items/form_ajax/'.$code.'/sales_orders.created_by/'.$sort_order); ?>').fadeIn();">Created By</a></th>
                <th class="text-center hidden-xs col-md-2"><a href="#" onclick="event.preventDefault(); $('#list_table_container').fadeOut(); $('#list_table_container').load('<?php echo base_url('sales_order_items/form_ajax/'.$code.'/sales_order_items.date_created/'.$sort_order); ?>').fadeIn();">Date Created</a></th>
            </tr>
        </thead>
        <tbody>
        	
                <?php foreach($sales_order_items as $_row):?>
                    <tr>
                        <td class=" text-center hidden-xs"><?php echo $_row['sales_order_id'] ?></td>
                        <td class="text-center hidden-xs"><?php echo $_row['name']; ?></td>
                        <td class="text-center hidden-xs"><?php echo $_row['firstname'].' '.$_row['lastname']; ?></td>
                        <td class="text-center hidden-xs"><?php echo format_currency($_row['unit_cost']); ?></td>
                        <td class="text-center hidden-xs"><?php echo $_row['quantity']; ?></td>
                        <td class="text-center hidden-xs"><?php echo format_currency($_row['discount']); ?></td>
                        <td class="text-center hidden-xs"><?php echo format_currency($_row['total_cost']); ?></td>
                        <td class="text-center hidden-xs"><?php echo $_row['user_firstname'].' '.$_row['user_lastname']; ?></td>
                        <td class="text-center hidden-xs"><?php echo date_time_format($_row['date_created']); ?></td>
                    </tr>
                <?php endforeach;?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="9">
                    <div class="col-sm-12 col-md-12 col-lg-12 text-right">
                        <?php echo ( isset( $pagination ) ? $pagination : '' ); ?>
                    </div>
                </td>
            </tr>
        </tfoot>
    </table>
<?php } else { ?>
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h3 class="font-w300 push-15">Warning</h3>
        <p>There are no sales order items right now!</p>
    </div>
<?php } ?>