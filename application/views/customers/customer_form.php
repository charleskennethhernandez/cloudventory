<?php echo asset_js('validation/customer_validation.js', true);?>
<div class="col-md-12">
    <!--Labels -->
    <div class="block">
        <div class="block-header">
            <ul class="block-options">
                <li>
                    <button type="button" btnClose><i class="si si-close"></i></button>
                </li>
            </ul>
            <h3 class="block-title">Add New Record</h3>
        </div>
        <div class="block-content block-content-narrow">
            <form id="customer_form" class="form-horizontal push-10-t" name="frmcustomer" method="post" onsubmit="return false;">
                
                <div class="form-group">
                    <div class="col-sm-4">
                        <div class="form-material form-material-success ">
                            <input class="form-control" type="text" id="firstname" name="firstname" value="<?php echo @$firstname; ?>">
                            <input class="form-control" type="hidden" id="id" name="id" value="<?php echo @$id; ?>">
                            <label for="firstname">First Name</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-material form-material-success ">
                            <input class="form-control" type="text" id="middlename" name="middlename" value="<?php echo @$middlename; ?>">
                            <label for="middlename">Middle Name</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-material form-material-success ">
                            <input class="form-control" type="text" id="lastname" name="lastname" value="<?php echo @$lastname; ?>">
                            <label for="lastname">Last Name</label>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-sm-4">
                        <div class="form-material form-material-success ">
                            <input class="form-control" type="text" id="phone" name="phone" value="<?php echo @$phone; ?>">
                            <label for="phone">Phone Number</label>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-material form-material-success ">
                            <input class="form-control" type="text" id="email" name="email" value="<?php echo @$email; ?>">
                            <label for="email">Email</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-material form-material-success">
                            <input class="js-datepicker form-control" type="text" value="<?php echo @$date_of_birth; ?>" id="date_of_birth" name="date_of_birth" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
                            <label for="date_of_birth">Date of Birth</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-4">
                        <div class="form-material form-material-success ">
                            <textarea class="form-control" type="text" id="address" name="address">
                                <?php echo @$address; ?>
                            </textarea>
                            <label for="phone">Address</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9">
                        <button class="btn btn-sm btn-primary" type="submit">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END  Labels -->
</div>