<?php if (!empty($customers)) { ?>
    <?php echo form_open( 'customers/delete_selected', array( 'id' => 'delete_selected', 'data-module' => 'customers' ) ); ?>
    <?php   
        if( $sort_order == 'ASC' ) {        
            $sort_order = 'DESC';           
        } else {    
            $sort_order = 'ASC';    
        }
    ?>
    <table class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <td class="text-center col-md-"> <input type="checkbox" class="checkall" /> </td>
                <th class="text-center col-md-">#</th>
                <th class="hidden-xs col-md-3"><a href="#" onclick="event.preventDefault(); $('#list_table_container').fadeOut(); $('#list_table_container').load('<?php echo base_url('customers/form_ajax/'.$code.'/customers.firstname/'.$sort_order); ?>').fadeIn();">Name</a></th>
                <th class="hidden-xs col-md-2"><a href="#" onclick="event.preventDefault(); $('#list_table_container').fadeOut(); $('#list_table_container').load('<?php echo base_url('customers/form_ajax/'.$code.'/customers.phone/'.$sort_order); ?>').fadeIn();">Phone</a></th>
                <th class="hidden-xs col-md-3"><a href="#" onclick="event.preventDefault(); $('#list_table_container').fadeOut(); $('#list_table_container').load('<?php echo base_url('customers/form_ajax/'.$code.'/customers.address/'.$sort_order); ?>').fadeIn();">Address</a></th>
                <th class="hidden-xs col-md-2"><a href="#" onclick="event.preventDefault(); $('#list_table_container').fadeOut(); $('#list_table_container').load('<?php echo base_url('customers/form_ajax/'.$code.'/customers.date_created/'.$sort_order); ?>').fadeIn();">Date Created</a></th>
                <th class="hidden-xs col-md-2"><a href="#" onclick="event.preventDefault(); $('#list_table_container').fadeOut(); $('#list_table_container').load('<?php echo base_url('customers/form_ajax/'.$code.'/customers.date_updated/'.$sort_order); ?>').fadeIn();">Date Updated</a></th>
                <th class="text-center">Actions</th>
            </tr>
        </thead>
        <tbody>
        	
                <?php foreach($customers as $_row):?>
                    <tr>
                        <td  class="text-center"> <input type="checkbox" name="customers[]"  value="<?php echo $_row['id']; ?>" /> </td>
                        <td class="text-center"><?php echo $_row['id']?></td>
                        <td class="hidden-xs"><?php echo $_row['firstname'].' '.$_row['middlename'].' '.$_row['lastname'] ?></td>
                        <td class="hidden-xs"><?php echo $_row['phone'] ?></td>
                        <td class="hidden-xs"><?php echo $_row['address'] ?></td>
                        <td class="hidden-xs"><?php echo date_time_format($_row['date_created']) ?></td>
                        <td class="hidden-xs"><?php echo date_time_format($_row['date_updated']) ?></td>
                        <td class="text-center">
                            <div class="btn-group">
                                 <?php if (user_permission($account['user_group_id'],'customers','edit')): ?>
                                      <button data-id="<?php echo $_row['id']?>" data-module="customers" btnedit class="btn btn-primary" type="button" data-toggle="tooltip" title="Edit Item">
                                        <i class="fa fa-pencil"></i>
                                    </button>
                                <?php endif ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach;?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="8">
                    <div class="col-sm-12 col-md-6 col-lg-6">
                         <?php if (user_permission($account['user_group_id'],'customers','delete')): ?>
                                    
                        <button type="submit" class="btn btn-danger btn-sm delete-selected-btn"> <i class="glyphicon glyphicon-trash"></i>Delete Selected</button>

                                <?php endif ?>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 text-right">
                        <?php echo ( isset( $pagination ) ? $pagination : '' ); ?>
                    </div>
                </td>
            </tr>
        </tfoot>
    </table>
    <?php echo form_close(); ?>
<?php } else { ?>
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h3 class="font-w300 push-15">Warning</h3>
        <p>There are no customers right now!</p>
    </div>
<?php } ?>