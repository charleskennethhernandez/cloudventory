<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Base_Model extends CI_Model {	
	public function __construct(){		
		parent::__construct();		
	}	
}
class Cloudventory_Model extends Base_Model {
	public $table;
	public $secondary_table;	
	public $account;	
	// public $account_information;	
	public $limit = 10;
	public $sort_by = 'id';
	public $sort_order = 'DESC';
	public function __construct() {
		parent::__construct();
		$this->load->library('upload');
		//RESET SYSTEM CLOCK
		date_default_timezone_set("Asia/Singapore");		
		if( $this->session->userdata( 'account' ) ) {
			//LOGGED USER MAKE IT GLOBAL
			$this->account = $this->session->userdata( 'account' );
			//LOGGED USER INFORMATION MAKE IT GLOBAL
			//$this->account_information = $this->session->userdata( 'account_information' );
		} else {
			//REDIRECT TO LOGIN PAGE
			//redirect( 'security/login' );

		}
	}
	/* THIS SERVE AS UNIVERSAL CRUD */
	public function dynamic_save($params = array()){
		
		if(!empty($params['id'])){
			$this->db->where('id', $id);
			$this->db->update($params['table'], $params['data']);
			$id = $params['id'];
		} else {
			$this->db->insert($params['table'], $params['data']);
			$id = $this->db->insert_id();
		}
		
		return $id;
	}
	
	public function dynamic_get($params){
		if(!empty($params['fields'])) {
			$this->db->select($params['fields']);
		}
		
		$this->db->from($params['table']);
		
		if(!empty($params['where'])) {
			foreach($params['where'] as $key => $value){
				$this->db->where($key, $value);
			}
		}
		if(!empty($params['order_by'])){
			foreach($params['order_by'] as $key => $value){
				$this->db->order_by($key, $value);
			}
		}
		
		if(!empty($params['joins'])) {
			foreach($params['joins'] as $key => $value){
				$this->db->join($value['table'], $value['condition'], $value['type']);
			}
		}
		if(!empty($params['or_where'])){
			/* foreach($params['or_where'] as $key => $value){
				$this->db->or_where($key, $value);
			} */
			//$this->db->or_where('(`created_by` = 15 AND `user_type_id` = 2)');
		}
		
		if(!empty($params['where_in'])) {
			foreach($params['where_in'] as $key => $value){
				$this->db->where_in($key, $value);
			}
		}
		
		return $this->db->get()->result_array();
	}
	
	public function dynamic_query_get($params){
		
		if(!empty($params['fields'])){
			$this->db->select($params['fields']);
		}
		$this->db->where($params['condition']);
		$this->db->from($params['table']);
		
		if(!empty($params['joins'])) {
			foreach($params['joins'] as $key => $value){
				$this->db->join($value['table'], $value['condition'], $value['type']);
			}
		}
		
		if(!empty($params['limit'])){
			$offset = 0;
			if(!empty($params['offset']))
				$offset = $params['offset'];
			
			$this->db->limit($params['limit'], $offset);
		} else {
		
		}
		
		return $this->db->get()->result_array();
		
	}
	
	public function dynamic_update($params = array()){
		$this->db->where('id', $params['id']);
		$this->db->update($params['table'], $params['data']);
		return $this->db->affected_rows();
	}
	
	public function dynamic_delete($params = array()){
		if(!empty($params['table']) && !empty($params['where'])){
			foreach($params['where'] as $key => $value){
				$this->db->where($key, $value);
			}
			$this->db->delete($params['table']);
			return $this->db->affected_rows();
		}
	
	}
	
	public function dynamic_history($params){
		$details = $params['details'];
		$changes_refering_to = $params['changes_refering_to'];
		unset($params['details']);
		unset($params['changes_refering_to']);
		
		$params['created_date'] = date('Y-m-d');
		$history_params = array(
			'table' => 'history',
			'data' => $params,
		);
		
		$history_id = $this->dynamic_save($history_params);
		
		$history_details_data = array(
			'history_id' => $history_id,
			'details' => $details,
			'changes_refering_to' => $changes_refering_to,
		);
		
		$histor_details_params = array(
			'table' => 'history_details',
			'data' => $history_details_data,
		);
		
		$this->dynamic_save($histor_details_params);
		return $history_id;
	}

	public function save( $data = array() ) {
		$data['date_updated'] = date('Y-m-d h:i:s');
		if( isset( $data['id'] ) && !empty( $data['id'] ) ) {
			if (isset($data['password']) && !empty($data['password'])) {
				$data['password'] = md5($data['password']);
			}
			//UPDATE
			$this->db->where( 'id', $data['id'] );
			$this->db->update( $this->table, $data );
			$id = $data['id'];
			//RECORD HISTORY
			// $this->record_history( $id, $this->table, 'update' );
		} else {
			if (isset($data['password']) && !empty($data['password'])) {
				$data['password'] = md5($data['password']);
			}
			$data['date_created'] = date('Y-m-d h:i:s');
			//SAVE
			$this->db->insert( $this->table, $data );
			$id = $this->db->insert_id();
			//RECORD HISTORY
			// $this->record_history( $id, $this->table, 'save' );
		}
		return $id;
	}



	/*

		THIS SERVE AS UNIVERSAL CRUD FOR EMAIL NOTIFY

	*/

	public function will_notify( $data = array() ) {

		

		if( isset( $data['id'] ) && !empty( $data['id'] ) ) {

		

			//UPDATE

			$this->db->where( 'id', $data['id'] );

			

			$this->db->update( 'cornerstone_developer_email_lists', $data );

			

			$id = $data['id'];

			

			//RECORD HISTORY

			$this->record_history( $id, 'cornerstone_developer_email_lists', 'update' );

		

		} else {

		

			//SAVE

			$this->db->insert( 'cornerstone_developer_email_lists', $data );

			

			$id = $this->db->insert_id();

			

			//RECORD HISTORY

			$this->record_history( $id, 'cornerstone_developer_email_lists', 'save' );



		}

		

		return $id;

	

	}

	

	/*

		WE DONT ACTUALLY DELETE RECORD

		THIS SERVES AS OUR HISTORY OF PREVIOUS TRANSACTIONS

	*/

	public function delete( $id = 0 ) {



		if( $id ) {

		

			$this->db->where( 'id', $id );

			

			$this->db->where( 'is_active', 1 );

			

			$this->db->where( 'is_deleted', 0 );



			$this->db->update( $this->table, array( 'is_deleted' => 1 ) );

			

			//RECORD HISTORY

			// $this->record_history( $id, $this->table, 'delete' );

			

			return $id;

		

		}

		

		return 0;

		

	}

	

	/*

		WE GIVE OPTION TO THE USER

		FOR THEM TO MAKE UP THEIR MIND IF A CERTAIN INFORMATION

		IS STILL NEED OR NOT

	*/

	public function deactivate( $id = 0 ) {

	

		if( $id ) {

		

			$this->db->where( 'id', $id );

			

			$this->db->where( 'is_active', 1 );

			

			$this->db->where( 'is_deleted', 0 );

			

			$this->db->update( $this->table, array( 'is_active' => 0 ) );

			

			//RECORD HISTORY

			$this->record_history( $id, $this->table, 'deactivate' );

			

			return $id;

		

		}

		

		return 0;

	

	}

	

	/*

		BASIC FETCH OF RECORDS

	*/

	public function get_record( $id = 0 ) {

	

		if( $id ) {

		

			$this->db->where( 'id', $id );

			

			$this->db->where( 'is_active', 1 );

			

			$this->db->where( 'is_deleted', 0 );

			

			return $this->db->get( $this->table )->row_array();

		

		}

		

		return array();

	

	}

	

	/*

		BASIC FETCH OF ALL RECORDS

	*/

	public function get_all_record( $id = 0 ) {

	

		$this->db->where( 'is_active', 1 );

			

		$this->db->where( 'is_deleted', 0 );

		

		$this->db->order_by( $this->sort_by, $this->sort_order );



		return $this->db->get( $this->table )->result_array();

	

	}

	

	/*

		BASIC FETCH OF COUNT OF ALL RECORDS

	*/

	public function total_record( $id = 0 ) {

	

		$this->db->where( 'is_active', 1 );

			

		$this->db->where( 'is_deleted', 0 );

	

		return $this->db->count_all_results( $this->table );

	

	}

	

	public function save_images( $id = 0 ) {

	

		$data['id'] = $id;		

		

		$config['allowed_types'] = 'gif|jpg|png';



		$config['max_size']	= '8000';



		$config['max_width']  = '4000';



		$config['max_height']  = '4000';

		

		$config['encrypt_name'] = TRUE;

		



		if( $id && ! empty( $_FILES ) ) {

		

			foreach( $_FILES as $name => $image_infomation ) {

			

				$config['upload_path'] = 'assets/img/' .$name;

				

				$this->upload->initialize( $config );

				

				if ( ! $this->upload->do_upload( $name ) ) {

				

					$error = $this->upload->display_errors();

					

				} else {

				

					$image = $this->upload->data();

					

					$data[$name] = $image['file_name'];

					

					$this->save( $data );

					

				}

			

			}

		

		}

	

	}

	public function save_files( $id = 0 , $type_id) {
		//$data['id'] = $id;
		$config['allowed_types'] = '*';
		$config['max_size']	= '8000';
		$config['encrypt_name'] = TRUE;
		
		if(! empty( $_FILES ) ) {
			foreach( $_FILES as $name => $file_infomation ) {
				$config['upload_path'] = 'assets/files/';
				$this->upload->initialize( $config );
				if ( ! $this->upload->do_upload( $name ) ) {
					$error = $this->upload->display_errors();
				} else {
					$image = $this->upload->data();
					$data['form_file'] = $image['file_name'];
					$data['name'] = @$image['filename'];
					$data['type_id'] = $type_id;
					$data['is_uploaded_by_client'] = '1';
					$data['created_date'] = date('Y-m-d H:s:i');
					return $id = $this->save( $data );
				}
			}

		}

	}

	

	/* ALWAYS RECORD HISTORY */

	public function record_history( $id = 0, $table = '', $action = '' ) {

		

		if( $id && $table && $action ) {

		

			$data['affected_id'] = $id;

			

			$data['affected_table'] = $table;

			

			$data['action'] = $action;



			$data['created_by'] = $this->account['id'];

		

			$this->db->insert( 'cornerstone_history', $data );

		

		} else {

		

			$this->session->set_flashdata( 'error', 'Error recording history! Check your source code!' );

			

			redirect( base_url() );

		

		}

		

	}



}


