<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Base_Controller extends CI_Controller {
	public $account;
	public function __construct(){
		parent::__construct();
		date_default_timezone_set("Asia/Singapore");
		if( $this->session->userdata( 'account' ) ) {
			$this->account = $this->session->userdata( 'account' );
			$this->account_information = $this->session->userdata( 'account_information' );
		} else {
			redirect( 'security/login' );
		}
	}
}

/*
	FRONT END
	FOLDER : THEMES
*/
class Front_Controller extends Base_Controller {
	
	public function __construct() {
		
		parent::__construct();

		$this->load->add_package_path( APPPATH.'themes/'.$this->config->item('theme').'/' );


		
	}
	
	public function view( $view, $vars = array(), $string = false ) {
	
		if( $string ) {
		
			$result	 = $this->load->view( 'template/header', $vars, true );
			
			$result	.= $this->load->view( $view, $vars, true );
			
			$result	.= $this->load->view( 'template/footer', $vars, true );
			
			return $result;
			
		} else {
		
			$this->load->view('template/header', $vars);
			
			$this->load->view($view, $vars);
			
			$this->load->view('template/footer', $vars);
			
		}
		
	}
	
	function partial( $view, $vars = array(), $string = false ) {
	
		if( $string ) {
		
			return $this->load->view( $view, $vars, true );
			
		} else {
		
			$this->load->view( $view, $vars );
			
		}
		
	}
	
}


/*
	BACK OFFICE
	FOLDER : VIEW
*/
class Back_Controller extends Base_Controller {
	public function __construct() {
		parent::__construct();
		$account = $this->account;
		$this->nav_infos = get_all_modules($account['user_group_id']);
		
	}
	
	public function view( $view, $vars = array(), $string = false ) {
		$vars['nav_infos'] = $this->nav_infos;
		$vars['account'] = $this->account;
		
		if( $string ) {
			$result	 = $this->load->view( 'template/header', $vars, true );
			$result	.= $this->load->view( $view, $vars, true );
			$result	.= $this->load->view( 'template/footer', $vars, true );
			return $result;
		} else {
			$this->load->view( 'template/header', $vars );
			$this->load->view( $view, $vars );
			$this->load->view( 'template/footer', $vars );
		}
	}
	
	function partial( $view, $vars = array(), $string = false ) {
		$vars['nav_infos'] = $this->nav_infos;
		$vars['account'] = $this->account;
		if( $string ) {
			return $this->load->view( $view, $vars, true );
		} else {
			$this->load->view( $view, $vars );
		}
	}
	
	/*
		UPDATE DEVELOPER BACK OFFICE IN AJAX
	*/
	public function update_developer( $data = array() ) {
		echo json_encode( $data );
	}
}