<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



/*TITLES*/

$lang['login'] = 'Login';

$lang['registration'] = 'Registration';

$lang['forgot_password'] = 'Forgot Password';


/*LOGIN*/

$lang['username'] = 'Username';

$lang['password'] = 'Password';

$lang['sign_in_with_your_account'] = 'Sign in with your account.';

$lang['sign_in'] = 'Sign in';


/*REGISTRATION*/

$lang['firstname'] = 'Firstname';

$lang['lastname'] = 'Lastname';

$lang['email_address'] = 'Email Address';

$lang['avatar'] = 'Avatar';

$lang['date_of_birth'] = 'Date of birth';

$lang['gender'] = 'Gender';

$lang['confirm_password'] = 'Confirm Password';

$lang['registration_complete'] = 'Registration Complete!';

$lang['fill_up_inputs_correctly'] = 'Fill up inputs correctly!';

/*BUTTON*/

$lang['register'] = 'Register';

