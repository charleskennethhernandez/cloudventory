<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



/*TITLES*/

$lang['add'] = 'Add';

$lang['save'] = 'Save';

$lang['edit'] = 'Edit';

$lang['delete'] = 'Delete';

$lang['close'] = 'Close';

$lang['search'] = 'Search';
