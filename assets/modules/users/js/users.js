$(document).ready(function(){
	var $modal = $('#modaluser');

	$('[btnAdduser]').on('click',function(e){
		e.preventDefault();
		$('#form_container').removeClass('hide');
		$('#list_container').addClass('hide');
		clear_fields('form_container');
	});

	$('[btnClose]').on('click',function(e){
		e.preventDefault();
		$('#form_container').addClass('hide');
		$('#list_container').removeClass('hide');
	});


	$(this).on('click','.btnDeleteuser',function(){
		var $this = $(this);
		swal({title:'Delete', text:'Are you sure you want to delete this record?',type:'warning',showCancelButton: true},function(confirm){
			if(!confirm) return false;
			$.ajax({
				url: BASE_URL + 'users/ajaxDelete'
				, type: 'POST'
				, dataType: 'JSON'
				, data : {id:$this.data('id')}
				, success: function(response){
					if(!response.success){
						alert(error);
						swal('Error', response.message,'error');
					} else {
						swal('Success', 'user has been deleted');
						document.location.reload();
					}
				}
				, error:function(x,r,e){
					swal('Error', e, 'error');
				}
			});
		})
	});
	$('[btnEdituser]').on('click',function(e){
		e.preventDefault();
		$('#form_container').removeClass('hide');
		$('#list_container').addClass('hide');
		var $form = $('form[name~=frmuser]');
		var id = $(this).data('id');
		var data = userS_MAP[id];
		$.each(data,function(k,v){
			$('[name~='+k+']').val(v);
		});
	});
	$('[btnSubmituser]').on('click',function(){
		$('form[name~=frmuser]').submit();
	});
	$('form[name~=frmuser]').validate({
		submitHandler:function(form){
			var $form = $(form);
			$.ajax({
				url: BASE_URL + 'users/ajaxAdd'
				, type: 'POST'
				, dataType: 'JSON'
				, data: $form.find('[name]')	
				, success:function(response){
					if(!response.success){
						$form.find('.errors').removeClass('hidden').html(response.message);
						swal('Error', response.message,'error');
					}
					else{
						swal('Success', 'user has been added');
						$modal.modal('hide');	
						document.location.reload();
					}
				}
				, error:function(x,r,e){
					swal('Error', e, 'error');
				}
			});
		}
	});
});

jQuery(function(){
    // Init page helpers (BS Datepicker + BS Datetimepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
    // App.initHelpers(['datepicker', 'datetimepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']);
    App.initHelpers(['datepicker']);
});
function clear_fields(id){
	$('#'+id+" input[type=text]").val('');
	$('#'+id+" input[type=password]").val('');
	$('#'+id+" input[type=email]").val('');
	$('#'+id+" input[type=hidden]").val('');
	$('#'+id+' select').val('');
	$('#'+id+' textarea').html('');
	$('#'+id+' textarea').val('');
}