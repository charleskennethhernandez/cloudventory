$(document).ready(function(){
	var $modal = $('#modalinventory');

	$(this).on('click','[btnAdd]',function(){
		$('#form_container').removeClass('hide');
		$('#list_container').addClass('hide');
		$('#image_container').addClass('hide');
		$('.dropzone').addClass('hide');
		$('#file_container').addClass('hide');
		$('#video_url_container').addClass('hide');
		$('#image_url_container').addClass('hide');
	});

	$(this).on('click','[btnClose]',function(){
		$('#form_container').addClass('hide');
		$('#list_container').removeClass('hide');
	});


	$(this).on('click','[btnDelete]',function(){
		var $this = $(this);
		swal({title:'Delete', text:'Are you sure you want to delete this record?',type:'warning',showCancelButton: true},function(confirm){
			if(!confirm) return false;
			$.ajax({
				url: BASE_URL + 'inventories/ajaxDelete'
				, type: 'POST'
				, dataType: 'JSON'
				, data : {id:$this.data('id')}
				, success: function(response){
					if(!response.success){
						alert(error);
						swal('Error', response.message,'error');
					} else {
						swal('Success', 'Inventory item has been deleted');
						document.location.reload();
					}
				}
				, error:function(x,r,e){
					swal('Error', e, 'error');
				}
			});
		})
	});
	$(this).on('click','[btnEdit]',function(){
		$('#form_container').removeClass('hide');
		$('#list_container').addClass('hide');
		$('#image_container').removeClass('hide');
		$('.dropzone').removeClass('hide');
		$('#file_container').removeClass('hide');
		$('#video_url_container').removeClass('hide');
		$('#image_url_container').removeClass('hide');

		var $form = $('form[name~=frminventory]');
		var id = $(this).data('id');
		var data = inventories_MAP[id];
		
		$.each(data,function(k,v){
			$('[name~='+k+']').val(v);
			if (k == "video_url") {
				$('#video_url_container').html( v );
			};
			if (k == "image_url") {
				$('#image_url_container').html( "<img src='"+v+"'>" );
			};
		});

		$.ajax({
			type: 'POST',
			url: BASE_URL +'file_uploads/get_files',
			data: { 
				module_id : id, 
				module : $('#module').html(), 
			},
			success: function(data){
				$('#file_container').html( data );
			}
		});

	});
	$('[btnSubmit]').on('click',function(){
		$('form[name~=frminventory]').submit();
	});
	$('form[name~=frminventory]').validate({
		submitHandler:function(form){
			var $form = $(form);
			$.ajax({
				url: BASE_URL + 'inventories/ajaxAdd'
				, type: 'POST'
				, dataType: 'JSON'
				, data: $form.find('[name]')	
				, success:function(response){
					if(!response.success){
						$form.find('.errors').removeClass('hidden').html(response.message);
						swal('Error', response.message,'error');
					} else {
						swal('Success', 'Inventory item has been added');
						$modal.modal('hide');	
						document.location.reload();
					}
				}
				, error:function(x,r,e){
					swal('Error', e, 'error');
				}
			});
		}
	});
});

jQuery(function(){
    // Init page helpers (BS Datepicker + BS Datetimepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
    // App.initHelpers(['datepicker', 'datetimepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']);
    App.initHelpers(['datepicker']);
});