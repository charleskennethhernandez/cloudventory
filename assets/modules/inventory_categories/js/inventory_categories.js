$(document).ready(function(){
	var $modal = $('#modalinventory_category');

	$('[btnAddinventorycategory]').on('click',function(e){
		e.preventDefault();
		$('#form_container').removeClass('hide');
		$('#list_container').addClass('hide');
		$('#image_container').addClass('hide');
	});

	$('[btnClose]').on('click',function(e){
		e.preventDefault();
		$('#form_container').addClass('hide');
		$('#list_container').removeClass('hide');
		$('#image_container').addClass('hide');
	});


	$(this).on('click','.btnDeleteinventorycategory',function(){
		var $this = $(this);
		swal({title:'Delete', text:'Are you sure you want to delete this record?',type:'warning',showCancelButton: true},function(confirm){
			if(!confirm) return false;
			$.ajax({
				url: BASE_URL + 'inventory_categories/ajaxDelete'
				, type: 'POST'
				, dataType: 'JSON'
				, data : {id:$this.data('id')}
				, success: function(response){
					if(!response.success){
						alert(error);
						swal('Error', response.message,'error');
					} else {
						swal('Success', 'inventory_category has been deleted');
						document.location.reload();
					}
				}
				, error:function(x,r,e){
					swal('Error', e, 'error');
				}
			});
		})
	});
	
	$('[btnEditinventorycategory]').on('click',function(e){
		e.preventDefault();
		$('#form_container').removeClass('hide');
		$('#list_container').addClass('hide');
		$('#image_container').removeClass('hide');
		var $form = $('form[name~=frminventory_category]');
		var id = $(this).data('id');
		var data = inventory_categories_MAP[id];
		$.each(data,function(k,v){
			$('[name~='+k+']').val(v);
		});
	});

	$('[btnSubmitinventory_category]').on('click',function(){
		$('form[name~=frminventory_category]').submit();
	});
	$('form[name~=frminventorycategory]').validate({
		submitHandler:function(form){
			var $form = $(form);
			$.ajax({
				url: BASE_URL + 'inventory_categories/ajaxAdd'
				, type: 'POST'
				, dataType: 'JSON'
				, data: $form.find('[name]')	
				, success:function(response){
					if(!response.success){
						$form.find('.errors').removeClass('hidden').html(response.message);
						swal('Error', response.message,'error');
					} else {
						swal('Success', 'inventory category has been added');
						$modal.modal('hide');	
						document.location.reload();
					}
				}
				, error:function(x,r,e){
					swal('Error', e, 'error');
				}
			});
		}
	});
});

jQuery(function(){
    // Init page helpers (BS Datepicker + BS Datetimepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
    // App.initHelpers(['datepicker', 'datetimepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']);
    App.initHelpers(['datepicker']);
});