$(document).ready(function(){
	var $modal = $('#modalbrand');

	$('[btnAddbrand]').on('click',function(e){
		e.preventDefault();
		$('#form_container').removeClass('hide');
		$('#list_container').addClass('hide');
	});

	$('[btnClose]').on('click',function(e){
		e.preventDefault();
		$('#form_container').addClass('hide');
		$('#list_container').removeClass('hide');
	});


	$(this).on('click','.btnDeletebrand',function(){
		var $this = $(this);
		swal({title:'Delete', text:'Are you sure you want to delete this record?',type:'warning',showCancelButton: true},function(confirm){
			if(!confirm) return false;
			$.ajax({
				url: BASE_URL + 'brands/ajaxDelete'
				, type: 'POST'
				, dataType: 'JSON'
				, data : {id:$this.data('id')}
				, success: function(response){
					if(!response.success){
						alert(error);
						swal('Error', response.message,'error');
					} else {
						swal('Success', 'brand has been deleted');
						document.location.reload();
					}
				}
				, error:function(x,r,e){
					swal('Error', e, 'error');
				}
			});
		})
	});
	$('[btnEditbrand]').on('click',function(e){
		e.preventDefault();
		$('#form_container').removeClass('hide');
		$('#list_container').addClass('hide');
		var $form = $('form[name~=frmbrand]');
		var id = $(this).data('id');
		var data = brands_MAP[id];
		$.each(data,function(k,v){
			$('[name~='+k+']').val(v);
		});
	});
	$('[btnSubmitbrand]').on('click',function(){
		$('form[name~=frmbrand]').submit();
	});
	$('form[name~=frmbrand]').validate({
		submitHandler:function(form){
			var $form = $(form);
			$.ajax({
				url: BASE_URL + 'brands/ajaxAdd'
				, type: 'POST'
				, dataType: 'JSON'
				, data: $form.find('[name]')	
				, success:function(response){
					if(!response.success){
						$form.find('.errors').removeClass('hidden').html(response.message);
						swal('Error', response.message,'error');
					}
					else{
						swal('Success', 'Brand has been added');
						$modal.modal('hide');	
						document.location.reload();
					}
				}
				, error:function(x,r,e){
					swal('Error', e, 'error');
				}
			});
		}
	});
});

jQuery(function(){
    // Init page helpers (BS Datepicker + BS Datetimepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
    // App.initHelpers(['datepicker', 'datetimepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']);
    App.initHelpers(['datepicker']);
});