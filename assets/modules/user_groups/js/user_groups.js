$(document).ready(function(){
	var $modal = $('#modaluser_group');

	$('[btnAdd]').on('click',function(e){
		e.preventDefault();
		clear_all_fields();
		$('#form_container').removeClass('hide');
		$('#list_container').addClass('hide');
	});

	$( this ).on( 'click', '.btnClose', function() {
		$('#form_container').addClass('hide');
		$('#list_container').removeClass('hide');
	});


	$(this).on('click','[btnDelete]',function(){
		var $this = $(this);
		swal({title:'Delete', text:'Are you sure you want to delete this record?',type:'warning',showCancelButton: true},function(confirm){
			if(!confirm) return false;
			$.ajax({
				url: BASE_URL + 'user_groups/ajaxDelete'
				, type: 'POST'
				, dataType: 'JSON'
				, data : {id:$this.data('id')}
				, success: function(response){
					if(!response.success){
						alert(error);
						swal('Error', response.message,'error');
					} else {
						swal('Success', 'User Group has been deleted');
						document.location.reload();
					}
				}
				, error:function(x,r,e){
					swal('Error', e, 'error');
				}
			});
		})
	});

	$( this ).on( 'click', '.btnEdit', function() {
		var id = $( this ).data( 'id' );
		$.post( BASE_URL + 'user_groups/information', { id : id }, function( data ) {
			$('#form_container').removeClass('hide');
			$('#list_container').addClass('hide');
			$('#form_container').html(data);
		});
	} );

	$('[btnSubmit]').on('click',function(){
		$('form[name~=frm]').submit();
	});

	$('form[name~=frm]').validate({
		submitHandler:function(form){
			var $form = $(form);
			$.ajax({
				url: BASE_URL + 'user_groups/ajaxAdd'
				, type: 'POST'
				, dataType: 'JSON'
				, data: $form.find('[name]')	
				, success:function(response){
					if(!response.success){
						$form.find('.errors').removeClass('hidden').html(response.message);
						swal('Error', response.message,'error');
					}
					else{
						swal('Success', 'User Group has been added');
						$modal.modal('hide');	
						document.location.reload();
					}
				}
				, error:function(x,r,e){
					swal('Error', e, 'error');
				}
			});
		}
	});
});
function clear_all_fields(){
	$('select').val('');
	$('input').val('');
	$('textarea').html('');
}
jQuery(function(){
    // Init page helpers (BS Datepicker + BS Datetimepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
    // App.initHelpers(['datepicker', 'datetimepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']);
    App.initHelpers(['datepicker']);
});