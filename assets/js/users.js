var datatableUsers;
$(document).ready(function(){
    var $form = $('form[name~=frmAddUser]');
    $form.find('select[name~=role]').dropdown({
        url: BASE_URL+'/api/roles/all'
        , key: 'id'
        , label: 'name'
    });
	datatableUsers = $('[tblUsers]').dataTable( {
        ajax: BASE_URL+'/api/users/all',
        pageLength:8,
        processing: true,
        columns: [
            { data: "username", title:'Username' },
            { data: null },
        ]
        , aoColumnDefs:[{
            mRender:function(data,type,row){
                return '\
                    <div class="btn-group">\
                        <button data-id="'+data.id+'" class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="" data-original-title="Edit Client"><i class="fa fa-pencil"></i></button>\
                        <button data-id="'+data.id+'" api-service-success-callback="datatableUsers.DataTable().ajax.reload()" api-service-delete="/api/roles/delete" class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="" data-original-title="Remove Client"><i class="fa fa-times"></i></button>\
                    </div>\
                ';
            }
            , aTargets:[1]
        }]
        ,fnDrawCallback:function(oSettings){
            api_service_init();
        }
    } );
	$form.validate(jQuery.extend(validation_preoptions,{
        submitHandler:function(form){
            var form = $(form);
            $.ajax({
                type: form.attr('method'),
                url: BASE_URL+'/api/users/insert',
                dataType: 'JSON',
                data: form.serialize(),
                success: function (r) {
                    if(!r.success){
                        form_error_message(form, r.message);
                        return false;
                    }
                    system_message('User has been added.');
                    $('#modalAddUser').modal('hide');
                }
            });
        }
    }));
	$('[btnAddUser]').on('click',function(){
		$('form[name~=frmAddUser]').submit();
	});
});