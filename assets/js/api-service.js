var api_service_init = function(){
	$('[api-service-delete]').on('click',function(){
		$el = $(this);
		if(!confirm('Are you sure you want to delete this record?'))
			return false;
		$.ajax({
			url: BASE_URL+$el.attr('api-service-delete')
			, type: 'POST'
			, dataType: 'JSON'
			, data: {
				id: $el.data('id')
			}
			, success:function(r){
				if(!r.success){
					swal('Error',r.message,'error');
					return false;
				}
				system_message('Record has been deleted');
				eval($el.attr('api-service-success-callback'));
			}
			, error:function(x,r,m){
				swal('Error',r,'error');
			}
		})
	});
}