$(document).ready(function(){
	$('[btnLogin]').on('click',function(e){
		e.preventDefault();
		$('form[name~=frmLogin]').submit();
	});
	$('form[name~=frmLogin]').validate(jQuery.extend(validation_preoptions,{
        submitHandler:function(form){
            var form = $(form);
            $.ajax({
                type: form.attr('method'),
                url: BASE_URL+'api/auth',
                dataType: 'JSON',
                data: form.serialize(),
                success: function (r) {
                    if(!r.success){
                        form_error_message(form, r.message);
                        return false;
                    }
                    document.location.href = BASE_URL;
                }
            });
        }
    }));
});