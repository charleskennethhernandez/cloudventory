$(document).ready(function(){
	// Dropzone.autoDiscover = false;
	$('.dropzone').dropzone ({
	    url: BASE_URL + 'file_uploads',
	    init: function() {
	        this.on("sending", function(file, xhr, formData){
	            formData.append("id", $('#id').val())
	            formData.append("module", $('#module').html())
	        }),
	        this.on("success", function(file, xhr){
	            console.log(file);
	        })
	    },
	    addRemoveLinks: true,
		removedfile: function(file) {
			console.log(file);
		    var data = file.xhr.response;   
		    $.ajax({
		        type: 'POST',
		        url: BASE_URL + 'file_uploads/delete',
		        data: "return="+data,
		        dataType: 'html'
		    });
			var _ref;
			return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;        
		}
	});
});	

