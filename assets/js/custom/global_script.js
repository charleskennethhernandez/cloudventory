$('document').ready(function(){
	/* GLOBAL SCRIPT START */
	$( document ).on( 'change' , '.checkall' , function() {
		if( $( this ).prop( 'checked' ) ) {
			$( 'tbody tr td input[type="checkbox"]' ).each( function() {
				$( this ).prop( 'checked', true );
			} );
		} else {
			$( 'tbody tr td input[type="checkbox"]' ).each( function() {
				$( this ).prop( 'checked', false );
				
			} );
		}
	} );
	
    $( document ).on( 'change', 'table tr td input[type="checkbox"]', function() {
	
		var total_checkbox = 0;
		
		var total_checked_box = 0;
		
		$( 'tbody tr td input[type="checkbox"]' ).each( function() {
			
			 total_checkbox++;
				
		} );
		
		$( 'tbody tr td input[type="checkbox"]' ).each( function() {
			
			 if( $( this ).prop( 'checked' ) ) {
			 
				total_checked_box++;
			 
			 }
				
		} );
		
		if( total_checkbox == total_checked_box ) {
		
			$( '.checkall' ).prop( 'checked', true );
		
		} else {
		
			$( '.checkall' ).prop( 'checked', false );
		
		}
		
    } );
	
	
	$( document ).on( 'click', '.delete-selected-btn', function( event ) {
		
		var total_checked_box = 0;
		
		$( 'tbody tr td input[type="checkbox"]' ).each( function() {
			
			 if( $( this ).prop( 'checked' ) ) {
			 
				total_checked_box++;
			 
			 }
				
		} );
		
		if( total_checked_box == 0 ) {
		
			event.preventDefault();
		
			modal_trigger('Warning','Please check boxes!' );
		}
	
	} );

	$( document ).on("mouseenter","label.error",function(){
	  $(this).fadeOut(100).remove();
	});
	/* GLOBAL SCRIPT END*/
});
	