/** DEVELOPER **/
$( function() {
	/* ACCOUNT SCRIPT START */
		$( document ).on('click', '.edit_account', function () {
			$('#account_container_list').hide();
			$('#account_container_add').show();
			$('#message_container_list').hide();
			$('#account_container_add').removeClass('hide');
		} );
		$( document ).on('click', '.close_account_edit', function () {
			$('#account_container_list').show();
			$('#account_container_add').hide();
			$('#message_container_list').show();
		} );
	/* ACCOUNT SCRIPT END */
	/* MESSAGE SCRIPT START */
		$( document ).on('change', '#user_type_id', function () {
			id = $(this).val();
			temp = "<option value='0'>SELECT RECEPIENT</option>";
			if (id != 0) {
				$.ajax({
			        url:base_url+'support/change_recipient',
			        type: 'post',
			        data: {
			        	id : id
			        },
			        success: function (data) {
			        	$('#recipient').html(data);
						$('select').trigger("chosen:updated");

			        }
				});
			} else {
			    $('#recipient').html(temp);
			}
		} );
		$( document ).on('click', '.add_message', function () {
			id = parseInt($(this).attr('data-id'));
			if (id == 0) {
				modal_trigger('Warning','You cannot send message without transaction to the developer');
				return false;
			};
			$('#message_container_list').hide();
			$('#message_container_add').show();
			$('#message_container_add').removeClass('hide');
			$('#account_container_list').hide();
			tinymce.init({
				mode : "none",
				selector: "textarea#content",
				plugins : "fullscreen",
				theme_advanced_buttons3_add : "fullscreen",
				fullscreen_new_window : false,
				fullscreen_settings : {
					theme_advanced_path_location : "top"
				},
				setup : function(ed) {
				  ed.on('change', function(e) {
					 $('#content').html(tinyMCE.activeEditor.getContent());
					  console.log($('#content').html());
				  });
				   ed.on('keypress', function(e) {
					  $('#content').html(tinyMCE.activeEditor.getContent());
					  console.log($('#content').html());
				  });
			   }
			});
			
			clear_all_message_values();
			
			if ($('#content').html() != '')
			{
				tinyMCE.activeEditor.setContent('');
			}
			$("label.error").hide();
			$('input[type="text"]').val('');
			$(".error").removeClass("error");
			$('#user_type_id').val('');
			$('select').trigger("chosen:updated");
		} );
		$( document ).on('click', '.close_view_message', function () {
			$('#message_view_container').hide();
		} );
		
		$( document ).on('click', '.close_message_add', function () {
			$('#message_container_list').show();
			$('#message_container_add').hide();
			$('#account_container_list').show();
			
			clear_all_message_values();
		} );
		
		
		$( document ).on( 'submit', '#support_delete_selected', function( event ) {
		
			event.preventDefault();
		
			
			confirm_trigger( 'Do you want to delete selected?' );
			$("#cofirm_button").unbind('click');
			$('#cofirm_button').click(function(){
			
				$( 'html' ).waitMe( {
					effect : 'ios',
					text : 'Processing...',
					bg : 'rgba(0,0,0,0.7)',
					color : '#fff',
					sizeW : '',
					sizeH : ''
				} );
				$.post( base_url + 'support/delete_selected',  $('#support_delete_selected').serializeArray(), function( response ) {
					$( 'html' ).waitMe( 'hide' );
					$('#message_list_table_container').html( response.message_list_table );

					// count = parseInt($('.count-item.msg').attr('data-count')) - 1;
					// count = (count<=0)? 0 : count;
					
					// $('.count-item.msg').attr('data-count',count);
					// $('.count-item.msg').html('('+count+')');

					modal_trigger('Success','Message has been ' + response.action + 'd!' );
				}, 'json' );
				
			});
			
		} );
		$(this).on('click', '#message_list_table_container .row_mail', function () {
		
			if($(this).hasClass('unread')){
				new_value = parseInt($('#message_counter').html()) - 1;
				if(new_value == 0) {
					new_value = '';
				}
				$('#message_counter').html(new_value);
			}
			$('#message_list_table_container .row_mail').removeClass('selected');
			message_id = parseInt($(this).attr('data-id'));
			$(this).addClass('selected');
			$('#msg_container_view').show();
			if ($(this).hasClass('unread')) {
				$(this).removeClass('unread');
				count = parseInt($('.count-item').attr('data-count')) - 1;
				count = (count<=0)? 0 : count;
				$('.count-item').html('('+count+')');
				$('.count-item.msg').attr('data-count',count);
				$('#message_counter').html(count);
				$.ajax({
			        url: base_url+'support/update_status/',
			        type: 'post',
			        data: {
		            id : message_id,
		            read : '1'
			        }
				});
			};
			$.post( base_url + 'support/information', { id : message_id }, function( response ) {
			
				if( response ) {
					
					assign_message_values( response );
					$('#message_view_container').show();
					$('#message_view_container').removeClass('hide');
				
				} else {
				
					modal_trigger('Error','Error retrieving information' );
				
				}
				
			}, 'json' );
		});
		$(this).on('click', '.star', function () {
			message_id = parseInt($(this).closest('tr').attr('data-id'));
			if ($(this).hasClass('star-btn-high')) {
				val = 0
				$(this).removeClass('star-btn-high').addClass('star-btn');
			} else {
				val = 1;
				$(this).removeClass('star-btn').addClass('star-btn-high');
			}
			$.ajax({
		        url: base_url+'support/update_status/',
		        type: 'post',
		        data: {
		            id : message_id,
		            important : val
		        }
			});
		});
			
		$(this).on('click', '.reply-btn', function (){
			message_id = parseInt($(this).attr('data-id'));
			$('#message_container_list').hide();
			$('#message_container_add').show();
			$('#message_container_add').removeClass('hide');
			$.post( base_url + 'support/information', { id : message_id }, function( response ) {
			
				if( response ) {
					
					assign_message_values( response );
					$('#message_view_container').show();
					$('#message_view_container').removeClass('hide');
				} else {
				
					modal_trigger('Error','Error retrieving information' );
				
				}
				
			}, 'json' );
		});

		$(this).on('click', '.view-msg', function (e){
	      message_id = parseInt($(this).attr('data-id'));
	      $('#sent_message_container_list').hide();
	      // $('#sent_message_view_container').show();
	      // $('#sent_message_view_container').removeClass('hide');
	      $.post( base_url + 'support/information_sent', { id : message_id, sent : '1' }, function( response ) {
	        if( response ) {
	          $('#sent_message_view_container').show();
	          $('#sent_message_view_container').removeClass('hide');
	          assign_message_values( response );
	        } else {
	          // alert( 'Error retrieving information' );
	          modal_trigger('Error','Error retrieving information');
	        }
	      }, 'json' );
	      e.stopImmediatePropagation();
	    });
	    $( document ).on('click', '.close_view_message', function () {
			$('#message_view_container').hide();
			$('#sent_message_view_container').hide();
			$('#sent_message_container_list').show();
		} );
		
		/* JUST CALL THIS FOR THE PROJECT TO ASSIGN VALUES */
		function assign_message_values( response ) {
				
			$( '.sender_name' ).html( response.sender_name );
			
			$( '.subject' ).html( response.subject );
				
			$( '.content' ).html( response.content );
			$( '.created_date' ).html( response.created_date );
			$( '#user_type_id' )
				.val( response.sender_user_type_id )
				.trigger('change');

			$( '#recipient' )
				.val( response.sender_user_id )
				.removeAttr( 'multiple' )
				.trigger('change')
				.attr( 'readonly' , true);


			tinymce.init({
				mode : "none",
				selector: "textarea#content",
				plugins : "fullscreen",
				theme_advanced_buttons3_add : "fullscreen",
				fullscreen_new_window : false,
				fullscreen_settings : {
					theme_advanced_path_location : "top"
				},
				setup : function(ed) {
					ed.on('change', function(e) {
						$('#content').html(tinyMCE.activeEditor.getContent());
						console.log($('#content').html());
					});
					ed.on('keypress', function(e) {
						$('#content').html(tinyMCE.activeEditor.getContent());
						console.log($('#content').html());
					});
				}
			});
		}
		
		//RESET ALL VALUES TO NULL
		function clear_all_message_values() {
		
			$( '.id' ).val( '' );
				
			$( '.sender' ).val( '' );
				
			$( '.subject' ).val('');
			$( '.content' ).val('');
			$( 'table tr td input[type="checkbox"]' ).each( function() {
				
				$( this ).prop( 'checked', false );
					
			} );
		
		}
	/* MESSAGE SCRIPT END */
	/* PROPERTY SCRIPT START */
		$( document ).on( 'click', '.btn-view-property', function() {
		
			var id = $( this ).data( 'id' );
		
			$.post( base_url + 'properties/information', { id : id }, function( response ) {
			
				if( response ) {
					
					$('#single_property_container_list').removeClass('hide');
					$('#single_property_container_list').show();
					assign_property_values( response );
				
				} else {
				
					modal_trigger('Error','Error retrieving information' );
				
				}
				
			}, 'json' );
		
		} );
		/* JUST CALL THIS FOR THE PROPERTY TO ASSIGN VALUES */
		// function assign_property_values( response ) {
		
		// 	$( '#price' ).html( response.price );
		// 	$( '#property_name' ).html( response.property_name );
			
		// 	if( response.image != null ) {
			
		// 		$('#image_container').html('<img src="http://'+server_url+'/projects/cornerstone/assets/img/image/'+response.image+'" class="image_type">');
			
		// 	}
		// 	if( response.status_name == 'Completed' ) {
			
		// 		$('#stats').html('<span style="" class="green-lbl green-lbl-prog">COMPLETED</span>');
			
		// 	} else {
		// 		$('#stats').html('<span style="" class="orange-lbl orange-lbl-prog">ON GOING</span>');
		// 	}
		
		// }
		/* JUST CALL THIS FOR THE PROJECT TO ASSIGN VALUES */
		function assign_property_values( response ) {
			$( '#docu-container' ).html( '' );
			$( '#price' ).html( response.original_property_value );
			$( '#property_name' ).html( response.property_name );
			var arr = response.property_code.split('-');
			blk = parseInt(arr[2], 10);
			$( '#view_map' ).attr( 'href','http://'+server_url+'/cornerstone/developers/map/view/'+response.project_id+'#'+response.property_code+'blk'+blk );
			$( '#view_map' ).attr( 'data-status',response.virtual_map_status );
			if( response.image != null ) {
				
				$('#image_container').html('<img src="http://'+server_url+'/cornerstone/developers/assets/img/image/'+response.image+'" class="image_type">');
			
			}
			$('#status_container').html('<img src="http://'+server_url+'/cornerstone/client/assets/img/cornerstone/'+response.unit_type_id+'/'+response.project_progress+'.jpg" style="width:243px">');
			$('.progress-label').html(response.project_progress+'<span style="font-size:18px">%</span>');
			count = 0;
			$(response.documents).each(function(index, data){
				count++;
					type = data.type;
					filename = data.filename;
					name = data.name;
					temp = '<div class="row" style="margin:15px 0px;"><a target="_blank" href="http://'+server_url+'/cornerstone/developers/assets/files/form_file/'+filename+'" style="text-decoration:none;"><span class="files_ext '+type+'"><b>'+type+'</b></span><span class="blue-green filename">'+name+'</span></a></div>';
					$( '#docu-container' ).append( temp );
			});
			if (count == 0) {
				$('.view_files').hide();
				$('.no_files').show().removeClass('hide');
			} else {
				$('.view_files').show();
				$('.no_files').hide().removeClass('hide');
			}
			// alert(response.property_progress);
			if (response.property_progress != "100") {
				$('.progress-label').css('color','#fda738');
				$('#stats').html('<span style="" class="orange-lbl orange-lbl-prog">ON GOING</span>');
			} else {
				
				$('.progress-label').css('color','#90ca38');
				$('#stats').html('<span style="" class="green-lbl green-lbl-prog">COMPLETED</span>');
			}
		
		}
		$( document ).on('click','#view_map',function(){
			status = parseInt($(this).attr('data-status'));
			if (status != 5) {
				modal_trigger( 'Warning', 'The map stages is not yet completed');
				return false;
			} else {
				return true;
			}
		});
		/* JUST CALL THIS FOR THE PROJECT TO ASSIGN VALUES */
		// function assign_property_values( response ) {
		// 	$( '#price' ).html( response.price );
		// 	$( '#property_name' ).html( response.property_name );
			
		// 	if( response.image != null ) {
				
		// 		$('#image_container').html('<img src="http://'+server_url+'/projects/cornerstone/assets/img/image/'+response.image+'" class="image_type">');
			
		// 	}
		// 	$('#status_container').html('<img src="http://'+server_url+'/projects/cornerstone-client/assets/img/cornerstone/'+response.property_progress+'.jpg" style="width:117px">');
		// 	$('.progress-label').html(response.property_progress+'<span style="font-size:18px">%</span>');
			
		// 	if( response.status_name == 'AVAILABLE' ) {
		// 		$('.progress-label').css('color','#90ca38');
			
		// 		$('#stats').html('<span style="" class="green-lbl green-lbl-prog">COMPLETED</span>');
			
		// 	} else if ( response.status_name == 'RESERVED' ) {
		// 		$('.progress-label').css('color','#90ca38');
		// 		$('#stats').html('<span style="" class="green-lbl green-lbl-prog">COMPLETED</span>');
		// 	} else {
		// 		$('.progress-label').css('color','#fda738');
		// 		$('#stats').html('<span style="" class="orange-lbl orange-lbl-prog">ON GOING</span>');
		// 	}
		
		// }
	/* PROPERTY SCRIPT END */
	/* PAYMENT SCRIPT START */
		$(document).on('change','#choose_select_property_payment',function () {
			val = parseInt($(this).val());
			if (val != 0) {
				$.ajax({
			        url:base_url+'payments/form_ajax',
			        type: 'post',
			        data: {
			        	property_id : val
			        },
			        success: function (data) {
			        	$('#complete_list_table_container').html(data);
			        }
				});
				$.ajax({
			        url:base_url+'payments/pending_form_ajax',
			        type: 'post',
			        data: {
			        	property_id : val
			        },
			        success: function (data) {
			        	$('#payment_list_table_container').html(data);
			        }
				});
				
			} else {
				$.ajax({
			        url:base_url+'payments',
			        type: 'post',
			        data: {
			        	refresh : 1
			        },
			        success: function (data) {
			        	$('#right_container').html(data);
			        }
				});
			}
		});
		$( document ).on( 'click', '.btn-view-payment', function() {
		
			var id = $( this ).data( 'id' );
			no = parseInt($( this ).data( 'no' ));
		
			$.post( base_url + 'payments/transaction_information', { id : id }, function( response ) {
			
				if( response ) {
					$('#payment_container_list').hide();
					$('#mortgage_view').hide();
					
					$('#payment_view').hide();
					$('#payment_container_view').removeClass('hide');
					$('#payment_container_view').show();
					$('#payment_list_view_table_container').html(response);
					
					if (no == 1) {
						$('#information').hide();
					} else {
						$('#information').show();
					}
				} 
				
			});
		
		} );
		$(document).on('click','#btn-view-payment',function(){
			$('#information').hide();
		})
		$( document ).on('click', '.close_payment_view', function () {
			$('#payment_container_view').hide();
			$('#payment_container_list').show();
			$('#complete_container_list').show();
		} );
		$( document ).on( 'click', '.btn-view-complete', function() {
		
			var id = $( this ).data( 'id' );
		
			$.post( base_url + 'payments/transaction_information', { id : id }, function( response ) {
			
				if( response ) {
					$('#complete_container_list').hide();
					$('#mortgage_view').hide();
					$('#payment_view').hide();
					$('#complete_container_view').removeClass('hide');
					$('#complete_container_view').show();
					$('#complete_list_view_table_container').html(response);
				
				} 
				
			});
		
		} );
		$( document ).on('click', '.close_complete_view', function () {
			$('#complete_container_view').hide();
			$('#complete_container_list').show();
			
		} );	
		$( document ).on('click', '.payment_content', function () {
			$('.payment_content').removeClass('selected');
			$(this).addClass('selected');
			id = $(this).attr('data-id');
			value = $(this).attr('data-value');
			downpayment = $(this).attr('data-downpayment');
			reservation = $(this).attr('data-reservation');
			interest = $(this).attr('data-interest');
			loan = $(this).attr('data-loan');
			monthly = $(this).attr('data-monthly');
			next_date = $(this).attr('data-next-date');
			property_name = $(this).attr('data-property-name');
			$('.view_mortgage').attr({'data-id':id,'data-value':value,'data-downpayment':downpayment,'data-reservation':reservation,'data-interest':interest,'data-loan':loan});
			$('.transaction_id').val(id);
			$('.payment_amount').val(monthly);
			$('.next_payment_date').val(next_date);
			$('.property_name').val(property_name);
			$('#property_name').html(property_name);
			$('#btn-view-payment').attr('data-id',id);
		});
		$(this).on('click', '.view_mortgage', function () {
			id = $(this).attr('data-id');
			value = $(this).attr('data-value');
			downpayment = $(this).attr('data-downpayment');
			reservation = $(this).attr('data-reservation');
			interest = $(this).attr('data-interest');
			loan = $(this).attr('data-loan');
			if (id) {
			$.ajax({
		            url: base_url+'mortgage/process',
		            type: 'post',
		            data: {
		                id : id,
		                value : value,
		                downpayment : downpayment,
		                reservation : reservation,
		                interest : interest,
		                loan : loan
		            },
		            success: function (data) {
		            	$('#complete_container_list').hide();
						$('#payment_view').hide();
		            	$('#mortgage_view').removeClass('hide').show().html(data);
		            	
		            }
	        	});
			} else {
				modal_trigger('Warning','Select a transaction first')
			}
		});
		$( document ).on('click', '.close_mortgage_view', function () {
			$('#mortgage_view').hide();
			$('#complete_container_list').show();
		} );
		$(this).on('click', '.pay_mortgage', function () {
			id = $('.view_mortgage').attr('data-id');
			if (id) {
	        	$('#payment_view').removeClass('hide').show();
	        	$('#mortgage_view').hide();
	        	$('#complete_container_list').hide();
			} else {
				modal_trigger('Warning','Select a transaction first')
			}
		});
		
		$( document ).on('click', '.close_payment_form', function () {
			$('#payment_view').hide();
			$('#complete_container_list').show();
		});
	/* PAYMENT SCRIPT END */
	/* FILES SCRIPT START */
		$(document).on('click','.view-comment',function () {
			val = parseInt($(this).attr('data-id'));
			comment = $('.comment-form[data-id='+val+']').val();
			modal_trigger('Reason for file rejection','<p>'+comment+'</p>');
		});
		$(document).on('change','#choose_select_property',function () {
			val = parseInt($(this).val());
			if (val != 0) {
				$.ajax({
			        url:base_url+'files/form_ajax',
			        type: 'post',
			        data: {
			        	property_id : val
			        },
			        success: function (data) {
			        	$('#file_list_table_container').html(data);
			        }
				});
				$.ajax({
			        url:base_url+'files/completed_form_ajax',
			        type: 'post',
			        data: {
			        	property_id : val
			        },
			        success: function (data) {
			        	$('#complete_list_table_container').html(data);
			        }
				});
			} else {
				$.ajax({
			        url:base_url+'files',
			        type: 'post',
			        data: {
			        	refresh : 1
			        },
			        success: function (data) {
			        	$('#right_container').html(data);
			        }
				});
			}
		});
		$(document).on('click','.upload-btn',function () {
			id = $(this).attr('data-id');
			type_id = $(this).attr('data-type-id');
			$('#pdf_form').hide();
			$('.transaction_document_id').val(id);
			$('.type_id').val(type_id);
			$('#upload_form').show().removeClass('hide');
			$('#complete_container_list').hide();
		});
		$(document).on('click','.close_upload_form',function () {
			$('#upload_form').hide();
			$('#complete_container_list').show();
		});
		$(document).on('click','.edit-pdf',function () {
			id = $(this).attr('data-id');
			document_id = $(this).attr('data-document-id');
			trans_doc_id = $(this).attr('data-doc-id');
			$('#upload_form').hide();
			$('.transaction_template_id').val(id);
			$('.transaction_document_id').val(document_id);
			$('.trans_doc_id').val(trans_doc_id);
			$('#pdf_form').show().removeClass('hide');
			$('#complete_container_list').hide();
			$.ajax({
		        url:base_url+'files/get_template',
		        type: 'post',
		        data: {
		        	id : id
		        },
		        success: function (data) {
		        	$('#content_pdf').val(data);
		        	tinymce.init({
						mode : "none",
						selector: "textarea#content_pdf",
						plugins : "fullscreen",
						theme_advanced_buttons3_add : "fullscreen",
						fullscreen_new_window : false,
						fullscreen_settings : {
							theme_advanced_path_location : "top"
						},
						setup : function(ed) {
						  ed.on('change', function(e) {
							 $('#content_pdf').html(tinyMCE.activeEditor.getContent());
							  console.log($('#content').html());
						  });
						   ed.on('keypress', function(e) {
							  $('#content_pdf').html(tinyMCE.activeEditor.getContent());
							  console.log($('#content').html());
						  });
					   }
					});
		        }
			});
		});
		$(document).on('click','.close_pdf_form',function () {
			$('#pdf_form').hide();
			$('#complete_container_list').show();
		});
		$( document ).on( 'submit', '#file_update_selected', function( event ) {
			event.preventDefault();
			confirm_trigger( 'Do you want this selected files mark as submitted?' );
			$("#cofirm_button").unbind('click');
			$('#cofirm_button').click(function(){
				$( 'html' ).waitMe( {
					effect : 'ios',
					text : ' Processing...',
					bg : 'rgba(0,0,0,0.7)',
					color : '#fff',
					sizeW : '',
					sizeH : ''
				} );
				$.post( base_url + 'files/update_selected',  $('#file_update_selected').serializeArray(), function( response ) {
					$( 'html' ).waitMe( 'hide' );
					$('#file_list_table_container').html( response.file_list_table );
					$('#complete_list_table_container').html( response.completed_file_list_table );
					modal_trigger('Success','Files has been submitted!' );
				}, 'json' );
			});
		});
	/* FILES SCRIPT END */
} );
/** END DEVELOPER **/
$(document).ready(function(){	
	$(function() {
		$( document ).tooltip();
	});
	$(this).on('click', '.side-menu', function () {
		$('#right_container').html('');
		$('#right_container').addClass('cs-loader-big');

		id = $(this).attr('data-id');
		name = $(this).attr('data-name');
		if (name == "v2-properties") {
			name = "v2/properties";
		};
		window.location.hash = name;
		if((name == 'files')){
			window.location = base_url+'#'+name;
		}
		url = base_url+name;
		$.ajax({
	        url:url,
	        type: 'post',
	        data: {
	        	id : id
	        },
	        success: function (data) {
	        	$('#right_container').removeClass('cs-loader-big');
	        	$('#right_container').html(data);
	        	$('.btn-view-property:first').trigger('click');
	        	for_faq();
				$("select:not('.select2')").chosen();
	        }
		});
	});
/** INITIALIZE THE FIRST NAV **/
	hash = window.location.hash;
	if(hash != ''){
		
		module = hash.replace('#', ''); 
		
		$('.tabs-left ul li').removeClass('active');
		$('.tabs-left > ul > li > a.side-menu').each(function(){
			if($(this).attr('data-name') == module){
				$(this).closest('li').addClass('active');
			}
		});
		
		url = base_url+module;
		id = 1;
		
		$.ajax({
			url:url,
			type: 'post',
			data: {
				id : id
			},
			success: function (data) {
				$('#right_container').html(data);
	        	$('.btn-view-property:first').trigger('click');
				$("select:not('.select2')").chosen();

			}
		});
	} else {
		$.ajax({
			url:base_url+'support',
			type: 'post',
			data: {
				id : '1',
				limit_by : '1000'
			},
			success: function (data) {
				$('#right_container').html(data);
	        	$('.btn-view-property:first').trigger('click');
				$("select:not('.select2')").chosen();
	        	
			}
		});
	}
/** END FIRST NAV **/
	// $.ajax({
 //        url:base_url+'support',
 //        type: 'post',
 //        data: {
 //        	id : '1'
 //        },
 //        success: function (data) {
 //        	$('#right_container').html(data);
 //        }
	// });
	for_faq();
});
function for_faq(){
	$("td[colspan=2]").hide();
	$("td[colspan=2]").addClass('desc-faq');
	$(".faq_container table").click(function(event) {
	event.stopPropagation();
	var $target = $(event.target);
	if ( $target.closest("td").attr("colspan") > 1 ) {
	//$target.slideUp();
	//$target.closest("tr").prev().find("td:first").html("+");
	} else {
	$target.closest("tr").next().find("td[colspan=2]").toggle('fast');
	if ($target.closest("tr").find("td:last-child").html() == '<img style="float:right; margin:8px 0px 2px 10px" src="assets/img/cornerstone/up.png" alt="up.png">')
	$target.closest("tr").find("td:last-child").html('<img style="float:right; margin:8px 0px 2px 10px" src="assets/img/cornerstone/down.png" alt="down.png">');
	else
	$target.closest("tr").find("td:last-child").html('<img style="float:right; margin:8px 0px 2px 10px" src="assets/img/cornerstone/up.png" alt="up.png">');
	}
	});
}
