$(document).ready(function(){
	var $modal = $('#modalsales_order');	

	$(this).on('keyup','.to_reload',function(e){
		val = parseFloat($(this).val());

		if (!isNaN(val)) {
			reload_prices();
		} else {
			$(this).val(0);
		}
	});

	$(this).on('change','[name="is_customer_exists"]',function(e){
		val = $(this).val();
		if (val == 1) {
			$('#is_exists').removeClass('hide');
			$('#is_not_exists').addClass('hide');
		} else {
			$('#is_exists').addClass('hide');
			$('#is_not_exists').removeClass('hide');
		}
	});

	$(this).on('keyup','.amount_paid',function(e){
		val = parseFloat($(this).val());
		total_cost = parseFloat($('.overall_total_cost').val());
		total_due = parseFloat($('.total_due').val());
		

		if (!isNaN(val)) {
			if (val > total_cost) {
				$(this).val(total_cost);
				return false;
			} else {
				total_due = total_cost - val;
				$('.total_due').val(total_due);
			}
		} else {
			$(this).val(total_cost);
		}

	});

	$(this).on('click','[btnAddsalesitem]',function(e){
		e.preventDefault();
		var $this = $(this);

		id = $this.data('id');
		quantity = $this.data('quantity');
		customer_type = $('[name=customer_type]').val();
		overall_total_cost = $('[name=overall_total_cost]').val();

		if (quantity == 0) {
			swal('Warning', "This item is out of stock. You cannot add this to cart.",'error');
			return false;
		};

		if ($('#sales_item_id_'+id).length) {
			swal('Warning', "This item is already at the cart.",'error');
			return false;
		};

		swal({title:'Add to cart', text:'Are you sure you want to add this item to sales order?',type:'warning',showCancelButton: true},function(confirm){
			if(!confirm) return false;
			$.ajax({
				url: BASE_URL + 'sales_orders/sales_cart'
				, type: 'POST'
				, dataType: 'JSON'
				, data : { 
					id:id, 
					quantity: quantity,
					customer_type: customer_type
				}
				, success: function(response){
					if (overall_total_cost) {
						$(response.item).insertBefore('table#tbl_sales_cart > tbody > tr:first');
						reload_prices();
					} else {
						$('#cart_table').html(response.view);
					}
				}
				, error:function(x,r,e){
					swal('Error', e, 'error');
				}
			});
		});

	});

	$(this).on('click','[btnDeletesalesitem]',function(){
		var $this = $(this);
		// alert('#sales_item_id_'+$this.data('id'));
		$('#sales_item_id_'+$this.data('id')).remove();
		reload_prices();

		// swal({title:'Delete', text:'Are you sure you want to delete this item?',type:'warning',showCancelButton: true},function(confirm){
		// 	if(!confirm) return false;
		// 	$.ajax({
		// 		url: BASE_URL + 'sales_order_items/ajaxDelete'
		// 		, type: 'POST'
		// 		, dataType: 'JSON'
		// 		, data : {id:$this.data('id')}
		// 		, success: function(response){
		// 			if(!response.success){
		// 				alert(error);
		// 				swal('Error', response.message,'error');
		// 			} else {
		// 				swal('Success', 'Sales order item has been deleted');
		// 				document.location.reload();
		// 			}
		// 		}
		// 		, error:function(x,r,e){
		// 			swal('Error', e, 'error');
		// 		}
		// 	});
		// })
	});

	$('[btnAddsalesorder]').on('click',function(e){
		e.preventDefault();
		$('#form_container').removeClass('hide');
		$('#list_container').addClass('hide');
	});

	$('[btnClose]').on('click',function(e){
		e.preventDefault();
		$('#form_container').addClass('hide');
		$('#list_container').removeClass('hide');
	});
	

	$(this).on('click','.btnDeletesalesorder',function(){
		var $this = $(this);
		swal({title:'Delete', text:'Are you sure you want to delete this record?',type:'warning',showCancelButton: true},function(confirm){
			if(!confirm) return false;
			$.ajax({
				url: BASE_URL + 'sales_orders/ajaxDelete'
				, type: 'POST'
				, dataType: 'JSON'
				, data : {id:$this.data('id')}
				, success: function(response){
					if(!response.success){
						alert(error);
						swal('Error', response.message,'error');
					} else {
						swal('Success', 'Sales order has been deleted');
						document.location.reload();
					}
				}
				, error:function(x,r,e){
					swal('Error', e, 'error');
				}
			});
		})
	});
	$('[btnEditsales_order]').on('click',function(e){
		e.preventDefault();
		$('#form_container').removeClass('hide');
		$('#list_container').addClass('hide');
		var $form = $('form[name~=frmsales_order]');
		var id = $(this).data('id');
		var data = inventories_MAP[id];
		$.each(data,function(k,v){
			$('[name~='+k+']').val(v);
		});
	});
	$('[btnSubmitsales_order]').on('click',function(){
		$('form[name~=frmsales_order]').submit();
	});
	$('form[name~=frmsales_order]').validate({
		submitHandler:function(form){
			var $form = $(form);
			$.ajax({
				url: BASE_URL + 'sales_orders/ajaxAdd'
				, type: 'POST'
				, dataType: 'JSON'
				, data: $form.find('[name]')	
				, success:function(response){
					if(!response.success){
						$form.find('.errors').removeClass('hidden').html(response.message);
						swal('Error', response.message,'error');
					}
					else{
						swal('Success', 'Sales order item has been added');
						$modal.modal('hide');	
					}
				}
				, error:function(x,r,e){
					swal('Error', e, 'error');
				}
			});
		}
	});
});

function reload_prices() {
	overall_total_cost = 0.00;
	overall_total_discount = 0.00;
	$('.sales_item').each(function(){
		id = $(this).attr('id');
		inventory_quantity = parseFloat($('#'+id+' .inventory_quantity').val());
		sales_item_quantity = parseFloat($('#'+id+' .sales_item_quantity').val());
		sales_item_unit_cost = parseFloat($('#'+id+' .sales_item_unit_cost').val());
		sales_item_discount = parseFloat($('#'+id+' .sales_item_discount').val());
		sales_item_total_cost = parseFloat($('#'+id+' .sales_item_total_cost').val());

		if (sales_item_quantity > inventory_quantity) {
			sales_item_quantity = inventory_quantity;
			$('#'+id+' .sales_item_quantity').val(sales_item_quantity);
		} 
		// else {
		// 	alert(inventory_quantity +' '+ sales_item_quantity)
		// }

		if (sales_item_discount > sales_item_unit_cost) {
			sales_item_discount = sales_item_unit_cost;
			$('#'+id+' .sales_item_discount').val(sales_item_discount);
		};

		margin_price = sales_item_unit_cost - sales_item_discount;
		total_cost = (sales_item_quantity * margin_price);
		total_discount = (sales_item_discount * sales_item_quantity);

		$('#'+id+' .sales_item_total_cost').val(total_cost).trigger('change');

		overall_total_cost = overall_total_cost + total_cost;
		overall_total_discount = overall_total_discount + total_discount;

		// setTimeout(function(){
			// alert(id+' '+inventory_quantity+' '+sales_item_quantity+' '+sales_item_unit_cost+' '+sales_item_discount+' '+sales_item_total_cost);
		// }, 500);
	});
	total_due = parseFloat($('[name=overall_total_cost]').val()) - parseFloat($('[name=amount_paid]').val());
	$('[name=overall_total_cost]').val(overall_total_cost);
	$('[name=overall_total_discount]').val(overall_total_discount);
	$('[name=amount_paid]').val(overall_total_cost).trigger('keyup');
	$('[name=total_due]').val(total_due);
}

// jQuery(function(){
    // Init page helpers (BS Datepicker + BS Datetimepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
    // App.initHelpers(['datepicker', 'datetimepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']);
    // App.initHelpers(['datepicker']);
// });