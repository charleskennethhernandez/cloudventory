$(function() {
	$( document ).on('click', '[btnadd]', function () {
		$('#list_container').hide();
		$('#form_container').show().removeClass('hide');
		clear_all_values();
		$("label.error").hide();
		$(".error").removeClass("error");
	});
	$( document ).on('click', '[btnclose]', function () {
		$('#list_container').show();
		$('#form_container').hide().addClass('hide');
		clear_all_values();
	});
	$( document ).on( 'click', '[btnedit]', function() {
		var id = $( this ).data( 'id' );
		var module = $( this ).data( 'module' );
		$.post( base_url + module + '/information', { id : id }, function( response ) {
			if( response ) {
				$( '[btnadd]' ).trigger('click');
				$('#form_container').html(response.view)
			} else {
				swal('Error','Error retrieving information');
			}
		}, 'json' );
	});
	$( document ).on( 'click', '[btnview]', function() {
		var id = $( this ).data( 'id' );
		var module = $( this ).data( 'module' );
		$.post( base_url + module + '/information', { id : id }, function( response ) {
			if( response ) {
				$( '[btnadd]' ).trigger('click');
				$('#form_container').html(response.view_details)
			} else {
				swal('Error','Error retrieving information');
			}
		}, 'json' );
	});
	$( document ).on( 'submit', '#delete_selected', function( event ) {
		module = $(this).attr('data-module');
		event.preventDefault();
		swal({
			title:'Delete Record', 
			text:'Are you sure you want to delete the selected record?',
			type:'warning',
			showCancelButton: true
		},function(confirm){
			if(!confirm) return false;
			selected_data = $('#delete_selected').serializeArray();
			selected_datas = [];
			$(selected_data).each(function(index, value){
				selected_datas[index] = value.value;
			});
			
			$.ajax({
				type: 'POST',
				url: base_url+module+'/delete_selected',
				data: { selected_datas : selected_datas },
				dataType: 'json',
				success: function(response){
					$('#list_table_container').html( response.list_table );
				}
			});
		});
	});
	
	$(document).on('change','[name=DataTables_Table_0_length]', function( event ){
		val = $(this).val();
		module = $('[btnedit]').attr('data-module');
		form = $(this).attr('data-form');
		if (module == "inventories") {
			url = base_url+module+'/search/0/0/0/DESC/'+val+'/0';
		} else {
			url = base_url+module+'/search/0/0/DESC/'+val+'/0';
		}
		$.ajax({
			url:url,
			dataType: 'json',
			success: function (response) {
				if (module == "sales_orders") {
					$('#sales_order_list_table_container').html( response.list_table );
				} else {
					$('#list_table_container').html( response.list_table );
				}
			}
		});
	});

	$(document).on('keyup','.search_field', function( event ){
		term = $(this).val();
		module = $(this).attr('data-module');
		form = $(this).attr('data-form');
		if (isNaN(form)) {
			form = 0;
		};
		$.ajax({
			url:base_url+module+'/search',
			type: 'post',
			data: {
				term : term,
				form : form,
			},
			dataType: 'json',
			success: function (response) {
				if (module == "sales_orders") {
					$('#sales_order_list_table_container').html( response.list_table );
				} else {
					$('#list_table_container').html( response.list_table );
				}
			}
		});
	});

	//RESET ALL VALUES TO NULL
	function clear_all_values() {
		$('input[type="text"]').val('');
        $('input[type="hidden"]').val('');
        $('textarea').html('');
        $('textarea').val('');
        $('input[type="checkbox"]').attr('checked', false);
        $('input[type="radio"]').attr('checked', false);
        $('input[type="file"]').val('');
        $('select').val('');
        $('.amt_lbl').html('');
        $("label.error").hide();
        $(".error").removeClass("error");
	}

	$( document ).on('click', '[btnaddsalesorder]', function () {
		$('#sales_order_list_container').hide();
		$('#sales_order_form_container').show().removeClass('hide');
		clear_all_values();
		$.ajax({
			url: base_url+'sales_orders/form',
			type: 'post',
			view : '0',
			success: function (response) {
				$('#sales_order_form_container').html( response );
			}
		});
	});
	$( document ).on('click', '[btneditsalesorder]', function () {
		id = $(this).attr('data-id');
		$('#sales_order_list_container').hide();
		$('#sales_order_form_container').show().removeClass('hide');
		clear_all_values();
		$.ajax({
			url: base_url+'sales_orders/form',
			type: 'post',
			data: {
				id : id,
				view : '0',
			},
			success: function (response) {
				$('#sales_order_form_container').html( response );
			}
		});
	});
	$( document ).on('click', '[btnviewsalesorder]', function () {
		id = $(this).attr('data-id');
		$('#sales_order_list_container').hide();
		$('#sales_order_form_container').show().removeClass('hide');
		clear_all_values();
		$.ajax({
			url: base_url+'sales_orders/form',
			type: 'post',
			data: {
				id : id,
				view : '1',
			},
			success: function (response) {
				$('#sales_order_form_container').html( response );
			}
		});
	});
	$( document ).on('click', '[btnclosesalesorder]', function () {
		$('#sales_order_list_container').show();
		$('#sales_order_form_container').hide().addClass('hide');
		clear_all_values();
	});

	$( document ).on('click', '[btnviewusergroup]', function () {
		id = $(this).attr('data-id');
		$.ajax({
			url:base_url+'user_group_permissions/information',
			type: 'post',
			data: {
				id : id,
			},
			dataType: 'json',
			success: function (response) {
				$('#user_group_permission_form_container').html( response.view );
			}
		});
	});
});

jQuery(function(){
    // Init page helpers (BS Datepicker + BS Datetimepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
    // App.initHelpers(['datepicker', 'datetimepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']);
    App.initHelpers(['datepicker']);
});