$( function( event ) {
	$("#sales_order_form").validate( {
		ignore: [],
        errorClass: 'help-block animated fadeInDown',
        errorElement: 'div',
        errorPlacement: function(error, e) {
            jQuery(e).parents('.form-group > div').append(error);
        },
        highlight: function(e) {
            var elem = jQuery(e);

            elem.closest('.form-group').removeClass('has-error').addClass('has-error');
            elem.closest('.help-block').remove();
        },
        success: function(e) {
            var elem = jQuery(e);

            elem.closest('.form-group').removeClass('has-error');
            elem.closest('.help-block').remove();
        },
		rules: {
			overall_total_cost: {
				required: true
			},
		},
		submitHandler: function( form, event ) {
			event.preventDefault();
			swal({
				title:'Save Record', 
				text:'Are you sure you want to save this record?',
				type:'warning',
				showCancelButton: true
			},function(confirm){
				if(!confirm) return false;
				$( 'html' ).waitMe( {
					effect : 'ios',
					text : 'Processing...',
					bg : 'rgba(0,0,0,0.7)',
					color : '#fff',
					sizeW : '',
					sizeH : ''
				});
				var data = new FormData( form );
				$.ajax({
					url: base_url + 'sales_orders/index',
					type: 'POST',
					data: data,
					async: false,
					cache: false,
					contentType: false,
					processData: false,
					dataType: "json",
					success: function ( data ) {
						$('html').waitMe('hide');
						$('#sales_order_list_table_container').html( data.list_table );
						$('[btnclosesalesorder]').trigger('click');
						if(!response.success){
							swal('Error', response.message,'error');
						} else {
							swal('Success', 'Record has been saved!');
						}
					},
					error:function(x,r,e){
						$('html').waitMe('hide');
						swal('Error', e, 'error');
					}
				});
			})
		}
	});
});