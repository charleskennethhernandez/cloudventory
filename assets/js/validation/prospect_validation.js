$( function( event ) {

	$("#prospect_form").validate( {

		rules: {
			
			firstname: {

				required: true

			},

			lastname: {

				required: true

			},

			email: {

				required: true,
				
				email: true

			},

			phone: {

				required: true

			},

			address: {

				required: true
		
			},

			status: {

				required: true
		
			}

		},

		submitHandler: function( form, event ) {
		
			event.preventDefault();
			
			//var answer = confirm( 'Do you want to save this prospect client?' );
			
			confirm_trigger( 'Do you want to save this prospect client?' );
			$("#cofirm_button").unbind('click');
			
			$('#cofirm_button').click(function(){

				$( 'html' ).waitMe( {

					effect : 'ios',

					text : 'Processing...',

					bg : 'rgba(0,0,0,0.7)',

					color : '#fff',

					sizeW : '',

					sizeH : ''

				} );

				var data = new FormData( form );

				$.ajax( {
				
					url: base_url + 'clients/index',
					
					type: 'POST',
					
					data: data,
					
					async: false,
					
					cache: false,
					
					contentType: false,
					
					processData: false,
					
					dataType: "json",
					
					success: function ( data ) {
					
						$('html').waitMe('hide');

						$('#prospect_list_table_container').html( data.client_list_table );
		
						$('.close_prospect_add').click();
					
						alert( 'Prospect client has been ' + data.action + 'd!' );
						
					}
					
				} );
			
			} );

		}

	} );

} );