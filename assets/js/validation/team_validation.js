$( function( event ) {

	$("#teams_form").validate( {

		rules: {
		
			agent_type_id: {
			
				required: true
			
			},

			firstname: {

				required: true

			},

			lastname: {

				required: true

			},

			email: {

				required: true,
				
				email: true

			},

			phone: {

				required: true

			},

			area_id: {

				required: true,
				
				integer: true
		
			},
			
			city_id: {

				required: true,
				
				integer: true
		
			},

			username: {

				required: true

			},

			password: {

				required: {
				
                  depends: function( element ) {
				  
						if( $('#id').val() == '' ) {
						
							return true;
						
						} else {
						
							return false;
						
						}
					   
					}
				  
				},
				
				minlength: 6

			},

			confirm_password: {

				required: {
				
                  depends: function( element ) {
				  
						if( $('#id').val() == '' ) {
						
							return true;
						
						} else {
						
							if( $('#password').val() != '' ) {
							
								return true;
							
							} else {
						
								return false;
							
							}
						
						}
					   
					}
				  
				},
				
				minlength: 6,
				
				equalTo: "#password"

			},
			
			is_active: {

				required: true,
				
				integer: true

			},

		},

		submitHandler: function( form, event ) {
		
			event.preventDefault();
			
			//var answer = confirm( 'Do you want to save this agent?' );
			
			confirm_trigger( 'Do you want to save this agent?' );
			$("#cofirm_button").unbind('click');
			
			$('#cofirm_button').click(function(){

				$( 'html' ).waitMe( {

					effect : 'ios',

					text : 'Processing...',

					bg : 'rgba(0,0,0,0.7)',

					color : '#fff',

					sizeW : '',

					sizeH : ''

				} );

				var data = new FormData( form );

				$.ajax( {
				
					url: base_url + 'teams/index',
					
					type: 'POST',
					
					data: data,
					
					async: false,
					
					cache: false,
					
					contentType: false,
					
					processData: false,
					
					dataType: "json",
					
					success: function ( data ) {
					
						$('html').waitMe('hide');
						
						$('#agent_list_table_container').html( data.agent_list_table );
						
						$('.cancel_info').click();
					
						alert( 'Agent has been ' + data.action + 'd!' );

					}
					
				} );
			
			} );

		}

	} );

} );