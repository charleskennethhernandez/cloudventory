$( function( event ) {

	$("#developer_form").validate( {

		rules: {

			name: {

				required: true

			},

			address: {

				required: true

			},

			phone: {

				required: true,

				number: true

			},

			email: {

				required: true,

				email: true

			},

			is_active: {

				required: true

			},

			username: {

				required: true

			},

			password: {

				required: true

			}, 
			secondpassword: {

				required: true,

				equalTo: "#password"
			
			}


		},

		submitHandler: function( form, event ) {
		
			event.preventDefault();
			
			var action;
			
			var id = $( '#id' ).val();
			
			if( id ) {
			
				action = 'update';
			
			} else {
			
				action = 'save';
			
			}
			
			//var answer = confirm( 'Do you want to '+action+' this developer?' );
			
			confirm_trigger( 'Do you want to '+action+' this developer?' );
			$("#cofirm_button").unbind('click');
			
			$('#cofirm_button').click(function(){

				$( 'html' ).waitMe( {

					effect : 'ios',

					text : 'Processing...',

					bg : 'rgba(0,0,0,0.7)',

					color : '#fff',

					sizeW : '',

					sizeH : ''

				} );

				var data = new FormData( form );

				$.ajax( {
				
					url: base_url + 'developers/index',
					
					type: 'POST',
					
					data: data,
					
					async: false,
					
					cache: false,
					
					contentType: false,
					
					processData: false,
					
					dataType: "json",
					
					success: function ( response ) {

						$( 'html' ).waitMe( 'hide' );
						
						$('#developer_list_table_container').html( response.developer_list_table );
						
						$( '.close_developer_add' ).click();
					
						alert( 'Developer has been ' + response.action + 'd!' );

					},

					error: function (response){
						console.log(response)
					}
					
				} );
			
			} );

		}

	} );

} );