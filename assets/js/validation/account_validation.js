$( function( event ) {
	$("#account_form").validate( {
		rules: {
			firstname: {
				required: true
			},
			lastname: {
				required: true
			},
			email: {
				required: true,				
				email: true
			},
			phone: {
				required: true
			},
			username: {
				required: true
			},
			/*address: {
				required: true
			},*/
			password: {
				required: {
				
                  depends: function( element ) {
				  
						if( $('#id').val() == '' ) {
						
							return true;
						
						} else {
						
							return false;
						
						}
					   
					}
				  
				},
				
				minlength: 6
			},
			secondpassword: {
				required: {
				
                  depends: function( element ) {
				  
						if( $('#id').val() == '' ) {
						
							return true;
						
						} else {
						
							if( $('#password').val() != '' ) {
							
								return true;
							
							} else {
						
								return false;
							
							}
						
						}
					   
					}
				  
				},
				
				minlength: 6,
				
				equalTo: "#password"
			}
		},
		submitHandler: function( form, event ) {		
			event.preventDefault();
			
			confirm_trigger( 'Do you want to update your account?' );
			$("#cofirm_button").unbind('click');
			
			$('#cofirm_button').click(function(){
				$( 'html' ).waitMe( {
					effect : 'ios',
					text : 'Processing...',
					bg : 'rgba(0,0,0,0.7)',
					color : '#fff',
					sizeW : '',
					sizeH : ''
				} );
				var data = new FormData( form );
				$.ajax( {				
					url: base_url + 'support/account',					
					type: 'POST',					
					data: data,					
					async: false,					
					cache: false,					
					contentType: false,					
					processData: false,					
					dataType: "json",					
					success: function ( data ) {					
						$('html').waitMe('hide');
						$('#account_container_list').html( data.account_list );						
						$('.close_account_edit').click();					
						modal_trigger('Success','Your account has been updated!' );
					}					
				});			
			});
		}
	} );
} );