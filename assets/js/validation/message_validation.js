$( function( event ) {

	$("#message_form").validate( {

		rules: {

			recipient: {

				required: true

			},

			subject: {

				required: true

			},

			content: {

				required: true

			},

			user_type_id: {

				required: true

			}

		},

		submitHandler: function( form, event ) {
		
			event.preventDefault();

			// if ($.trim(tinyMCE.get('content').getContent({format: 'text'})) === '') {
			// 	modal_trigger('Alert', 'Your message is required.');
			// 	return;
			// }
			
			var id = $( '#id' ).val();
			
			var action = 'send';
			
		
			confirm_trigger( 'Do you want to '+action+' this message?' );
			$("#cofirm_button").unbind('click');
			$('#cofirm_button').click(function(){
			

				$( 'html' ).waitMe( {

					effect : 'ios',

					text : 'Processing...',

					bg : 'rgba(0,0,0,0.7)',

					color : '#fff',

					sizeW : '',

					sizeH : ''

				} );

				var data = new FormData( form );

				$.ajax( {
				
					url: base_url + 'support/index',
					
					type: 'POST',
					
					data: data,
					
					async: false,
					
					cache: false,
					
					contentType: false,
					
					processData: false,
					
					dataType: "json",
					
					success: function ( response ) {

						$( 'html' ).waitMe( 'hide' );
						
						$('#sent_message_list_table_container').html( response.message_list_table );
						
						$( '.close_message_add' ).click();
						
						
						modal_trigger('Success','Message has been sent!' );

					},

					error: function (response){
						console.log(response)
					}
					
				} );
			
			});

		}

	} );

} );