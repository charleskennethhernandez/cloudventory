$('.modal').on('show.bs.modal', function (e) {
	$(this).find('label.error').hide();
	$(this).find('[name]').val(null).trigger('change');
})