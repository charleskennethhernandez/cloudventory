var system_message = function(message){
	$('#system_message_container').empty().append('\
		<div class="alert alert-success alert-dismissable">\
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>\
            <h3 class="font-w300 push-15">'+message+'</h3>\
        </div>\
	');
}
var error_message = function(message){
	$('#system_message_container').empty().append('\
		<div class="alert alert-danger alert-dismissable">\
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>\
            <h3 class="font-w300 push-15">'+message+'</h3>\
        </div>\
	');
}
var form_success_message = function(form, message){
	$(form).find('.alert').remove();
	$(form).prepend('\
		<div class="alert alert-success alert-dismissable">'+message+'</div>\
	');
}
var form_error_message = function(form, message){
	$(form).find('.alert').remove();
	$(form).prepend('\
		<div class="alert alert-danger alert-dismissable">'+message+'</div>\
	');
}