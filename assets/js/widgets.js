(function ( $ ) {
    $.fn.dropdown = function(params) {
    	if(!params)
    		throw 'missing param';
    	if(!'url' in params)
    		throw 'missing url parameter';
    	if(!'key' in params)
    		throw 'missing key parameter';
    	if(!'label' in params)
    		throw 'missing label parameter';
    	var $this = this;
    	$.ajax({
    		url: params.url
    		, 'type': 'POST'
    		, 'dataType': 'JSON'
    		, success:function(r){
    			if(!r.success)
    				throw r.message;
    			else{
    				for(var k in r.data){
    					var key = r.data[k][params.key];
    					var label = r.data[k][params.label];
    					$this.append(new Option(label,key));
    				}
    			}
    		}
    		, error:function(r,x,s){
    			throw x;
    		}
    	});
        return this;
    };
}( jQuery ));