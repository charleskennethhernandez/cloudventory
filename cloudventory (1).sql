-- Adminer 4.2.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `brands`;
CREATE TABLE `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `is_deleted` int(11) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `brands` (`id`, `name`, `description`, `date_created`, `date_updated`, `created_by`, `is_deleted`, `is_active`) VALUES
(1,	'Tiger Fireworks',	'sample descrition\n',	'2016-06-24 21:37:14',	'2016-06-24 21:37:14',	0,	0,	1),
(2,	'Leopard Fireworks',	'sample description',	'2016-06-24 21:37:14',	'2016-06-24 21:37:14',	0,	0,	1),
(3,	'LF Fireworks',	'',	'2016-06-24 21:37:14',	'2016-06-24 21:37:14',	0,	0,	1),
(4,	'Dragon Fireworks',	'\n',	'2016-06-24 21:37:14',	'2016-06-24 21:37:14',	0,	0,	1),
(5,	'Nation Fireworks',	'',	'2016-06-24 21:37:14',	'2016-06-24 21:37:14',	0,	0,	1),
(6,	'Nation Fireworks',	'',	'2016-06-24 21:37:14',	'2016-06-24 21:37:14',	0,	0,	1),
(7,	'Platinum Fireworks',	'',	'2016-06-24 21:37:14',	'2016-06-24 21:37:14',	0,	0,	1),
(8,	'Diamond Fireworks',	'',	'2016-06-24 21:37:14',	'2016-12-03 05:25:41',	4,	0,	1),
(9,	'Phoenix Fireworks',	'',	'2016-06-24 21:37:14',	'2016-06-24 21:37:14',	0,	0,	1),
(10,	'Nation Fireworks',	'',	'2016-07-21 22:42:06',	'0000-00-00 00:00:00',	0,	0,	1),
(11,	'Nation Fireworks',	'',	'2016-11-30 02:35:25',	'2016-12-03 05:25:29',	4,	1,	1);

DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `middlename` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `date_of_birth` datetime NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `is_deleted` int(11) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `customers` (`id`, `firstname`, `middlename`, `lastname`, `address`, `email`, `phone`, `date_of_birth`, `date_created`, `date_updated`, `created_by`, `is_deleted`, `is_active`) VALUES
(1,	'Juan',	'Miguel',	'Dela Cruz',	'151 Camia St. Sta Cruz Manila',	'juanmigueldelacruz@gmail.com',	'09251012932',	'0000-00-00 00:00:00',	'2016-12-03 05:17:54',	'2016-12-03 05:17:54',	4,	0,	1),
(2,	'Carl',	'Cubacub',	'Hernandez',	'',	'',	'',	'0000-00-00 00:00:00',	'2016-12-03 07:36:57',	'2016-12-03 07:36:57',	4,	0,	1);

DROP TABLE IF EXISTS `files`;
CREATE TABLE `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(255) NOT NULL,
  `raw_file` varchar(255) NOT NULL,
  `module` varchar(255) NOT NULL,
  `module_id` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `is_deleted` int(11) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `files` (`id`, `file`, `raw_file`, `module`, `module_id`, `date_created`, `is_deleted`, `is_active`) VALUES
(27,	'75d2c44afe5823b35683a28195eab980.jpg',	'azure north.jpg',	'inventories',	3,	'2016-06-25 15:58:20',	0,	0),
(31,	'47b0b8535341e5d86b52c7243a8ef9a5.jpg',	'^C400B1ED744F4291443396F4D58EF590DC18481A705D165F1B^pimgpsh_fullsize_distr.jpg',	'inventories',	3,	'2016-06-25 17:10:54',	0,	0);

DROP TABLE IF EXISTS `inventory_categories`;
CREATE TABLE `inventory_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `is_deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `inventory_categories` (`id`, `name`, `description`, `date_created`, `date_updated`, `created_by`, `is_active`, `is_deleted`) VALUES
(1,	'sample 1',	'sample 1s',	'2016-03-29 16:25:19',	'2016-07-31 06:58:49',	0,	1,	0),
(2,	'sample 2',	'sample 2',	'2016-03-31 12:05:16',	'2016-03-31 12:05:16',	0,	1,	0),
(3,	'sample 3',	'sample 3',	'2016-03-31 12:05:35',	'2016-03-31 12:05:35',	0,	1,	0),
(4,	'sample 4',	'sample 4',	'2016-03-31 12:05:43',	'2016-03-31 12:05:43',	0,	1,	0),
(5,	'sample 5',	'sample 5',	'2016-03-31 12:05:50',	'2016-03-31 12:05:50',	0,	1,	0),
(6,	'Novelties, Wheels & Spinners',	'',	'2016-07-31 06:59:06',	'0000-00-00 00:00:00',	0,	1,	0),
(7,	'Confetti',	'',	'2016-07-31 06:59:18',	'0000-00-00 00:00:00',	0,	1,	0),
(8,	'Sparklers / Lances and Rockets',	'',	'2016-07-31 06:59:29',	'0000-00-00 00:00:00',	0,	1,	0),
(9,	'Fountains and Waterfalls',	'',	'2016-07-31 06:59:39',	'0000-00-00 00:00:00',	0,	1,	0),
(10,	'Bangers',	'',	'2016-07-31 06:59:50',	'0000-00-00 00:00:00',	0,	1,	0),
(11,	'Small Caliber Multi-Shot Cakes',	'',	'2016-07-31 06:59:59',	'0000-00-00 00:00:00',	0,	1,	0),
(12,	'Large Caliber Multi-Shot Cakes',	'',	'2016-07-31 07:00:09',	'0000-00-00 00:00:00',	0,	1,	0),
(13,	'Aerial Display Shells',	'',	'2016-07-31 07:00:19',	'0000-00-00 00:00:00',	0,	1,	0),
(14,	'Premium Edition Line',	'',	'2016-07-31 07:00:28',	'0000-00-00 00:00:00',	0,	1,	0),
(15,	'Professional Line',	'',	'2016-07-31 07:00:37',	'2016-12-03 05:24:42',	4,	1,	0);

DROP TABLE IF EXISTS `modules`;
CREATE TABLE `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `key` int(11) NOT NULL,
  `module` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `class-icon` varchar(255) NOT NULL,
  `is_deleted` int(11) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `modules` (`id`, `parent`, `parent_id`, `key`, `module`, `title`, `class-icon`, `is_deleted`, `is_active`) VALUES
(1,	1,	0,	1,	'accounts',	'Accounts',	'fa fa-users',	0,	1),
(2,	1,	0,	2,	'inventory',	'Inventory',	'fa fa-table',	0,	1),
(3,	1,	0,	3,	'orders',	'Orders',	'fa fa-shopping-cart',	0,	1),
(5,	1,	0,	5,	'settings',	'Settings',	'fa fa-wrench',	0,	1),
(6,	0,	1,	1,	'customers',	'Customer List',	'',	0,	1),
(7,	0,	1,	2,	'suppliers',	'Supplier List',	'',	0,	1),
(8,	0,	2,	1,	'inventories',	'Inventory List',	'',	0,	1),
(9,	0,	2,	2,	'inventory_categories',	'Inventory Category',	'',	0,	1),
(10,	0,	2,	3,	'brands',	'Manage Brands',	'',	0,	1),
(11,	0,	3,	1,	'sales_orders',	'Purchase Orders',	'',	0,	1),
(12,	0,	3,	2,	'sales_order_items',	'Sales Order Items',	'',	0,	1),
(13,	0,	5,	1,	'users',	'Users',	'',	0,	1),
(14,	0,	5,	2,	'user_groups',	'User Groups',	'',	0,	1),
(16,	1,	0,	6,	'reports',	'Reports',	'fa fa-download',	0,	1),
(17,	0,	16,	1,	'sales_reports',	'Sales Reports',	'',	0,	1);

DROP TABLE IF EXISTS `patients`;
CREATE TABLE `patients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `middlename` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `is_deleted` int(11) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `patients` (`id`, `firstname`, `middlename`, `lastname`, `date_created`, `date_updated`, `is_deleted`, `is_active`) VALUES
(1,	'Charles Kenneth',	'Cubacub',	'Hernandez',	'2016-03-15 00:00:00',	'2016-03-15 00:00:00',	0,	0);

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `is_deleted` int(11) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `sales_orders`;
CREATE TABLE `sales_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `total_due` double(20,2) NOT NULL,
  `total_discount` double(20,2) NOT NULL,
  `grand_total` double(20,2) NOT NULL,
  `amount_paid` double(20,2) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `is_deleted` int(11) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `sales_orders` (`id`, `customer_id`, `total_due`, `total_discount`, `grand_total`, `amount_paid`, `status`, `created_by`, `date_created`, `date_updated`, `is_deleted`, `is_active`) VALUES
(1,	1,	0.00,	0.00,	231.25,	231.25,	1,	4,	'2016-12-03 05:27:42',	'2016-12-03 05:28:24',	0,	1),
(2,	1,	0.00,	0.00,	14.00,	14.00,	1,	4,	'2016-12-03 05:48:14',	'2016-12-03 07:34:14',	0,	1),
(3,	1,	219.25,	0.00,	231.25,	12.00,	2,	4,	'2016-12-03 05:56:41',	'2016-12-03 07:36:26',	0,	1),
(4,	2,	437.50,	0.00,	462.50,	25.00,	2,	4,	'2016-12-03 07:36:57',	'2016-12-03 07:37:40',	0,	1);

DROP TABLE IF EXISTS `sales_order_items`;
CREATE TABLE `sales_order_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sales_order_id` int(11) NOT NULL,
  `inventory_id` int(11) NOT NULL,
  `unit_cost` double(20,2) NOT NULL,
  `actual_price` double(20,2) NOT NULL,
  `discount` double(20,2) NOT NULL,
  `total_cost` double(20,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `is_deleted` int(11) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `sales_order_items` (`id`, `sales_order_id`, `inventory_id`, `unit_cost`, `actual_price`, `discount`, `total_cost`, `quantity`, `date_created`, `date_updated`, `is_deleted`, `is_active`) VALUES
(1,	1,	607,	231.25,	0.00,	0.00,	231.25,	1,	'2016-12-03 05:27:42',	'2016-12-03 05:27:42',	0,	1),
(2,	1,	1,	231.25,	0.00,	0.00,	231.25,	1,	'2016-12-03 05:28:24',	'2016-12-03 05:28:24',	0,	1),
(3,	2,	607,	1.00,	0.00,	0.00,	14.00,	14,	'2016-12-03 05:48:14',	'2016-12-03 07:29:15',	1,	1),
(4,	2,	3,	1.00,	0.00,	0.00,	14.00,	14,	'2016-12-03 05:48:21',	'2016-12-03 05:48:21',	1,	1),
(5,	3,	607,	231.25,	0.00,	0.00,	231.25,	1,	'2016-12-03 05:56:41',	'2016-12-03 07:36:26',	0,	1),
(8,	2,	607,	1.00,	0.00,	0.00,	14.00,	14,	'2016-12-03 07:33:44',	'2016-12-03 07:33:44',	1,	1),
(9,	2,	607,	1.00,	0.00,	0.00,	14.00,	14,	'2016-12-03 07:34:14',	'2016-12-03 07:34:14',	0,	1),
(10,	2,	3,	1.00,	0.00,	0.00,	14.00,	14,	'2016-12-03 07:34:14',	'2016-12-03 07:34:14',	0,	1),
(11,	4,	607,	231.25,	0.00,	0.00,	462.50,	2,	'2016-12-03 07:36:57',	'2016-12-03 07:37:40',	0,	1);

DROP TABLE IF EXISTS `sales_order_status`;
CREATE TABLE `sales_order_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `sales_order_status` (`id`, `name`) VALUES
(1,	'Paid'),
(2,	'Incomplete');

DROP TABLE IF EXISTS `sales_reports`;
CREATE TABLE `sales_reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `is_deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `sales_reports` (`id`, `name`, `description`, `is_active`, `is_deleted`) VALUES
(1,	'Time Framed Sales Report',	'',	1,	0),
(2,	'Top Sold Items',	'',	1,	0);

DROP TABLE IF EXISTS `search`;
CREATE TABLE `search` (
  `code` varchar(40) NOT NULL,
  `term` varchar(255) NOT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `suppliers`;
CREATE TABLE `suppliers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `middlename` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `date_of_birth` datetime NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `is_deleted` int(11) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `suppliers` (`id`, `name`, `firstname`, `middlename`, `lastname`, `email`, `phone`, `date_of_birth`, `date_created`, `date_updated`, `created_by`, `is_deleted`, `is_active`) VALUES
(1,	'Supplier A',	'John',	'Hawthorne',	'Doe',	'johndoe@gmail.com',	'09992921478',	'0000-00-00 00:00:00',	'2016-12-03 05:19:29',	'2016-12-03 05:19:29',	4,	0,	1);

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(20) NOT NULL,
  `middlename` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `is_deleted` int(11) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `users` (`id`, `firstname`, `middlename`, `lastname`, `username`, `password`, `email`, `phone`, `address`, `user_group_id`, `date_created`, `date_updated`, `created_by`, `is_deleted`, `is_active`) VALUES
(2,	'Charles',	'Hernandez',	'Hernandez',	'admin',	'8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92',	'ckhrnndz@gmail.com',	'0778402253',	'Flair Towers',	1,	'0000-00-00 00:00:00',	'2016-06-28 22:05:36',	0,	0,	1),
(4,	'neneth',	'gonzales',	'ramos',	'nenethramos',	'e10adc3949ba59abbe56e057f20f883e',	'',	'09228432681',	'                                        295 zone 1, sto nino baliwag, buiacan                                    ',	1,	'2016-07-31 14:27:49',	'2016-11-30 10:17:04',	4,	0,	1),
(5,	'vendor1',	'vendor1',	'vendor1',	'vendor1',	'de750fc06e9e4d447695b87c74cb928b44f7ad721e39747d83f194f439837efc',	'',	'',	'vendor1',	2,	'2016-11-17 19:07:10',	'2016-11-17 19:07:10',	0,	1,	1),
(6,	'das',	'ads',	'sad',	'dsa',	'5f039b4ef0058a1d652f13d612375a5b',	'',	'321',	'dsa',	6,	'2016-11-30 10:17:45',	'2016-11-30 10:17:45',	4,	1,	1);

DROP TABLE IF EXISTS `user_groups`;
CREATE TABLE `user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `is_deleted` int(11) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user_groups` (`id`, `name`, `description`, `date_created`, `date_updated`, `created_by`, `is_deleted`, `is_active`) VALUES
(1,	'Administrator',	'Full access on system',	'2016-06-28 22:00:02',	'2016-11-30 07:52:56',	4,	0,	1),
(2,	'Vendor 1',	'Vendor Group',	'2016-06-28 22:17:45',	'2016-11-30 07:52:53',	4,	0,	1),
(3,	'Staff',	'Additional Sales group',	'2016-11-30 07:52:48',	'2016-11-30 07:53:10',	4,	1,	1),
(4,	'New',	'',	'2016-11-30 08:20:27',	'2016-11-30 08:20:27',	4,	1,	1),
(5,	'asd',	'',	'2016-11-30 08:25:43',	'2016-11-30 08:25:43',	4,	1,	1),
(6,	'dsa',	'',	'2016-11-30 08:26:46',	'2016-11-30 08:26:46',	4,	1,	1);

DROP TABLE IF EXISTS `user_group_permissions`;
CREATE TABLE `user_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `view` int(11) NOT NULL DEFAULT '1',
  `add` int(11) NOT NULL DEFAULT '1',
  `edit` int(11) NOT NULL DEFAULT '1',
  `delete` int(11) NOT NULL DEFAULT '1',
  `date_updated` datetime NOT NULL,
  `date_created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `is_deleted` int(11) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user_group_permissions` (`id`, `module_id`, `user_group_id`, `view`, `add`, `edit`, `delete`, `date_updated`, `date_created`, `created_by`, `is_deleted`, `is_active`) VALUES
(1,	1,	1,	1,	1,	1,	1,	'2016-10-09 10:44:37',	'0000-00-00 00:00:00',	0,	0,	1),
(2,	2,	1,	1,	1,	1,	1,	'2016-10-09 10:44:37',	'0000-00-00 00:00:00',	0,	0,	1),
(3,	3,	1,	1,	1,	1,	1,	'2016-10-09 10:44:37',	'0000-00-00 00:00:00',	0,	0,	1),
(4,	4,	1,	1,	1,	1,	1,	'2016-10-09 10:44:37',	'0000-00-00 00:00:00',	0,	0,	1),
(5,	5,	1,	1,	1,	1,	1,	'2016-10-09 10:44:37',	'0000-00-00 00:00:00',	0,	0,	1),
(6,	6,	1,	1,	1,	1,	1,	'2016-10-09 10:44:37',	'0000-00-00 00:00:00',	0,	0,	1),
(7,	7,	1,	1,	1,	1,	1,	'2016-10-09 10:44:37',	'0000-00-00 00:00:00',	0,	0,	1),
(8,	8,	1,	1,	1,	1,	1,	'2016-10-09 10:44:37',	'0000-00-00 00:00:00',	0,	0,	1),
(9,	9,	1,	1,	1,	1,	1,	'2016-10-09 10:44:37',	'0000-00-00 00:00:00',	0,	0,	1),
(10,	10,	1,	1,	1,	1,	1,	'2016-10-09 10:44:37',	'0000-00-00 00:00:00',	0,	0,	1),
(11,	11,	1,	1,	1,	1,	1,	'2016-10-09 10:44:37',	'0000-00-00 00:00:00',	0,	0,	1),
(12,	12,	1,	1,	1,	1,	1,	'2016-10-09 10:44:37',	'0000-00-00 00:00:00',	0,	0,	1),
(13,	13,	1,	1,	1,	1,	1,	'2016-10-09 10:44:37',	'0000-00-00 00:00:00',	0,	0,	1),
(14,	14,	1,	1,	1,	1,	1,	'2016-10-09 10:27:55',	'0000-00-00 00:00:00',	0,	0,	1),
(15,	15,	1,	1,	1,	1,	1,	'2016-10-09 10:27:55',	'0000-00-00 00:00:00',	0,	0,	1),
(17,	1,	2,	1,	1,	1,	1,	'2016-12-02 10:08:18',	'0000-00-00 00:00:00',	0,	0,	1),
(18,	2,	2,	1,	0,	0,	0,	'2016-12-02 10:08:18',	'0000-00-00 00:00:00',	0,	0,	1),
(19,	3,	2,	1,	1,	1,	1,	'2016-12-02 10:08:18',	'0000-00-00 00:00:00',	0,	0,	1),
(20,	4,	2,	1,	1,	1,	1,	'2016-12-02 10:08:18',	'0000-00-00 00:00:00',	0,	0,	1),
(21,	5,	2,	1,	1,	1,	1,	'2016-12-02 10:08:18',	'0000-00-00 00:00:00',	0,	0,	1),
(22,	6,	2,	1,	0,	0,	0,	'2016-12-02 10:08:18',	'0000-00-00 00:00:00',	0,	0,	1),
(23,	7,	2,	0,	1,	1,	1,	'2016-12-02 10:08:18',	'0000-00-00 00:00:00',	0,	0,	1),
(24,	8,	2,	1,	0,	0,	0,	'2016-12-02 10:08:18',	'0000-00-00 00:00:00',	0,	0,	1),
(25,	9,	2,	0,	1,	1,	1,	'2016-12-02 10:08:18',	'0000-00-00 00:00:00',	0,	0,	1),
(26,	10,	2,	1,	0,	0,	0,	'2016-12-02 10:08:18',	'0000-00-00 00:00:00',	0,	0,	1),
(27,	11,	2,	1,	1,	1,	1,	'2016-12-02 10:08:18',	'0000-00-00 00:00:00',	0,	0,	1),
(28,	12,	2,	0,	1,	1,	1,	'2016-12-02 10:08:18',	'0000-00-00 00:00:00',	0,	0,	1),
(29,	13,	2,	0,	1,	1,	1,	'2016-12-02 10:08:18',	'0000-00-00 00:00:00',	0,	0,	1),
(30,	14,	2,	0,	1,	1,	1,	'2016-12-02 10:08:18',	'0000-00-00 00:00:00',	0,	0,	1),
(31,	15,	2,	0,	1,	1,	1,	'2016-12-02 10:08:18',	'0000-00-00 00:00:00',	0,	0,	1),
(32,	13,	4,	1,	1,	1,	1,	'2016-11-30 08:20:27',	'2016-11-30 08:20:27',	4,	0,	1),
(33,	6,	4,	1,	1,	1,	1,	'2016-11-30 08:20:27',	'2016-11-30 08:20:27',	4,	0,	1),
(34,	8,	4,	1,	1,	1,	1,	'2016-11-30 08:20:27',	'2016-11-30 08:20:27',	4,	0,	1),
(35,	11,	4,	1,	1,	1,	1,	'2016-11-30 08:20:27',	'2016-11-30 08:20:27',	4,	0,	1),
(36,	9,	4,	1,	1,	1,	1,	'2016-11-30 08:20:27',	'2016-11-30 08:20:27',	4,	0,	1),
(37,	14,	4,	1,	1,	1,	1,	'2016-11-30 08:20:27',	'2016-11-30 08:20:27',	4,	0,	1),
(38,	12,	4,	1,	1,	1,	1,	'2016-11-30 08:20:27',	'2016-11-30 08:20:27',	4,	0,	1),
(39,	7,	4,	1,	1,	1,	1,	'2016-11-30 08:20:27',	'2016-11-30 08:20:27',	4,	0,	1),
(40,	10,	4,	1,	1,	1,	1,	'2016-11-30 08:20:27',	'2016-11-30 08:20:27',	4,	0,	1),
(41,	15,	4,	1,	1,	1,	1,	'2016-11-30 08:20:27',	'2016-11-30 08:20:27',	4,	0,	1),
(42,	1,	4,	1,	1,	1,	1,	'2016-11-30 08:20:27',	'2016-11-30 08:20:27',	4,	0,	1),
(43,	2,	4,	1,	1,	1,	1,	'2016-11-30 08:20:27',	'2016-11-30 08:20:27',	4,	0,	1),
(44,	3,	4,	1,	1,	1,	1,	'2016-11-30 08:20:27',	'2016-11-30 08:20:27',	4,	0,	1),
(45,	5,	4,	1,	1,	1,	1,	'2016-11-30 08:20:27',	'2016-11-30 08:20:27',	4,	0,	1),
(46,	13,	5,	1,	1,	1,	1,	'2016-11-30 08:25:43',	'2016-11-30 08:25:43',	4,	0,	1),
(47,	6,	5,	1,	1,	1,	1,	'2016-11-30 08:25:43',	'2016-11-30 08:25:43',	4,	0,	1),
(48,	8,	5,	1,	1,	1,	1,	'2016-11-30 08:25:43',	'2016-11-30 08:25:43',	4,	0,	1),
(49,	11,	5,	1,	1,	1,	1,	'2016-11-30 08:25:43',	'2016-11-30 08:25:43',	4,	0,	1),
(50,	9,	5,	1,	1,	1,	1,	'2016-11-30 08:25:43',	'2016-11-30 08:25:43',	4,	0,	1),
(51,	14,	5,	1,	1,	1,	1,	'2016-11-30 08:25:43',	'2016-11-30 08:25:43',	4,	0,	1),
(52,	12,	5,	1,	1,	1,	1,	'2016-11-30 08:25:43',	'2016-11-30 08:25:43',	4,	0,	1),
(53,	7,	5,	1,	1,	1,	1,	'2016-11-30 08:25:43',	'2016-11-30 08:25:43',	4,	0,	1),
(54,	10,	5,	1,	1,	1,	1,	'2016-11-30 08:25:43',	'2016-11-30 08:25:43',	4,	0,	1),
(55,	15,	5,	1,	1,	1,	1,	'2016-11-30 08:25:43',	'2016-11-30 08:25:43',	4,	0,	1),
(56,	1,	5,	1,	1,	1,	1,	'2016-11-30 08:25:43',	'2016-11-30 08:25:43',	4,	0,	1),
(57,	2,	5,	1,	1,	1,	1,	'2016-11-30 08:25:43',	'2016-11-30 08:25:43',	4,	0,	1),
(58,	3,	5,	1,	1,	1,	1,	'2016-11-30 08:25:43',	'2016-11-30 08:25:43',	4,	0,	1),
(59,	5,	5,	1,	1,	1,	1,	'2016-11-30 08:25:43',	'2016-11-30 08:25:43',	4,	0,	1),
(60,	13,	6,	1,	1,	1,	1,	'2016-11-30 08:33:53',	'2016-11-30 08:26:46',	4,	0,	1),
(61,	6,	6,	0,	1,	1,	1,	'2016-11-30 08:33:53',	'2016-11-30 08:26:46',	4,	0,	1),
(62,	8,	6,	0,	1,	1,	1,	'2016-11-30 08:33:53',	'2016-11-30 08:26:46',	4,	0,	1),
(63,	11,	6,	0,	1,	1,	1,	'2016-11-30 08:33:53',	'2016-11-30 08:26:46',	4,	0,	1),
(64,	9,	6,	1,	1,	1,	1,	'2016-11-30 08:33:53',	'2016-11-30 08:26:46',	4,	0,	1),
(65,	14,	6,	1,	1,	1,	1,	'2016-11-30 08:33:53',	'2016-11-30 08:26:46',	4,	0,	1),
(66,	12,	6,	1,	1,	1,	1,	'2016-11-30 08:33:53',	'2016-11-30 08:26:46',	4,	0,	1),
(67,	7,	6,	1,	1,	1,	1,	'2016-11-30 08:33:53',	'2016-11-30 08:26:46',	4,	0,	1),
(68,	10,	6,	1,	1,	1,	1,	'2016-11-30 08:33:53',	'2016-11-30 08:26:46',	4,	0,	1),
(69,	15,	6,	1,	1,	1,	1,	'2016-11-30 08:33:53',	'2016-11-30 08:26:46',	4,	0,	1),
(70,	1,	6,	1,	1,	1,	1,	'2016-11-30 08:33:53',	'2016-11-30 08:26:46',	4,	0,	1),
(71,	2,	6,	1,	1,	1,	1,	'2016-11-30 08:33:53',	'2016-11-30 08:26:46',	4,	0,	1),
(72,	3,	6,	1,	1,	1,	1,	'2016-11-30 08:33:53',	'2016-11-30 08:26:46',	4,	0,	1),
(73,	5,	6,	1,	1,	1,	1,	'2016-11-30 08:33:53',	'2016-11-30 08:26:46',	4,	0,	1),
(74,	16,	1,	1,	1,	1,	1,	'2016-10-09 10:27:55',	'0000-00-00 00:00:00',	0,	0,	1),
(75,	17,	1,	1,	1,	1,	1,	'2016-10-09 10:27:55',	'0000-00-00 00:00:00',	0,	0,	1);

-- 2016-12-05 02:29:28
